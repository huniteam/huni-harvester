#!/bin/bash

# This script gets called by clients to wait until the service is ready.
# The env.sh files are also printed to stdout so the output can be evalled.
# Only the env commands should go to stdout, everything else goes to stderr.

set -e

# We can't use localhost here, as this uses a local Unix socket, not an external
# TCP socket. The Unix sockets become ready before the TCP ones do.
: ${PGHOST:=$( hostname )}
: ${PGPORT:=5432}

postgres-is-ready() {
    psql -l > /dev/null
}

wait-for-postgres() {
    local msg="Waiting for postgres to start up on $PGHOST:$PGPORT"
    local count=1
    local max=30
    while [[ $count -le $max ]]; do
        echo $msg \(attempt $count of $max ...\)
        if postgres-is-ready; then
            echo postgres is ready
            return 0
        fi
        count=$(( $count + 1 ))
        sleep 2
    done
    echo Giving up
    return 1
}

wait-for-postgres             >&2 # output to stderr
cat /fixture/postgres/env.sh  >&2 # cat to stderr for logging purposes
cat /fixture/postgres/env.sh      # cat to stdout for caller to eval
