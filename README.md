[TOC]

# Overview

This repo contains both the harvester which harvests documents from partner
feeds and processes them and the harvest status portal. See the [documentation
wiki](/huniteam/documentation/wiki/Home) for more information.

# Harvester

The harvest process runs daily and is run via the swarm cron service.

Harvested and pre-processed (clean) documents are stored in a PostgreSQL
database. Clean documents are transformed on demand for indexing in Solr
and are submitted to the HuNI Solr store.

## Provider and feed configuration

All feed configuration is contained within the harvest database.

Data providers are configured in [`harvest.provider`](/app/sqitch/deploy/create-provider-table.sql).

Feeds are configured in [`harvest.feed`](/app/sqitch/deploy/create-feed-table.sql).

Supported feed types are listed [`harvest.feed_type`](/app/sqitch/deploy/create-feed-table.sql).

Note that in additon to the original `HuNI` and `OAI` feed types a [`HuNI_CSV`](https://bitbucket.org/huniteam/huni-csv-validator/src/master/) type has been added. The `huni-harvester` now supports this feed type and it can
be added in the same way as the original types setting both `harvest.feed.feed_type_id` and `harvest.feed.preprocess_type_id` to the value `HuNI_CSV`.

The CSV format is documented in the [`huni-csv-validator repository`](https://bitbucket.org/huniteam/huni-csv-validator/src/master/).

# Harvest status

The harvest status application reads directly from the harvest database.
