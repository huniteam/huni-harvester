function harvestDateFormatter(value) {

    if ( value === '-Inf' ) {
      return undefined;
    }
    else {
      return '<a href="/#' + value + '">' + value + '</a>';
    }
}

function originalCountFormatter(value, row) {
    return countFormatter(value, row, 'original');
}

function cleanCountFormatter(value, row) {
    return countFormatter(value, row, 'clean');
}

function invalidCountFormatter(value, row) {
    return countFormatter(value, row, 'invalid');
}

function solrCountFormatter(value, row) {
    return countFormatter(value, row, 'solr');
}

function noSolrCountFormatter(value, row) {
    return countFormatter(value, row, 'no-solr');
}

function countFormatter (value, row, type) {
    if ( row.harvest_status === 'succeeded' ) {
        if ( value > 0 ) {
          return '<a href="/documents/#' + row.feed_id + '/' + row.harvest_date + '/' + type + '">' + value + '</a>';
        }
        else {
            return value;
        }
    }
    else {
        return undefined;
    }
}

function documentFormatter (value, row) {
    return '<a href="' + row.href + '">' + value + '</a>';
}

function rowStyle(row, index) {

    // var classes = ['active', 'success', 'info', 'warning', 'danger'];

    if (row.harvest_status === 'no harvest' || row.harvest_status === 'failed' ) {
        return {
             classes: 'danger'
        };
    }

    if (row.invalid_count) {
        return {
            classes: 'warning'
        };
    }

    if (row.is_latest) {
        return {
            classes: 'success'
        };
    }

    return {};
}
