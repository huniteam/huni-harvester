use 5.20.0;
use warnings;

use Test::Expr;
use Test::Fatal;

use Crypt::Digest::SHA1         qw( sha1_hex );
use Path::Tiny                  qw( path );

use HuNI::Harvest::Schema;

sub digest { goto &sha1_hex }
my $padding = "\n";

my $schema = HuNI::Harvest::Schema->connect($ENV{PGDSN});
my $rs = $schema->resultset('File');

$schema->txn_begin;

my $data_path = './t/data/file-collision';
my $file_a = path($data_path, 'a.bin')->slurp_raw;
my $file_b = path($data_path, 'b.bin')->slurp_raw;

ok $file_a ne $file_b, 'Files are different';
ok digest($file_a) eq digest($file_b), 'But SHA1 hashes match';

my $key_a = $rs->insert_content($file_a);
my $key_b = $rs->insert_content($file_b);
ok $key_a ne $key_b, 'Database keys differ';

ok $rs->find($key_a)->content eq $file_a, 'First file is untouched';
ok $rs->find($key_b)->content eq $file_b . $padding, 'Second file got padding appended';

ok $key_a eq digest($file_a), 'First key is digest of first file';
ok $key_b eq digest($file_b . $padding), 'Second key is digest of second file with added padding';

ok $key_a eq $rs->insert_content($file_a), 'Inserting the first file gives the first key';
ok $key_b eq $rs->insert_content($file_b), 'Inserting the second file gives the second key';

$schema->txn_rollback;

ok ! $rs->find($key_a);
ok ! $rs->find($key_b);

done_testing;
