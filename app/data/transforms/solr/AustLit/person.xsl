<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="urn:isbn:1-931666-33-4"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="biography"/>
      <xsl:call-template name="birthDate"/>
      <xsl:call-template name="culturalHeritage"/>
      <xsl:call-template name="deathDate"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="familyName"/>
      <xsl:call-template name="gender"/>
      <xsl:call-template name="individualName"/>
      <xsl:call-template name="occupation"/>
      <xsl:call-template name="primaryName"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="workCount"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="biography">
    <field name="biography">
      <xsl:value-of select="//src:cpfDescription/src:description/src:biogHist" />
    </field>
  </xsl:template>

  <xsl:template name="birthDate">
    <field name="birthDate">
      <xsl:value-of select="//src:cpfDescription/src:description/src:existDates/src:fromDate" />
    </field>
  </xsl:template>

  <xsl:template name="culturalHeritage">
    <field name="culturalHeritage">
      <xsl:value-of select="//src:cpfDescription/src:description/src:localDescriptions/src:localDescription[@localType='Ethnicity']/src:term" />
    </field>
  </xsl:template>

  <xsl:template name="deathDate">
    <field name="deathDate">
      <xsl:value-of select="//src:cpfDescription/src:description/src:existDates/src:toDate" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Person</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="familyName">
    <field name="familyName">
      <xsl:value-of select="//src:cpfDescription/src:identity/src:nameEntry/src:part[@localType='nameSuffix']" />
    </field>
  </xsl:template>

  <xsl:template name="gender">
    <field name="gender">
      <xsl:value-of select="//src:cpfDescription/src:description/src:localDescriptions/src:localDescription[@localType='gender']/src:term" />
    </field>
  </xsl:template>

  <xsl:template name="individualName">
    <field name="individualName">
      <xsl:value-of select="//src:cpfDescription/src:identity/src:nameEntry/src:part[@localType='namePrefix']" />
    </field>
  </xsl:template>

  <xsl:template name="occupation">
    <field name="occupation">
      <xsl:value-of select="//src:cpfDescription/src:description/src:functions/src:function/src:term" />
    </field>
  </xsl:template>

  <xsl:template name="primaryName">
    <field name="primaryName">
      <xsl:text>Y</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>AustLit</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>AustLit</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//src:control/src:recordId" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:value-of select="//src:cpfDescription/src:identity/src:entityId" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>http://www.austlit.edu.au/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="workCount">
    <field name="workCount">
      <xsl:value-of select="//src:cpfDescription/src:description/src:localDescriptions/src:localDescription[@localType='workCount']/src:term" />
    </field>
  </xsl:template>

</xsl:stylesheet>
