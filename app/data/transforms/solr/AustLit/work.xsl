<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="NONE"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="description"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="format"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="primaryTitle"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="startDate"/>
      <xsl:call-template name="type"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="description">
    <field name="description">
      <xsl:value-of select="//cpfDescription/description/biogHist" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Work</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="format">
    <field name="format">
      <xsl:value-of select="//cpfDescription/description/localDescriptions/localDescription[@localType='workType']" />
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//cpfDescription/identity/title" />
    </field>
  </xsl:template>

  <xsl:template name="primaryTitle">
    <field name="primaryTitle">
      <xsl:text>Y</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>AustLit</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>AustLit</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//control/recordId" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:value-of select="//cpfDescription/identity/entityId" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>http://www.austlit.edu.au/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="startDate">
    <field name="startDate">
      <xsl:value-of select="//cpfDescription/description/firstDate/fromDate" />
    </field>
  </xsl:template>

  <xsl:template name="type">
    <field name="type">
      <xsl:value-of select="//cpfDescription/description/localDescriptions/localDescription[@localType='form']" />
    </field>
  </xsl:template>

</xsl:stylesheet>
