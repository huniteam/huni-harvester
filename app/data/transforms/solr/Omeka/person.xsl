<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:oai="http://www.openarchives.org/OAI/2.0/"
    xmlns:src="http://omeka.org/schemas/omeka-xml/v5"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="biography"/>
      <xsl:call-template name="birthDate"/>
      <xsl:call-template name="culturalHeritage"/>
      <xsl:call-template name="deathDate"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="ethnicity"/>
      <xsl:call-template name="familyName"/>
      <xsl:call-template name="gender"/>
      <xsl:call-template name="individualName"/>
      <xsl:call-template name="occupation"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="sourceSiteTag"/>
      <xsl:call-template name="title"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="biography">
    <field name="biography">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Person_Description_Biography'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="birthDate">
    <field name="birthDate">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Person_BirthDate'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="culturalHeritage">
    <field name="culturalHeritage">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Person_CulturalHeritage'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="deathDate">
    <field name="deathDate">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Person_DeathDate'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Person</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="ethnicity">
    <field name="ethnicity">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Person_Nationality'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="familyName">
    <field name="familyName">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Person_Name_Family'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="gender">
    <field name="gender">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Person_Gender'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="individualName">
    <field name="individualName">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Person_Name_Individual'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="occupation">
    <field name="occupation">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Person_PersonType_Occupation'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>Omeka</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>Omeka</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//oai:record/oai:header/oai:identifier" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:value-of select="//src:item/src:itemLocation" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>https://omeka.cloud.unimelb.edu.au/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteTag">
    <field name="sourceSiteTag">
      <xsl:text>Omeka</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="title">
    <field name="title">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Person_Name_Title'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

</xsl:stylesheet>
