<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:oai="http://www.openarchives.org/OAI/2.0/"
    xmlns:src="http://omeka.org/schemas/omeka-xml/v5"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="country"/>
      <xsl:call-template name="description"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="locality"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="postcode"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="sourceSiteTag"/>
      <xsl:call-template name="state"/>
      <xsl:call-template name="type"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="country">
    <field name="country">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Place_Name_Country'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="description">
    <field name="description">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Place_Description_Long'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Place</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="locality">
    <field name="locality">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Place_Name_Locality'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Place_Name_PropertyName'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="postcode">
    <field name="postcode">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Place_Name_Postcode'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>Omeka</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>Omeka</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//oai:record/oai:header/oai:identifier" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:value-of select="//src:item/src:itemLocation" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>https://omeka.cloud.unimelb.edu.au/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteTag">
    <field name="sourceSiteTag">
      <xsl:text>Omeka</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="state">
    <field name="state">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Place_Name_State'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

  <xsl:template name="type">
    <field name="type">
      <xsl:for-each select="//src:elementContainer/src:element/src:name">
        <xsl:if test="text() = 'HuNI_Place_Type_of_Place'">
          <xsl:value-of select="following-sibling::src:elementTextContainer/src:elementText/src:text"/>
        </xsl:if>
      </xsl:for-each>
    </field>
  </xsl:template>

</xsl:stylesheet>
