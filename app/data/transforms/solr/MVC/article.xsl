<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="NONE"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="creationDate"/>
      <xsl:call-template name="creator"/>
      <xsl:call-template name="description"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="sourceSiteTag"/>
      <xsl:call-template name="type"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="creationDate">
    <field name="creationDate">
      <xsl:value-of select="//record/yearWritten" />
    </field>
  </xsl:template>

  <xsl:template name="creator">
    <field name="creator">
      <xsl:value-of select="//record/authors/fullName" />
    </field>
  </xsl:template>

  <xsl:template name="description">
    <field name="description">
      <xsl:value-of select="//record/content" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Work</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//record/title" />
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>MVC</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>Museums Victoria Collections</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//record/id" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:text>https://collections.museumsvictoria.com.au/</xsl:text>
      <xsl:value-of select="//record/id" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>https://collections.museumsvictoria.com.au/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteTag">
    <field name="sourceSiteTag">
      <xsl:text>This site allows users to explore the natural sciences and humanities collections of Museums Victoria in Australia, featuring collections of zoology, geology, palaeontology, history, indigenous cultures and technology.</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="type">
    <field name="type">
      <xsl:text>Article</xsl:text>
    </field>
  </xsl:template>

</xsl:stylesheet>
