<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="NONE"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="alternativeTitle"/>
      <xsl:call-template name="country"/>
      <xsl:call-template name="creationDate"/>
      <xsl:call-template name="description"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="format"/>
      <xsl:call-template name="formatDetails"/>
      <xsl:call-template name="language"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="sourceSiteTag"/>
      <xsl:call-template name="subTitle"/>
      <xsl:call-template name="type"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="alternativeTitle">
    <field name="alternativeTitle">
      <xsl:value-of select="//acmi/other_title" />
    </field>
  </xsl:template>

  <xsl:template name="country">
    <field name="country">
      <xsl:value-of select="//acmi/place_of_production" />
    </field>
  </xsl:template>

  <xsl:template name="creationDate">
    <field name="creationDate">
      <xsl:value-of select="//acmi/creation_date" />
    </field>
  </xsl:template>

  <xsl:template name="description">
    <field name="description">
      <xsl:value-of select="//acmi/description" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Work</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="format">
    <field name="format">
      <xsl:value-of select="//acmi/form" />
    </field>
  </xsl:template>

  <xsl:template name="formatDetails">
    <field name="formatDetails">
      <xsl:value-of select="//acmi/colour" />
      <xsl:text>, </xsl:text>
      <xsl:value-of select="//acmi/sound_audio" />
      <xsl:text>, </xsl:text>
      <xsl:value-of select="//acmi/length" />
    </field>
  </xsl:template>

  <xsl:template name="language">
    <field name="language">
      <xsl:value-of select="//acmi/language_keywords" />
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//acmi/title" />
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>ACMI</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>Australian Centre for the Moving Image</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//acmi/acmi_identifier" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:value-of select="//acmi/permalink" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>https://www.acmi.net.au/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteTag">
    <field name="sourceSiteTag">
      <xsl:text>Art, Film &amp; Digital Culture</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="subTitle">
    <field name="subTitle">
      <xsl:value-of select="//acmi/abstract" />
    </field>
  </xsl:template>

  <xsl:template name="type">
    <field name="type">
      <xsl:text>Motion Picture</xsl:text>
    </field>
  </xsl:template>

</xsl:stylesheet>
