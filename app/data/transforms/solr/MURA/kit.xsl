<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://www.openarchives.org/OAI/2.0/"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="description"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="format"/>
      <xsl:call-template name="formatDetails"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="type"/>
      <xsl:call-template name="warning"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="description">
    <field name="description">
      <xsl:value-of select="//src:datafield[@tag='520']/src:subfield[@code='a']" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Work</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="format">
    <field name="format">
      <xsl:value-of select="//src:datafield[@tag='300']/src:subfield[@code='a']" />
    </field>
  </xsl:template>

  <xsl:template name="formatDetails">
    <field name="formatDetails">
      <xsl:value-of select="//src:datafield[@tag='505']/src:subfield[@code='a']" />
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//src:datafield[@tag='245']/src:subfield[@code='a']" />
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>MURA</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>Mura® the AIATSIS collections catalogue</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//src:controlfield[@tag='001']" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:value-of select="//src:datafield[@tag='856'][last()]/src:subfield[@code='u']" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>http://www.aiatsis.gov.au/collections/muraread.html</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="type">
    <field name="type">
      <xsl:text>Kit</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="warning">
    <field name="warning">
      <xsl:text>Please be aware that some records contain language, words or descriptions which may be considered offensive or distressing. These words reflect the attitude of the period in which the item was written and/or that of its author. Please also be aware that records may contain references to deceased people which may cause sadness or distress.</xsl:text>
    </field>
  </xsl:template>

</xsl:stylesheet>
