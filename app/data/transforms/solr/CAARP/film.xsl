<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://caarp.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="creationDate"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="releaseDate"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="type"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="creationDate">
    <field name="creationDate">
      <xsl:value-of select="//src:film/src:production_year" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Work</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//src:film/src:film_title/src:title" />
    </field>
  </xsl:template>

  <xsl:template name="releaseDate">
    <field name="releaseDate">
      <xsl:value-of select="//src:film/src:min_screening_date" />
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>CAARP</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>Cinema and Audiences Research Project</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//src:film/src:id" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:text>http://caarp.edu.au/film/view/</xsl:text>
      <xsl:value-of select="//src:film/src:id" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>http://caarp.edu.au/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="type">
    <field name="type">
      <xsl:text>Film</xsl:text>
    </field>
  </xsl:template>

</xsl:stylesheet>
