<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="NONE"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="contactDetails"><xsl:call-template name="contactDetails"/></field>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="familyName"><xsl:call-template name="familyName"/></field>
      <field name="individualName"><xsl:call-template name="individualName"/></field>
      <field name="primaryName"><xsl:call-template name="primaryName"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="title"><xsl:call-template name="title"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="contactDetails">
    <xsl:text>Email: </xsl:text>
    <xsl:value-of select="//person/EMAIL" />
  </xsl:template>
  <xsl:template name="entityType">
    <xsl:text>Person</xsl:text>
  </xsl:template>
  <xsl:template name="familyName">
    <xsl:value-of select="//person/FAMILY_NAME" />
  </xsl:template>
  <xsl:template name="individualName">
    <xsl:value-of select="//person/GIVEN_NAME" />
  </xsl:template>
  <xsl:template name="primaryName">
    <xsl:text>Y</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>AMHD</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:text>Australian Media History Database</xsl:text>
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//person/PERSON_ID" />
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http://amhd.info/</xsl:text>
  </xsl:template>
  <xsl:template name="title">
    <xsl:value-of select="//person/PERSONS_TITLE" />
  </xsl:template>

</xsl:stylesheet>
