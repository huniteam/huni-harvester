<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="NONE"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="contactDetails"><xsl:call-template name="contactDetails"/></field>
      <field name="description"><xsl:call-template name="description"/></field>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="format"><xsl:call-template name="format"/></field>
      <field name="formatDetails"><xsl:call-template name="formatDetails"/></field>
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceRecordLink"><xsl:call-template name="sourceRecordLink"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="type"><xsl:call-template name="type"/></field>
      <field name="url"><xsl:call-template name="url"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="contactDetails">
    <xsl:value-of select="//project/PROJECTS_EMAIL" />
  </xsl:template>
  <xsl:template name="description">
    <xsl:value-of select="//project/PROJECTS_DESCRIPTION" />
  </xsl:template>
  <xsl:template name="entityType">
    <xsl:text>Work</xsl:text>
  </xsl:template>
  <xsl:template name="format">
    <xsl:text>Media History Research Project</xsl:text>
  </xsl:template>
  <xsl:template name="formatDetails">
    <xsl:value-of select="//project/PROJECTS_RESOURCES" />
  </xsl:template>
  <xsl:template name="name">
    <xsl:value-of select="//project/PROJECTS_TITLE" />
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>AMHD</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:text>Australian Media History Database</xsl:text>
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//project/PROJECT_ID" />
  </xsl:template>
  <xsl:template name="sourceRecordLink">
    <xsl:text>http://amhd.info/search/view_entry.php?PROJECT_ID=</xsl:text>
    <xsl:value-of select="//project/PROJECT_ID" />
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http://amhd.info/</xsl:text>
  </xsl:template>
  <xsl:template name="type">
    <xsl:text>Research Project: </xsl:text>
    <xsl:value-of select="//project/PERSON_TITLE" />
    <xsl:text>: </xsl:text>
    <xsl:value-of select="//project/PROJECT_TYPE_DESCRIPTION" />
  </xsl:template>
  <xsl:template name="url">
    <xsl:value-of select="//project/PROJECTS_URL" />
  </xsl:template>

</xsl:stylesheet>
