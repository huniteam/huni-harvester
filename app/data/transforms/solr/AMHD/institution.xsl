<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="NONE"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="primaryName"><xsl:call-template name="primaryName"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="url"><xsl:call-template name="url"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="entityType">
    <xsl:text>Organisation</xsl:text>
  </xsl:template>
  <xsl:template name="name">
    <xsl:value-of select="//institution/INSTITUTION_NAME" />
  </xsl:template>
  <xsl:template name="primaryName">
    <xsl:text>Y</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>AMHD</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:text>Australian Media History Database</xsl:text>
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//institution/INSTITUTION_ID" />
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http://amhd.info/</xsl:text>
  </xsl:template>
  <xsl:template name="url">
    <xsl:value-of select="//institution/INSTITUTION_URL" />
  </xsl:template>

</xsl:stylesheet>
