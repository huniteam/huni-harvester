<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://archive.circusoz.com/huni"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="creationDate"><xsl:call-template name="creationDate"/></field>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="format"><xsl:call-template name="format"/></field>
      <field name="formatDetails"><xsl:call-template name="formatDetails"/></field>
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceRecordLink"><xsl:call-template name="sourceRecordLink"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="sourceSiteTag"><xsl:call-template name="sourceSiteTag"/></field>
      <field name="type"><xsl:call-template name="type"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="creationDate">
    <xsl:value-of select="//src:huni_videos/src:start_date" />
  </xsl:template>
  <xsl:template name="entityType">
    <xsl:text>Work</xsl:text>
  </xsl:template>
  <xsl:template name="format">
    <xsl:text>Video</xsl:text>
  </xsl:template>
  <xsl:template name="formatDetails">
    <xsl:value-of select="//src:huni_videos/src:format" />
  </xsl:template>
  <xsl:template name="name">
    <xsl:value-of select="//src:huni_videos/src:title" />
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>CircusOz</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:text>Circus Oz</xsl:text>
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//src:huni_videos/src:id" />
  </xsl:template>
  <xsl:template name="sourceRecordLink">
    <xsl:text>http://archive.circusoz.com/clips/view/</xsl:text>
    <xsl:value-of select="//src:huni_videos/src:id" />
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http:/archive.circusoz.com</xsl:text>
  </xsl:template>
  <xsl:template name="sourceSiteTag">
    <xsl:text>CircusOz video archive.</xsl:text>
  </xsl:template>
  <xsl:template name="type">
    <xsl:text>Recording of Circus Performance</xsl:text>
  </xsl:template>

</xsl:stylesheet>
