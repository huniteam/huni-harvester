<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://archive.circusoz.com/huni"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="familyName"><xsl:call-template name="familyName"/></field>
      <field name="individualName"><xsl:call-template name="individualName"/></field>
      <field name="occupation"><xsl:call-template name="occupation"/></field>
      <field name="primaryName"><xsl:call-template name="primaryName"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceRecordLink"><xsl:call-template name="sourceRecordLink"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="sourceSiteTag"><xsl:call-template name="sourceSiteTag"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="entityType">
    <xsl:text>Person</xsl:text>
  </xsl:template>
  <xsl:template name="familyName">
    <xsl:value-of select="//src:huni_performers/src:last_name" />
  </xsl:template>
  <xsl:template name="individualName">
    <xsl:value-of select="//src:huni_performers/src:first_name" />
  </xsl:template>
  <xsl:template name="occupation">
    <xsl:value-of select="//src:huni_performers/src:primary_role" />
  </xsl:template>
  <xsl:template name="primaryName">
    <xsl:text>Y</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>CircusOz</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:text>Circus Oz</xsl:text>
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//src:huni_performers/src:id" />
  </xsl:template>
  <xsl:template name="sourceRecordLink">
    <xsl:text>http://archive.circusoz.com/</xsl:text>
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http:/archive.circusoz.com</xsl:text>
  </xsl:template>
  <xsl:template name="sourceSiteTag">
    <xsl:text>CircusOz video archive.</xsl:text>
  </xsl:template>

</xsl:stylesheet>
