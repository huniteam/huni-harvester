<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://ands.org.au/standards/rif-cs/registryObjects"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="contactDetails"><xsl:call-template name="contactDetails"/></field>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="familyName"><xsl:call-template name="familyName"/></field>
      <field name="individualName"><xsl:call-template name="individualName"/></field>
      <field name="primaryName"><xsl:call-template name="primaryName"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceRecordLink"><xsl:call-template name="sourceRecordLink"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="sourceSiteTag"><xsl:call-template name="sourceSiteTag"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="contactDetails">
    <xsl:text>Postal Address: </xsl:text>
    <xsl:value-of select="//src:registryObject/src:party/src:location/src:address/src:physical[@type='postalAddress']/src:addressPart" />
  </xsl:template>
  <xsl:template name="entityType">
    <xsl:text>Person</xsl:text>
  </xsl:template>
  <xsl:template name="familyName">
    <xsl:value-of select="//src:registryObject/src:party/src:name/src:namePart[@type='family']" />
  </xsl:template>
  <xsl:template name="individualName">
    <xsl:value-of select="//src:registryObject/src:party/src:name/src:namePart[@type='given']" />
  </xsl:template>
  <xsl:template name="primaryName">
    <xsl:text>Y</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>PDSC</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:text>PARADISEC: Pacific and Regional Archive for Digital Sources in Endangered Cultures</xsl:text>
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//src:registryObject/src:party/src:identifier[@type='uri']" />
  </xsl:template>
  <xsl:template name="sourceRecordLink">
    <xsl:value-of select="//src:registryObject/src:party/src:location/src:address/src:electronic[@type='url']" />
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http://catalog.paradisec.org.au</xsl:text>
  </xsl:template>
  <xsl:template name="sourceSiteTag">
    <xsl:text>PARADISEC curates digital material about small or endangered languages.</xsl:text>
  </xsl:template>

</xsl:stylesheet>
