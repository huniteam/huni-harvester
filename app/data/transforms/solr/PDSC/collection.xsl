<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://ands.org.au/standards/rif-cs/registryObjects"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="contactDetails"><xsl:call-template name="contactDetails"/></field>
      <field name="description"><xsl:call-template name="description"/></field>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="format"><xsl:call-template name="format"/></field>
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceRecordLink"><xsl:call-template name="sourceRecordLink"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="sourceSiteTag"><xsl:call-template name="sourceSiteTag"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="contactDetails">
    <xsl:text>Postal Address: </xsl:text>
    <xsl:value-of select="//src:registryObject/src:collection/src:location/src:address/src:physical[@type='postalAddress']/src:addressPart" />
  </xsl:template>
  <xsl:template name="description">
    <xsl:value-of select="//src:registryObject/src:collection/src:description" />
  </xsl:template>
  <xsl:template name="entityType">
    <xsl:text>Work</xsl:text>
  </xsl:template>
  <xsl:template name="format">
    <xsl:text>Collection</xsl:text>
  </xsl:template>
  <xsl:template name="name">
    <xsl:value-of select="//src:registryObject/src:collection/src:name/src:namePart" />
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>PDSC</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:text>PARADISEC: Pacific and Regional Archive for Digital Sources in Endangered Cultures</xsl:text>
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//src:registryObject/src:collection/src:identifier[@type='uri']" />
  </xsl:template>
  <xsl:template name="sourceRecordLink">
    <xsl:value-of select="//src:registryObject/src:collection/src:location/src:address/src:electronic[@type='url']" />
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http://catalog.paradisec.org.au</xsl:text>
  </xsl:template>
  <xsl:template name="sourceSiteTag">
    <xsl:text>PARADISEC curates digital material about small or endangered languages.</xsl:text>
  </xsl:template>

</xsl:stylesheet>
