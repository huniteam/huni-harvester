<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:dcterms="http://purl.org/dc/terms/"
    xmlns:olac="http://www.language-archives.org/OLAC/1.1/"
    xmlns:src="http://www.openarchives.org/OAI/2.0/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="creationDate"><xsl:call-template name="creationDate"/></field>
      <field name="description"><xsl:call-template name="description"/></field>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="format"><xsl:call-template name="format"/></field>
      <field name="formatDetails"><xsl:call-template name="formatDetails"/></field>
      <field name="language"><xsl:call-template name="language"/></field>
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceRecordLink"><xsl:call-template name="sourceRecordLink"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="sourceSiteTag"><xsl:call-template name="sourceSiteTag"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="creationDate">
    <xsl:value-of select="//src:record/src:metadata/olac:olac/dcterms:created" />
  </xsl:template>
  <xsl:template name="description">
    <xsl:value-of select="//src:record/src:metadata/olac:olac/dc:description" />
  </xsl:template>
  <xsl:template name="entityType">
    <xsl:text>Work</xsl:text>
  </xsl:template>
  <xsl:template name="format">
    <xsl:value-of select="//src:record/src:metadata/olac:olac/dc:type" />
  </xsl:template>
  <xsl:template name="formatDetails">
    <xsl:value-of select="//src:record/src:metadata/olac:olac/dc:format" />
  </xsl:template>
  <xsl:template name="language">
    <xsl:value-of select="//src:record/src:metadata/olac:olac/dc:language/@olac:code" />
  </xsl:template>
  <xsl:template name="name">
    <xsl:value-of select="//src:record/src:metadata/olac:olac/dc:title" />
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>PDSC</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:text>PARADISEC: Pacific and Regional Archive for Digital Sources in Endangered Cultures</xsl:text>
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//src:record/src:metadata/olac:olac/dc:identifier" />
  </xsl:template>
  <xsl:template name="sourceRecordLink">
    <xsl:value-of select="//src:record/src:metadata/olac:olac/dc:identifier[@xsi:type='dcterms:URI']" />
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http://catalog.paradisec.org.au</xsl:text>
  </xsl:template>
  <xsl:template name="sourceSiteTag">
    <xsl:text>PARADISEC curates digital material about small or endangered languages.</xsl:text>
  </xsl:template>

</xsl:stylesheet>
