<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://www.bonzadb.com.au/static/huni"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="country"><xsl:call-template name="country"/></field>
      <field name="endDate"><xsl:call-template name="endDate"/></field>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="locality"><xsl:call-template name="locality"/></field>
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="postcode"><xsl:call-template name="postcode"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceRecordLink"><xsl:call-template name="sourceRecordLink"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="sourceSiteTag"><xsl:call-template name="sourceSiteTag"/></field>
      <field name="startDate"><xsl:call-template name="startDate"/></field>
      <field name="state"><xsl:call-template name="state"/></field>
      <field name="type"><xsl:call-template name="type"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="country">
    <xsl:value-of select="//src:huni_cinema/src:country" />
  </xsl:template>
  <xsl:template name="endDate">
    <xsl:value-of select="//src:huni_cinema/src:date_finished" />
  </xsl:template>
  <xsl:template name="entityType">
    <xsl:text>Place</xsl:text>
  </xsl:template>
  <xsl:template name="locality">
    <xsl:value-of select="//src:huni_cinema/src:suburb" />
  </xsl:template>
  <xsl:template name="name">
    <xsl:value-of select="//src:huni_cinema/src:name" />
  </xsl:template>
  <xsl:template name="postcode">
    <xsl:value-of select="//src:huni_cinema/src:postcode" />
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>Bonza</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:text>BONZA: National Cinema and Television Database</xsl:text>
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//src:huni_cinema/src:id" />
  </xsl:template>
  <xsl:template name="sourceRecordLink">
    <xsl:text>http://www.bonzadb.com.au/cinema/view/</xsl:text>
    <xsl:value-of select="//src:huni_cinema/src:id" />
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http://www.bonzadb.com.au/</xsl:text>
  </xsl:template>
  <xsl:template name="sourceSiteTag">
    <xsl:text>An online collection of national cinema databases (to date French, Australian and New Zealand) prepared by film researchers and students.</xsl:text>
  </xsl:template>
  <xsl:template name="startDate">
    <xsl:value-of select="//src:huni_cinema/src:date_started" />
  </xsl:template>
  <xsl:template name="state">
    <xsl:value-of select="//src:huni_cinema/src:state" />
  </xsl:template>
  <xsl:template name="type">
    <xsl:text>Cinema</xsl:text>
  </xsl:template>

</xsl:stylesheet>
