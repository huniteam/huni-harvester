<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://www.bonzadb.com.au/static/huni"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="description"><xsl:call-template name="description"/></field>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceRecordLink"><xsl:call-template name="sourceRecordLink"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="sourceSiteTag"><xsl:call-template name="sourceSiteTag"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="description">
    <xsl:value-of select="//src:huni_company/src:description" />
  </xsl:template>
  <xsl:template name="entityType">
    <xsl:text>Organisation</xsl:text>
  </xsl:template>
  <xsl:template name="name">
    <xsl:value-of select="//src:huni_company/src:name" />
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>Bonza</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:text>BONZA: National Cinema and Television Database</xsl:text>
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//src:huni_company/src:id" />
  </xsl:template>
  <xsl:template name="sourceRecordLink">
    <xsl:text>http://www.bonzadb.com.au/company/view/</xsl:text>
    <xsl:value-of select="//src:huni_company/src:id" />
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http://www.bonzadb.com.au/</xsl:text>
  </xsl:template>
  <xsl:template name="sourceSiteTag">
    <xsl:text>An online collection of national cinema databases (to date French, Australian and New Zealand) prepared by film researchers and students.</xsl:text>
  </xsl:template>

</xsl:stylesheet>
