<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://www.bonzadb.com.au/static/huni"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="biography"><xsl:call-template name="biography"/></field>
      <field name="birthDate"><xsl:call-template name="birthDate"/></field>
      <field name="deathDate"><xsl:call-template name="deathDate"/></field>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="familyName"><xsl:call-template name="familyName"/></field>
      <field name="individualName"><xsl:call-template name="individualName"/></field>
      <field name="primaryName"><xsl:call-template name="primaryName"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceRecordLink"><xsl:call-template name="sourceRecordLink"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="sourceSiteTag"><xsl:call-template name="sourceSiteTag"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="biography">
    <xsl:value-of select="//src:huni_person/src:description" />
  </xsl:template>
  <xsl:template name="birthDate">
    <xsl:value-of select="//src:huni_person/src:birth_year" />
  </xsl:template>
  <xsl:template name="deathDate">
    <xsl:value-of select="//src:huni_person/src:death_year" />
  </xsl:template>
  <xsl:template name="entityType">
    <xsl:text>Person</xsl:text>
  </xsl:template>
  <xsl:template name="familyName">
    <xsl:value-of select="//src:huni_person/src:family_name" />
  </xsl:template>
  <xsl:template name="individualName">
    <xsl:value-of select="//src:huni_person/src:first_name" />
  </xsl:template>
  <xsl:template name="primaryName">
    <xsl:text>Y</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>Bonza</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:text>BONZA: National Cinema and Television Database</xsl:text>
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//src:huni_person/src:id" />
  </xsl:template>
  <xsl:template name="sourceRecordLink">
    <xsl:text>http://www.bonzadb.com.au/person/view/</xsl:text>
    <xsl:value-of select="//src:huni_person/src:id" />
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http://www.bonzadb.com.au/</xsl:text>
  </xsl:template>
  <xsl:template name="sourceSiteTag">
    <xsl:text>An online collection of national cinema databases (to date French, Australian and New Zealand) prepared by film researchers and students.</xsl:text>
  </xsl:template>

</xsl:stylesheet>
