<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://www.ausstage.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="contactDetails"/>
      <xsl:call-template name="description"/>
      <xsl:call-template name="endDate"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="primaryName"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="startDate"/>
      <xsl:call-template name="url"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="contactDetails">
    <field name="contactDetails">
      <xsl:text>Contact: </xsl:text>
      <xsl:value-of select="//src:huni_organisation/src:contact" />
      <xsl:text> Email: </xsl:text>
      <xsl:value-of select="//src:huni_organisation/src:email" />
      <xsl:text> Phone: </xsl:text>
      <xsl:value-of select="//src:huni_organisation/src:phone1" />
      <xsl:text>,</xsl:text>
      <xsl:value-of select="//src:huni_organisation/src:phone2" />
      <xsl:text>,</xsl:text>
      <xsl:value-of select="//src:huni_organisation/src:phone3" />
      <xsl:text> Fax: </xsl:text>
      <xsl:value-of select="//src:huni_organisation/src:fax" />
    </field>
  </xsl:template>

  <xsl:template name="description">
    <field name="description">
      <xsl:value-of select="//src:huni_organisation/src:notes" />
    </field>
  </xsl:template>

  <xsl:template name="endDate">
    <field name="endDate">
      <xsl:value-of select="//src:huni_organisation/src:ddlast_date" />
      <xsl:value-of select="//src:huni_organisation/src:mmlast_date" />
      <xsl:value-of select="//src:huni_organisation/src:yyyylast_date" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Organisation</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//src:huni_organisation/src:name" />
    </field>
  </xsl:template>

  <xsl:template name="primaryName">
    <field name="primaryName">
      <xsl:text>Y</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>AusStage</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>AusStage</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//src:huni_organisation/src:organisationid" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:text>https://www.AusStage.edu.au/pages/organisation/</xsl:text>
      <xsl:value-of select="//src:huni_organisation/src:organisationid" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>http://www.ausstage.edu.au/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="startDate">
    <field name="startDate">
      <xsl:value-of select="//src:huni_organisation/src:ddfirst_date" />
      <xsl:value-of select="//src:huni_organisation/src:mmfirst_date" />
      <xsl:value-of select="//src:huni_organisation/src:yyyyfirst_date" />
    </field>
  </xsl:template>

  <xsl:template name="url">
    <field name="url">
      <xsl:value-of select="//src:huni_organisation/src:web_links" />
    </field>
  </xsl:template>

</xsl:stylesheet>
