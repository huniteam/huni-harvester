<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://www.ausstage.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="contactDetails"/>
      <xsl:call-template name="description"/>
      <xsl:call-template name="endDate"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="primaryName"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="startDate"/>
      <xsl:call-template name="type"/>
      <xsl:call-template name="url"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="contactDetails">
    <field name="contactDetails">
      <xsl:text>Email: </xsl:text>
      <xsl:value-of select="//src:huni_venue/src:email" />
      <xsl:text> Phone: </xsl:text>
      <xsl:value-of select="//src:huni_venue/src:phone" />
      <xsl:text> Fax: </xsl:text>
      <xsl:value-of select="//src:huni_venue/src:fax" />
    </field>
  </xsl:template>

  <xsl:template name="description">
    <field name="description">
      <xsl:value-of select="//src:huni_venue/src:notes" />
    </field>
  </xsl:template>

  <xsl:template name="endDate">
    <field name="endDate">
      <xsl:value-of select="//src:huni_venue/src:ddlast_date" />
      <xsl:value-of select="//src:huni_venue/src:mmlast_date" />
      <xsl:value-of select="//src:huni_venue/src:yyyylast_date" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Place</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//src:huni_venue/src:venue_name" />
    </field>
  </xsl:template>

  <xsl:template name="primaryName">
    <field name="primaryName">
      <xsl:text>Y</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>AusStage</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>AusStage</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//src:huni_venue/src:venueid" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:text>http://www.AusStage.edu.au/pages/venue/</xsl:text>
      <xsl:value-of select="//src:huni_venue/src:venueid" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>http://www.ausstage.edu.au/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="startDate">
    <field name="startDate">
      <xsl:value-of select="//src:huni_venue/src:ddfirst_date" />
      <xsl:value-of select="//src:huni_venue/src:mmfirst_date" />
      <xsl:value-of select="//src:huni_venue/src:yyyyfirst_date" />
    </field>
  </xsl:template>

  <xsl:template name="type">
    <field name="type">
      <xsl:text>Venue</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="url">
    <field name="url">
      <xsl:value-of select="//src:huni_venue/src:web_links" />
    </field>
  </xsl:template>

</xsl:stylesheet>
