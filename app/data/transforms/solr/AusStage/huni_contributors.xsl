<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://www.ausstage.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="birthDate"/>
      <xsl:call-template name="deathDate"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="ethnicity"/>
      <xsl:call-template name="familyName"/>
      <xsl:call-template name="individualName"/>
      <xsl:call-template name="primaryName"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="birthDate">
    <field name="birthDate">
      <xsl:value-of select="//src:huni_contributors/src:date_of_birth" />
    </field>
  </xsl:template>

  <xsl:template name="deathDate">
    <field name="deathDate">
      <xsl:value-of select="//src:huni_contributors/src:date_of_death" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Person</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="ethnicity">
    <field name="ethnicity">
      <xsl:value-of select="//src:huni_contributors/src:countryname" />
    </field>
  </xsl:template>

  <xsl:template name="familyName">
    <field name="familyName">
      <xsl:value-of select="//src:huni_contributors/src:last_name" />
    </field>
  </xsl:template>

  <xsl:template name="individualName">
    <field name="individualName">
      <xsl:value-of select="//src:huni_contributors/src:first_name" />
    </field>
  </xsl:template>

  <xsl:template name="primaryName">
    <field name="primaryName">
      <xsl:text>Y</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>AusStage</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>AusStage</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//src:huni_contributors/src:contributorid" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:text>http://www.ausstage.edu.au/pages/contributor/</xsl:text>
      <xsl:value-of select="//src:huni_contributors/src:contributorid" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>http://www.ausstage.edu.au/</xsl:text>
    </field>
  </xsl:template>

</xsl:stylesheet>
