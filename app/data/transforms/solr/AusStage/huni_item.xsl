<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://www.ausstage.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="description"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="format"/>
      <xsl:call-template name="formatDetails"/>
      <xsl:call-template name="language"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="primaryTitle"/>
      <xsl:call-template name="releaseDate"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="url"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="description">
    <field name="description">
      <xsl:value-of select="//src:huni_item/src:item_description_abstract" />
      <xsl:text> </xsl:text>
      <xsl:value-of select="//src:huni_item/src:item_description" />
      <xsl:text> </xsl:text>
      <xsl:value-of select="//src:huni_item/src:detail_comments" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Work</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="format">
    <field name="format">
      <xsl:value-of select="//src:huni_item/src:item_type" />
    </field>
  </xsl:template>

  <xsl:template name="formatDetails">
    <field name="formatDetails">
      <xsl:value-of select="//src:huni_item/src:item_sub_type" />
    </field>
  </xsl:template>

  <xsl:template name="language">
    <field name="language">
      <xsl:value-of select="//src:huni_item/src:language" />
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//src:huni_item/src:title" />
    </field>
  </xsl:template>

  <xsl:template name="primaryTitle">
    <field name="primaryTitle">
      <xsl:text>Y</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="releaseDate">
    <field name="releaseDate">
      <xsl:value-of select="//src:huni_item/src:issued_date" />
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>AusStage</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>AusStage</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//src:huni_item/src:itemid" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:text>https://www.ausstage.edu.au/pages/resource/</xsl:text>
      <xsl:value-of select="//src:huni_item/src:itemid" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>http://www.ausstage.edu.au/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="url">
    <field name="url">
      <xsl:value-of select="//src:huni_item/src:item_url" />
    </field>
  </xsl:template>

</xsl:stylesheet>
