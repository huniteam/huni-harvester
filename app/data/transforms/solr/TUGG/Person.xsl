<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://heuristnetwork.org"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="familyName"/>
      <xsl:call-template name="gender"/>
      <xsl:call-template name="givenName"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="sourceSiteTag"/>
      <xsl:call-template name="title"/>
      <xsl:call-template name="warning"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Person</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="familyName">
    <field name="familyName">
      <xsl:value-of select="//src:record/src:detail[@name='Family name']" />
    </field>
  </xsl:template>

  <xsl:template name="gender">
    <field name="gender">
      <xsl:value-of select="//src:record/src:detail[@name='Birth Gender']" />
    </field>
  </xsl:template>

  <xsl:template name="givenName">
    <field name="givenName">
      <xsl:value-of select="//src:record/src:detail[@name='Given name(s)']" />
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>TUGG</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>TUGG</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//src:record/src:id" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:value-of select="//src:record/src:url" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>http://tugg.me/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteTag">
    <field name="sourceSiteTag">
      <xsl:value-of select="//src:record/src:detail[@name='Source of data']" />
    </field>
  </xsl:template>

  <xsl:template name="title">
    <field name="title">
      <xsl:value-of select="//src:record/src:detail[@name='Honorific/src:Title']" />
    </field>
  </xsl:template>

  <xsl:template name="warning">
    <field name="warning">
      <xsl:text>Published to HuNI through the &lt;a href='http://HeuristNetwork.org'&gt;Heurist Humanities eResearch database tool&lt;/a&gt;</xsl:text>
    </field>
  </xsl:template>

</xsl:stylesheet>
