<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://heuristnetwork.org"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="country"/>
      <xsl:call-template name="endDate"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="locality"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="postcode"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="sourceSiteTag"/>
      <xsl:call-template name="startDate"/>
      <xsl:call-template name="state"/>
      <xsl:call-template name="type"/>
      <xsl:call-template name="warning"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="country">
    <field name="country">
      <xsl:value-of select="//src:record/src:detail[@name='Country']" />
    </field>
  </xsl:template>

  <xsl:template name="endDate">
    <field name="endDate">
      <xsl:value-of select="//src:record/src:detail[@name='End date']/src:raw" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Place</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="locality">
    <field name="locality">
      <xsl:value-of select="//src:record/src:detail[@name='Locality, suburb or town']" />
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//src:record/src:detail[@name='Name of place']" />
    </field>
  </xsl:template>

  <xsl:template name="postcode">
    <field name="postcode">
      <xsl:value-of select="//src:record/src:detail[@name='Postcode']" />
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>TUGG</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>TUGG</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//src:record/src:id" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:value-of select="//src:record/src:url" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>http://tugg.me/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteTag">
    <field name="sourceSiteTag">
      <xsl:value-of select="//src:record/src:detail[@name='Source of data']" />
    </field>
  </xsl:template>

  <xsl:template name="startDate">
    <field name="startDate">
      <xsl:value-of select="//src:record/src:detail[@name='Start date']/src:raw" />
    </field>
  </xsl:template>

  <xsl:template name="state">
    <field name="state">
      <xsl:value-of select="//src:record/src:detail[@name='State']" />
    </field>
  </xsl:template>

  <xsl:template name="type">
    <field name="type">
      <xsl:value-of select="//src:record/src:detail[@name='Type of Place']" />
    </field>
  </xsl:template>

  <xsl:template name="warning">
    <field name="warning">
      <xsl:text>Published to HuNI through the &lt;a href='http://HeuristNetwork.org'&gt;Heurist Humanities eResearch database tool&lt;/a&gt;</xsl:text>
    </field>
  </xsl:template>

</xsl:stylesheet>
