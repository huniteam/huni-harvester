<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="NONE"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="contactDetails"/>
      <xsl:call-template name="description"/>
      <xsl:call-template name="endDate"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="format"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="startDate"/>
      <xsl:call-template name="type"/>
      <xsl:call-template name="url"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="contactDetails">
    <field name="contactDetails">
      <xsl:text>Contact Name: </xsl:text>
      <xsl:value-of select="//collection/location/contact_name" />
      <xsl:text> Email: </xsl:text>
      <xsl:value-of select="//collection/location/email" />
      <xsl:text> Phone: </xsl:text>
      <xsl:value-of select="//collection/location/phone" />
      <xsl:text> Address: </xsl:text>
      <xsl:value-of select="//collection/location/address" />
      <xsl:text> Access for Researchers: </xsl:text>
      <xsl:value-of select="//collection/access_for_researchers" />
    </field>
  </xsl:template>

  <xsl:template name="description">
    <field name="description">
      <xsl:value-of select="//collection/description" />
      <xsl:text> </xsl:text>
      <xsl:value-of select="//collection/comments" />
      <xsl:text> </xsl:text>
      <xsl:value-of select="//collection/keywords" />
    </field>
  </xsl:template>

  <xsl:template name="endDate">
    <field name="endDate">
      <xsl:value-of select="//collection/end_date" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Work</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="format">
    <field name="format">
      <xsl:text>Research Collection</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//collection/title" />
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>MAP</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>Media Archives Project</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//collection/id" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:text>https://mediaarchivesproject.mq.edu.au/redbox/default/detail/</xsl:text>
      <xsl:value-of select="//collection/id" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>https://mediaarchivesproject.mq.edu.au/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="startDate">
    <field name="startDate">
      <xsl:value-of select="//collection/start_date" />
    </field>
  </xsl:template>

  <xsl:template name="type">
    <field name="type">
      <xsl:text>Research Collection: Media: </xsl:text>
      <xsl:value-of select="//collection/media/type" />
    </field>
  </xsl:template>

  <xsl:template name="url">
    <field name="url">
      <xsl:value-of select="//collection/location/website1" />
    </field>
  </xsl:template>

</xsl:stylesheet>
