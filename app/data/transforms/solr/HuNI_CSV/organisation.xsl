<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="NONE"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="associatedWithOccupation"/>
      <xsl:call-template name="contactDetails"/>
      <xsl:call-template name="description"/>
      <xsl:call-template name="endDate"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="function"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="primaryName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="startDate"/>
      <xsl:call-template name="type"/>
      <xsl:call-template name="url"/>
      <xsl:call-template name="warning"/>
      <xsl:call-template name="workCount"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="associatedWithOccupation">
    <field name="associatedWithOccupation">
      <xsl:value-of select="//record/associatedWithOccupation" />
    </field>
  </xsl:template>

  <xsl:template name="contactDetails">
    <field name="contactDetails">
      <xsl:value-of select="//record/contactDetails" />
    </field>
  </xsl:template>

  <xsl:template name="description">
    <field name="description">
      <xsl:value-of select="//record/description" />
    </field>
  </xsl:template>

  <xsl:template name="endDate">
    <field name="endDate">
      <xsl:value-of select="//record/endDate" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Organisation</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="function">
    <field name="function">
      <xsl:value-of select="//record/function" />
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//record/name" />
    </field>
  </xsl:template>

  <xsl:template name="primaryName">
    <field name="primaryName">
      <xsl:value-of select="//record/primaryName" />
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//record/id" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:value-of select="//record/sourceRecordLink" />
    </field>
  </xsl:template>

  <xsl:template name="startDate">
    <field name="startDate">
      <xsl:value-of select="//record/startDate" />
    </field>
  </xsl:template>

  <xsl:template name="type">
    <field name="type">
      <xsl:value-of select="//record/type" />
    </field>
  </xsl:template>

  <xsl:template name="url">
    <field name="url">
      <xsl:value-of select="//record/url" />
    </field>
  </xsl:template>

  <xsl:template name="warning">
    <field name="warning">
      <xsl:value-of select="//record/warning" />
    </field>
  </xsl:template>

  <xsl:template name="workCount">
    <field name="workCount">
      <xsl:value-of select="//record/workCount" />
    </field>
  </xsl:template>

</xsl:stylesheet>
