<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="NONE"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="biography"/>
      <xsl:call-template name="birthDate"/>
      <xsl:call-template name="contactDetails"/>
      <xsl:call-template name="culturalHeritage"/>
      <xsl:call-template name="deathDate"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="ethnicity"/>
      <xsl:call-template name="familyName"/>
      <xsl:call-template name="gender"/>
      <xsl:call-template name="individualName"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="occupation"/>
      <xsl:call-template name="primaryName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="title"/>
      <xsl:call-template name="warning"/>
      <xsl:call-template name="workCount"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="biography">
    <field name="biography">
      <xsl:value-of select="//record/biography" />
    </field>
  </xsl:template>

  <xsl:template name="birthDate">
    <field name="birthDate">
      <xsl:value-of select="//record/birthDate" />
    </field>
  </xsl:template>

  <xsl:template name="contactDetails">
    <field name="contactDetails">
      <xsl:value-of select="//record/contactDetails" />
    </field>
  </xsl:template>

  <xsl:template name="culturalHeritage">
    <field name="culturalHeritage">
      <xsl:value-of select="//record/culturalHeritage" />
    </field>
  </xsl:template>

  <xsl:template name="deathDate">
    <field name="deathDate">
      <xsl:value-of select="//record/deathDate" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Person</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="ethnicity">
    <field name="ethnicity">
      <xsl:value-of select="//record/ethnicity" />
    </field>
  </xsl:template>

  <xsl:template name="familyName">
    <field name="familyName">
      <xsl:value-of select="//record/familyName" />
    </field>
  </xsl:template>

  <xsl:template name="gender">
    <field name="gender">
      <xsl:value-of select="//record/gender" />
    </field>
  </xsl:template>

  <xsl:template name="individualName">
    <field name="individualName">
      <xsl:value-of select="//record/individualName" />
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//record/name" />
    </field>
  </xsl:template>

  <xsl:template name="occupation">
    <field name="occupation">
      <xsl:value-of select="//record/occupation" />
    </field>
  </xsl:template>

  <xsl:template name="primaryName">
    <field name="primaryName">
      <xsl:value-of select="//record/primaryName" />
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//record/id" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:value-of select="//record/sourceRecordLink" />
    </field>
  </xsl:template>

  <xsl:template name="title">
    <field name="title">
      <xsl:value-of select="//record/title" />
    </field>
  </xsl:template>

  <xsl:template name="warning">
    <field name="warning">
      <xsl:value-of select="//record/warning" />
    </field>
  </xsl:template>

  <xsl:template name="workCount">
    <field name="workCount">
      <xsl:value-of select="//record/workCount" />
    </field>
  </xsl:template>

</xsl:stylesheet>
