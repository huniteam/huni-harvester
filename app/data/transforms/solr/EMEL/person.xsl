<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="urn:isbn:1-931666-33-4"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="birthDate"/>
      <xsl:call-template name="deathDate"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="familyName"/>
      <xsl:call-template name="individualName"/>
      <xsl:call-template name="occupation"/>
      <xsl:call-template name="primaryName"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="sourceSiteTag"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="birthDate">
    <field name="birthDate">
      <xsl:value-of select="//src:cpfDescription/src:description/src:existDates/src:dateRange/src:fromDate[@standardDate]" />
    </field>
  </xsl:template>

  <xsl:template name="deathDate">
    <field name="deathDate">
      <xsl:value-of select="//src:cpfDescription/src:description/src:existDates/src:dateRange/src:toDate[@standardDate]" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Person</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="familyName">
    <field name="familyName">
      <xsl:value-of select="//src:cpfDescription/src:identity/src:nameEntry/src:part[@localType='familyname']" />
    </field>
  </xsl:template>

  <xsl:template name="individualName">
    <field name="individualName">
      <xsl:value-of select="//src:cpfDescription/src:identity/src:nameEntry/src:part[@localType='givenname']" />
    </field>
  </xsl:template>

  <xsl:template name="occupation">
    <field name="occupation">
      <xsl:value-of select="//src:cpfDescription/src:description/src:occupations/src:occupation/src:term" />
    </field>
  </xsl:template>

  <xsl:template name="primaryName">
    <field name="primaryName">
      <xsl:text>Y</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>EMEL</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:value-of select="//src:control/src:maintenanceAgency/src:agencyName" />
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//src:control/src:recordId" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:value-of select="//src:cpfDescription/src:identity/src:entityId" />
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>http://www.emelbourne.net.au</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteTag">
    <field name="sourceSiteTag">
      <xsl:text>The Encyclopedia is an A to Z reference work covering the city's history from pre-European settlement up to the present day.</xsl:text>
    </field>
  </xsl:template>

</xsl:stylesheet>
