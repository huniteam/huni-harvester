<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="urn:isbn:1-931666-33-4"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="description"><xsl:call-template name="description"/></field>
      <field name="endDate"><xsl:call-template name="endDate"/></field>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceRecordLink"><xsl:call-template name="sourceRecordLink"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="startDate"><xsl:call-template name="startDate"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="description">
    <xsl:value-of select="//src:cpfDescription/src:description/src:biogHist" />
  </xsl:template>
  <xsl:template name="endDate">
    <xsl:value-of select="//src:cpfDescription/src:description/src:existDates/src:dateRange/src:toDate[@standardDate]" />
  </xsl:template>
  <xsl:template name="entityType">
    <xsl:text>Place</xsl:text>
  </xsl:template>
  <xsl:template name="name">
    <xsl:value-of select="//src:cpfDescription/src:identity/src:nameEntry/src:part" />
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>GOLD</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:value-of select="//src:control/src:maintenanceAgency/src:agencyName" />
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//src:control/src:recordId" />
  </xsl:template>
  <xsl:template name="sourceRecordLink">
    <xsl:value-of select="//src:cpfDescription/src:identity/src:entityId" />
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http://www.egold.net.au/</xsl:text>
  </xsl:template>
  <xsl:template name="startDate">
    <xsl:value-of select="//src:cpfDescription/src:description/src:existDates/src:dateRange/src:fromDate[@standardDate]" />
  </xsl:template>

</xsl:stylesheet>
