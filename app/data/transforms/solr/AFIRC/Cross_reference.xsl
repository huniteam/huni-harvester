<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://www.inmagic.com/webpublisher/query"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <xsl:call-template name="description"/>
      <xsl:call-template name="entityType"/>
      <xsl:call-template name="formatDetails"/>
      <xsl:call-template name="isbn"/>
      <xsl:call-template name="issn"/>
      <xsl:call-template name="language"/>
      <xsl:call-template name="name"/>
      <xsl:call-template name="releaseDate"/>
      <xsl:call-template name="sourceAgencyCode"/>
      <xsl:call-template name="sourceAgencyName"/>
      <xsl:call-template name="sourceID"/>
      <xsl:call-template name="sourceRecordLink"/>
      <xsl:call-template name="sourceSiteAddress"/>
      <xsl:call-template name="subTitle"/>
      <xsl:call-template name="type"/>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="description">
    <field name="description">
      <xsl:value-of select="//src:Record/src:Cross-Reference" />
    </field>
  </xsl:template>

  <xsl:template name="entityType">
    <field name="entityType">
      <xsl:text>Work</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="formatDetails">
    <field name="formatDetails">
      <xsl:value-of select="//src:Record/src:PhysDes" />
    </field>
  </xsl:template>

  <xsl:template name="isbn">
    <field name="isbn">
      <xsl:value-of select="//src:Record/src:ISBN" />
    </field>
  </xsl:template>

  <xsl:template name="issn">
    <field name="issn">
      <xsl:value-of select="//src:Record/src:ISSN" />
    </field>
  </xsl:template>

  <xsl:template name="language">
    <field name="language">
      <xsl:value-of select="//src:Record/src:Language" />
    </field>
  </xsl:template>

  <xsl:template name="name">
    <field name="name">
      <xsl:value-of select="//src:Record/src:Title" />
    </field>
  </xsl:template>

  <xsl:template name="releaseDate">
    <field name="releaseDate">
      <xsl:value-of select="//src:Record/src:PubDate" />
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyCode">
    <field name="sourceAgencyCode">
      <xsl:text>AFIRC</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceAgencyName">
    <field name="sourceAgencyName">
      <xsl:text>Australian Film Institute Research Centre</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceID">
    <field name="sourceID">
      <xsl:value-of select="//src:Record/src:ID" />
    </field>
  </xsl:template>

  <xsl:template name="sourceRecordLink">
    <field name="sourceRecordLink">
      <xsl:text>https://afiresearch.rmit.edu.au/search.php</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="sourceSiteAddress">
    <field name="sourceSiteAddress">
      <xsl:text>https://afiresearch.rmit.edu.au/</xsl:text>
    </field>
  </xsl:template>

  <xsl:template name="subTitle">
    <field name="subTitle">
      <xsl:value-of select="//src:Record/src:Subtitle" />
    </field>
  </xsl:template>

  <xsl:template name="type">
    <field name="type">
      <xsl:value-of select="//src:Record/src:Record-Type" />
    </field>
  </xsl:template>

</xsl:stylesheet>
