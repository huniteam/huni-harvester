<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="urn:isbn:1-931666-33-4"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
  <add>
    <doc>
      <field name="biography"><xsl:call-template name="biography"/></field>
      <field name="birthDate"><xsl:call-template name="birthDate"/></field>
      <field name="deathDate"><xsl:call-template name="deathDate"/></field>
      <field name="entityType"><xsl:call-template name="entityType"/></field>
      <field name="familyName"><xsl:call-template name="familyName"/></field>
      <field name="individualName"><xsl:call-template name="individualName"/></field>
      <field name="occupation"><xsl:call-template name="occupation"/></field>
      <field name="primaryName"><xsl:call-template name="primaryName"/></field>
      <field name="sourceAgencyCode"><xsl:call-template name="sourceAgencyCode"/></field>
      <field name="sourceAgencyName"><xsl:call-template name="sourceAgencyName"/></field>
      <field name="sourceID"><xsl:call-template name="sourceID"/></field>
      <field name="sourceRecordLink"><xsl:call-template name="sourceRecordLink"/></field>
      <field name="sourceSiteAddress"><xsl:call-template name="sourceSiteAddress"/></field>
      <field name="sourceSiteTag"><xsl:call-template name="sourceSiteTag"/></field>
      <field name="title"><xsl:call-template name="title"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name="biography">
    <xsl:value-of select="//src:cpfDescription/src:description/src:biogHist/src:abstract" />
  </xsl:template>
  <xsl:template name="birthDate">
    <xsl:value-of select="//src:cpfDescription/src:description/src:existDates/src:dateRange/src:fromDate[@standardDate]" />
  </xsl:template>
  <xsl:template name="deathDate">
    <xsl:value-of select="//src:cpfDescription/src:description/src:existDates/src:dateRange/src:toDate[@standardDate]" />
  </xsl:template>
  <xsl:template name="entityType">
    <xsl:text>Person</xsl:text>
  </xsl:template>
  <xsl:template name="familyName">
    <xsl:value-of select="//src:cpfDescription/src:identity/src:nameEntry/src:part[@localType='familyname']" />
  </xsl:template>
  <xsl:template name="individualName">
    <xsl:value-of select="//src:cpfDescription/src:identity/src:nameEntry/src:part[@localType='givenname']" />
  </xsl:template>
  <xsl:template name="occupation">
    <xsl:value-of select="//src:cpfDescription/src:description/src:occupations/src:occupation/src:term" />
  </xsl:template>
  <xsl:template name="primaryName">
    <xsl:text>Y</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyCode">
    <xsl:text>FCNS</xsl:text>
  </xsl:template>
  <xsl:template name="sourceAgencyName">
    <xsl:value-of select="//src:control/src:maintenanceAgency/src:agencyName" />
  </xsl:template>
  <xsl:template name="sourceID">
    <xsl:value-of select="//src:control/src:recordId" />
  </xsl:template>
  <xsl:template name="sourceRecordLink">
    <xsl:value-of select="//src:cpfDescription/src:identity/src:entityId" />
  </xsl:template>
  <xsl:template name="sourceSiteAddress">
    <xsl:text>http://www.findandconnect.gov.au/nsw</xsl:text>
  </xsl:template>
  <xsl:template name="sourceSiteTag">
    <xsl:text>The Find and Connect web resource is for Forgotten Australians, Former Child Migrants and everyone with an interest in the history of out-of-home 'care' in Australia.</xsl:text>
  </xsl:template>
  <xsl:template name="title">
    <xsl:value-of select="//src:cpfDescription/src:identity/src:nameEntry/src:part[starts-with(@localType,'honorific')]" />
  </xsl:template>

</xsl:stylesheet>
