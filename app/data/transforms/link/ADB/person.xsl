<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:src="urn:isbn:1-931666-33-4"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
    <links>
      <link>
        <to>
          <provider>
            OA
          </provider>
          <id>
            <xsl:value-of select="//src:control/src:recordId" />
          </id>
        </to>
      </link>
    </links>
  </xsl:template>

</xsl:stylesheet>
