<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://ands.org.au/standards/rif-cs/registryObjects"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
    <links>
      <xsl:call-template name="relatedObject"/>
    </links>
  </xsl:template>

  <xsl:template name="relatedObject">
    <xsl:for-each select="//src:activity/src:relatedObject">
      <link>
        <relationship>
          <xsl:value-of select="./src:relation/@type" />
        </relationship>
        <to>
          <provider>
            DAAO
          </provider>
          <id>
            <xsl:value-of select="./src:key" />
          </id>
        </to>
      </link>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
