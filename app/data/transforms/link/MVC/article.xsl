<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="http://ands.org.au/standards/rif-cs/registryObjects"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
    <links>
      <xsl:call-template name="relatedArticles"/>
      <xsl:call-template name="relatedItems"/>
    </links>
  </xsl:template>

  <xsl:template name="relatedArticles">
    <xsl:for-each select="/record/relatedArticleIds/element">
      <link>
        <to>
          <provider>MVC</provider>
          <type>article</type>
          <id><xsl:value-of select="substring-after(., '/')" /></id>
        </to>
      </link>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="relatedItems">
    <xsl:for-each select="/record/relatedItemIds/element">
      <link>
        <to>
          <provider>MVC</provider>
          <type>item</type>
          <id><xsl:value-of select="substring-after(., '/')" /></id>
        </to>
      </link>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
