<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:src="urn:isbn:1-931666-33-4"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
    <links>
      <xsl:call-template name="cpfRelation"/>
    </links>
  </xsl:template>

  <xsl:template name="cpfRelation">
    <xsl:for-each select="//src:cpfDescription/src:relations/src:cpfRelation[src:relationEntry/@localType]">
      <link>
        <to>
          <feed>
            DAAO-eac-cpf
          </feed>
          <id>
            <xsl:value-of select="./src:relationEntry/@localType" />
          </id>
        </to>
        <synopsis>
          <xsl:value-of select="./src:descriptiveNote/src:p" />
        </synopsis>
      </link>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
