This directory contains a series of revisions of the example csv feed.

One dated directory should be symlinked to csv - it's the csv directory which gets served up by the example-csv-feed service.

You can use tools/test-huni-csv-daily-update to iterate through those feeds.

Note: not all the csv files pass. This is deliberate, to show how bad or missing csv files are handled.
