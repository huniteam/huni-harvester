-- Verify huni-harvest:create-harvest-status-type-table on pg

BEGIN;

SELECT
    id
  FROM harvest.harvest_status_type
  WHERE false;

ROLLBACK;
