-- Verify huni-harvest:create-harvest-table on pg

BEGIN;

SELECT
    feed_id,
    date,
    status_id
  FROM harvest.harvest
  WHERE false;

ROLLBACK;
