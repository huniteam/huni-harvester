-- Verify huni-harvest:add-links-column-to-feed-table on pg

BEGIN;

SELECT -- may as well check that we haven't deleted any columns
    id,
    provider_id,
    feed_type_id,
    preprocess_type_id,
    is_enabled,
    params,
    links_config
  FROM harvest.feed
 WHERE false;

ROLLBACK;
