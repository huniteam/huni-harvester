-- Verify huni-harvest:create-feed-table on pg

BEGIN;

SELECT
    id,
    provider_id,
    feed_type_id,
    preprocess_type_id,
    is_enabled,
    params
  FROM harvest.feed
  WHERE false;

ROLLBACK;
