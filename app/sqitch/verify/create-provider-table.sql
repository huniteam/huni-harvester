-- Verify huni-harvest:create-provider-table on pg

BEGIN;

SELECT
    id,
    name
  FROM harvest.provider
  WHERE false;

ROLLBACK;
