-- Verify huni-harvest:create-clean-table on pg

BEGIN;

SELECT
    id,
    type,
    original_name,
    feed_id,
    harvest_date,
    file_id
  FROM harvest.clean
  WHERE false;

ROLLBACK;
