-- Verify huni-harvest:create-feed-type-table on pg

BEGIN;

SELECT
    id
  FROM harvest.feed_type
  WHERE false;

ROLLBACK;
