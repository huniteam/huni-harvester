-- Verify huni-harvest:create-transform-type-table on pg

BEGIN;

SELECT id
  FROM harvest.transform_type
 WHERE false;

ROLLBACK;
