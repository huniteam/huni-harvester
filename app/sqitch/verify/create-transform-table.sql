-- Verify huni-harvest:create-transform-table on pg

BEGIN;

SELECT
    type,
    feed_id,
    file_type,
    transform
  FROM harvest.transform
 WHERE false;

ROLLBACK;
