-- Verify huni-harvest:create-harvest-schema on pg

BEGIN;

SELECT pg_catalog.has_schema_privilege('harvest', 'usage');

SELECT 1/count(*) FROM pg_extension WHERE extname = 'pgcrypto';

ROLLBACK;
