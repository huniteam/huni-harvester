-- Verify huni-harvest:create-insert-file-function on pg

BEGIN;

SELECT has_function_privilege('harvest.insert_file(BYTEA)', 'execute');

ROLLBACK;
