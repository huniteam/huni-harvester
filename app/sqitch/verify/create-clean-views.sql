-- Verify huni-harvest:create-clean-views on pg

BEGIN;

SELECT
    id,
    type,
    feed_id,
    history
  FROM harvest.clean_history
  WHERE false;

SELECT
    id,
    type,
    original_name,
    feed_id,
    harvest_date,
    file_id,
    history,
    is_latest
  FROM harvest.extended_clean
  WHERE false;

ROLLBACK;
