-- Verify huni-harvest:create-original-views on pg

BEGIN;

SELECT
    name,
    feed_id,
    history
  FROM harvest.original_history
  WHERE false;

SELECT
    name,
    feed_id,
    harvest_date,
    file_id,
    data,
    is_deleted,
    history,
    is_latest
  FROM harvest.extended_original
  WHERE false;

ROLLBACK;
