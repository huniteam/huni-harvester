-- Verify huni-harvest:create-invalid-table on pg

BEGIN;

SELECT
    original_name,
    feed_id,
    harvest_date,
    error
  FROM harvest.invalid
  WHERE false;

ROLLBACK;
