-- Verify huni-harvest:create-original-table on pg

BEGIN;

SELECT
    name,
    feed_id,
    harvest_date,
    file_id,
    data,
    is_deleted
  FROM harvest.original
  WHERE false;

ROLLBACK;
