-- Verify huni-harvest:create-preprocess-type-table on pg

BEGIN;

SELECT
    id
  FROM harvest.preprocess_type
  WHERE false;

ROLLBACK;
