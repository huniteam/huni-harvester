-- Verify huni-harvest:create-file-table on pg

BEGIN;

SELECT
    digest,
    content
  FROM harvest.file
  WHERE false;

ROLLBACK;
