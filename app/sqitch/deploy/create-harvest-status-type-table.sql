-- Deploy huni-harvest:create-harvest-status-type-table to pg
-- requires: create-harvest-schema

BEGIN;

CREATE TABLE harvest.harvest_status_type (
    id      TEXT    PRIMARY KEY
);

INSERT INTO harvest.harvest_status_type (id) VALUES
    ('in-progress'),
    ('succeeded'),
    ('failed');

COMMIT;
