-- Deploy huni-harvest:create-original-views to pg
-- requires: create-original-table

BEGIN;

-- This aggregates all versions of each original file into a single row.
CREATE VIEW harvest.original_history AS
    SELECT
        name,
        feed_id,
        json_agg(harvest_date order by harvest_date desc) as history
    FROM harvest.original
    GROUP BY name, feed_id;

-- This extends the clean table with two extra columns from clean_history.
-- Note that history is the same for all versions of each given file.
-- I'd filter out the others (eg h.history where _ <= harvest_date) but I
-- don't really know how. We can do this in the client code if necessary.
-- This view is mostly included as a convenience for DBIC code.
CREATE VIEW harvest.extended_original AS
    SELECT
        o.*,
        h.history,
        (o.harvest_date::text = h.history->>0) AS is_latest
    FROM harvest.original o
    NATURAL JOIN harvest.original_history h;


COMMIT;
