-- Deploy huni-harvest:create-transform-type-table to pg
-- requires: create-harvest-schema

BEGIN;

CREATE TABLE harvest.transform_type (
    id      TEXT    PRIMARY KEY
);

INSERT INTO harvest.transform_type (id) VALUES
    ('link'),
    ('solr');

COMMIT;
