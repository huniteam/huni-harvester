-- Deploy huni-harvest:create-guest to pg
-- requires: create-harvest-schema

-- Thanks to Tomek Paczkowski
-- https://gist.github.com/oinopion/4a207726edba8b99fd0be31cb28124d0

BEGIN;

CREATE ROLE read_access;

GRANT USAGE ON SCHEMA harvest TO read_access;
GRANT SELECT ON ALL TABLES IN SCHEMA harvest TO read_access;
ALTER DEFAULT PRIVILEGES IN SCHEMA harvest
  GRANT SELECT ON TABLES TO read_access;

CREATE USER guest WITH PASSWORD 'guest';
GRANT read_access TO guest;

COMMIT;
