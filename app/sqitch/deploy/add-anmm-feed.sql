-- Deploy huni-harvest:add-anmm-feed to pg

BEGIN;

INSERT INTO harvest.provider (id, name) VALUES
    ('ANMM', 'Australian National Maritime Museum');

INSERT INTO harvest.feed (id, provider_id, feed_type_id, preprocess_type_id, is_enabled, params) VALUES
    ('ANMM', 'ANMM', 'HuNI_CSV', 'HuNI_CSV', true,
        '{ "url":"https://raw.githubusercontent.com/HuNIVL/anmm/main/2022_feb_anmm", "index":"" }');

COMMIT;
