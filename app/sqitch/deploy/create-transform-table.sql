-- Deploy huni-harvest:create-transform-table to pg
-- requires: create-transform-type-table

BEGIN;

CREATE TABLE harvest.transform (
    type        TEXT NOT NULL REFERENCES harvest.transform_type (id),
    feed_id     TEXT NOT NULL REFERENCES harvest.feed (id),
    file_type   TEXT NOT NULL,
    transform   TEXT NOT NULL,

    PRIMARY KEY (type, feed_id, file_type)
);

COMMIT;
