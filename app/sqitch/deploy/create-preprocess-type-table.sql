-- Deploy huni-harvest:create-preprocess-type-table to pg
-- requires: create-harvest-schema

BEGIN;

CREATE TABLE harvest.preprocess_type (
    id TEXT PRIMARY KEY
);

INSERT INTO harvest.preprocess_type (id) VALUES
    ('AustLit'),
    ('DAAO'),
    ('EAC'),
    ('Heurist'),
    ('HuNI'),
    ('MARC21'),
    ('PDSC');

COMMIT;
