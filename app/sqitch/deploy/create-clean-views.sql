-- Deploy huni-harvest:create-clean-views to pg
-- requires: create-clean-table

BEGIN;

-- This aggregates all versions of each clean file into a single row.
CREATE VIEW harvest.clean_history AS
    SELECT
        id,
        type,
        feed_id,
        json_agg(harvest_date order by harvest_date desc) as history
    FROM harvest.clean
    GROUP BY id, type, feed_id;

-- This extends the clean table with two extra columns from clean_history.
-- Note that history is the same for all versions of each given file.
-- I'd filter out the others (eg h.history where _ <= harvest_date) but I
-- don't really know how. We can do this in the client code if necessary.
CREATE VIEW harvest.extended_clean AS
    SELECT
        c.id,
        c.type,
        c.original_name,
        c.feed_id,
        c.harvest_date,
        c.file_id,
        h.history,
        (c.harvest_date::text = h.history->>0) AS is_latest
    FROM harvest.clean c
    NATURAL JOIN harvest.clean_history h;

COMMIT;
