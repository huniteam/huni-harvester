-- Deploy huni-harvest:add-links-column-to-feed-table to pg
-- requires: create-feed-table

BEGIN;

ALTER TABLE harvest.feed
 ADD COLUMN links_config JSON;

COMMIT;
