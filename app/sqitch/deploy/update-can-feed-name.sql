-- Deploy huni-harvest:update-can-feed-name to pg

BEGIN;

UPDATE harvest.provider 
SET name = 'Canadian Datasets'
WHERE id  = 'CAN';

COMMIT;
