-- Deploy huni-harvest:create-harvest-table to pg
-- requires: create-feed-table

BEGIN;

CREATE TABLE harvest.harvest (
    feed_id     TEXT    NOT NULL REFERENCES harvest.feed (id),
    date        DATE    NOT NULL,
    status_id   TEXT    NOT NULL REFERENCES harvest.harvest_status_type (id),

    PRIMARY KEY (feed_id, date)
);

COMMIT;
