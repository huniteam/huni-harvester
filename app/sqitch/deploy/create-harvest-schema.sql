-- Deploy huni-harvest:create-harvest-schema to pg

BEGIN;

CREATE SCHEMA harvest;

CREATE EXTENSION pgcrypto;

COMMIT;
