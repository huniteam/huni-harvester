-- Deploy huni-harvest:add-huni-csv-preprocessor to pg

BEGIN;

INSERT INTO harvest.feed_type       (id) VALUES ('HuNI_CSV');
INSERT INTO harvest.preprocess_type (id) VALUES ('HuNI_CSV');

COMMIT;
