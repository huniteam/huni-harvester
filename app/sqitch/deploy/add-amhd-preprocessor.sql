-- Deploy huni-harvest:add-amhd-preprocessor to pg
-- requires: create-feed-table

BEGIN;

INSERT INTO harvest.preprocess_type (id) VALUES
    ('AMHD');

UPDATE harvest.feed
   SET preprocess_type_id = 'AMHD'
 WHERE id = 'AMHD';

COMMIT;
