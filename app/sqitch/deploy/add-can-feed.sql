-- Deploy huni-harvest:add-can-feed to pg

BEGIN;

INSERT INTO harvest.provider (id, name) VALUES
    ('CAN', 'Canadiana');

INSERT INTO harvest.feed (id, provider_id, feed_type_id, preprocess_type_id, is_enabled, params) VALUES
    ('CAN', 'CAN', 'HuNI_CSV', 'HuNI_CSV', true,
        '{ "url":"https://raw.githubusercontent.com/HuNIVL/canadiana/main/", "index":"" }');


COMMIT;
