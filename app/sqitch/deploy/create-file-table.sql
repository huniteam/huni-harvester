-- Deploy huni-harvest:create-file-table to pg
-- requires: create-harvest-schema

BEGIN;

CREATE TABLE harvest.file (
    digest  TEXT    PRIMARY KEY,
    content BYTEA   NOT NULL
);

COMMIT;
