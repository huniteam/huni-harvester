-- Deploy huni-harvest:create-feed-type-table to pg
-- requires: create-harvest-schema

BEGIN;

CREATE TABLE harvest.feed_type (
    id      TEXT    PRIMARY KEY
);

INSERT INTO harvest.feed_type (id) VALUES
    ('HuNI'),
    ('OAI');

COMMIT;
