-- Deploy huni-harvest:update-feed-urls-2020 to pg

BEGIN;

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://adb.anu.edu.au/oai-pmh"}'
WHERE id = 'ABD';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.mq.edu.au/research/research-centres-groups-and-facilities/resilient-societies/centres/centre-for-media-history/australian-media-history-database/researcher-databasexml"}'
WHERE id = 'AMHD';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.ausstage.edu.au/feed"}'
WHERE id = 'AusStage';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.austlit.edu.au/static/huniHarvest/agents"}'
WHERE id = 'AustLit-agents';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.austlit.edu.au/static/huniHarvest/works"}'
WHERE id = 'AustLit-works';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://oai.esrc.unimelb.edu.au/AWAPnew/provider"}'
WHERE id = 'AWAP';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://oai.esrc.unimelb.edu.au/EOASnew/provider"}'
WHERE id = 'EOAS';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.findandconnect.gov.au/ref/act/eac"}'
WHERE id = 'FCAC';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.findandconnect.gov.au/ref/australia/eac"}'
WHERE id = 'FCNA';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.findandconnect.gov.au/ref/nsw/eac"}'
WHERE id = 'FCNS';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.findandconnect.gov.au/ref/nt/eac"}'
WHERE id = 'FCNT';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.findandconnect.gov.au/ref/qld/eac"}'
WHERE id = 'FCQD';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.findandconnect.gov.au/ref/sa/eac"}'
WHERE id = 'FCSA';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.findandconnect.gov.au/ref/tas/eac"}'
WHERE id = 'FCTS';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.findandconnect.gov.au/ref/vic/eac"}'
WHERE id = 'FCVC';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://www.findandconnect.gov.au/ref/wa/eac"}'
WHERE id = 'FCWA';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://mura-prd.aiatsis.gov.au/uhtbin/OAI-script.pl"}'
WHERE id = 'MURA-abi';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://mura-prd.aiatsis.gov.au/uhtbin/OAI-script.pl"}'
WHERE id = 'MURA-book';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://mura-prd.aiatsis.gov.au/uhtbin/OAI-script.pl"}'
WHERE id = 'MURA-huni';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://oa.anu.edu.au/oai-pmh"}'
WHERE id = 'OA';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://catalog.paradisec.org.au/oai/item"}'
WHERE id = 'PDSC-olac';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://catalog.paradisec.org.au/oai/collection"}'
WHERE id = 'PDSC-rif';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"https://heurist.sydney.edu.au/HEURIST/HEURIST_FILESTORE/HuNI_TUGG/hml-output"}'
WHERE id = 'TUGG';

COMMIT;
