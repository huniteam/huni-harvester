-- Deploy huni-harvest:create-original-table to pg
-- requires: create-harvest-table

BEGIN;

CREATE TABLE harvest.original (
    name            TEXT        NOT NULL,
    feed_id         TEXT        NOT NULL,
    harvest_date    DATE        NOT NULL,
    file_id         TEXT        NOT NULL,
    data            JSON        NOT NULL,
    is_deleted      BOOLEAN     NOT NULL,

    PRIMARY KEY (feed_id, harvest_date, name),

    FOREIGN KEY (feed_id)
        REFERENCES harvest.feed (id),
    FOREIGN KEY (file_id)
        REFERENCES harvest.file (digest),
    FOREIGN KEY (feed_id, harvest_date)
        REFERENCES harvest.harvest (feed_id, date)
);

COMMIT;
