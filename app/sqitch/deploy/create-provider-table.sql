-- Deploy huni-harvest:create-provider-table to pg
-- requires: create-harvest-schema

BEGIN;

CREATE TABLE harvest.provider (
    id      TEXT    PRIMARY KEY,
    name    TEXT    NOT NULL
);

INSERT INTO harvest.provider (id, name) VALUES
    ('ACMI',	    'Australian Centre for the Moving Image'),
    ('ADB',         'Australian Dictionary of Biography'),
    ('AFIRC',       'AFI Research Collection'),
    ('AMHD',        'Australian Media History Database'),
    ('AU-APFA',     'Colonial Australian Popular Fiction'),
    ('AusStage',    'AusStage'),
    ('AustLit',     'AustLit'),
    ('AWAP',        'The Australian Women''s Register'),
    ('Bonza',       'BONZA - National Cinema and Television Database'),
    ('CAARP',       'CAARP - Cinema and Audiences Research Project database'),
    ('CircusOz',    'CircusOz'),
    ('DAAO',        'Design and ART Australia Online'),
    ('EMEL',        'Encyclopedia of Melbourne'),
    ('EOAS',        'Encyclopaedia of Australian Science'),
    ('FCAC',        'Find and Connect ACT'),
    ('FCNA',        'Find and Connect Australia'),
    ('FCNS',        'Find and Connect NSW'),
    ('FCNT',        'Find and Connect NT'),
    ('FCQD',        'Find and Connect Qld)'),
    ('FCSA',        'Find and Connect SA'),
    ('FCTS',        'Find and Connect Tas'),
    ('FCVC',        'Find and Connect Vic'),
    ('FCWA',        'Find and Connect WA'),
    ('GOLD',        'Electronic Encyclopedia of Gold in Australia'),
    ('MAP',         'Media Archives Project'),
    ('MURA',        'Mura® the AIATSIS Collections Catalogue'),
    ('OA',          'Obituaries Australia'),
    ('PDSC',        'PARADISEC: Pacific and Regional Archive for Digital Sources in Endangered Cultures'),
    ('SAUL',        'Saulwick Polls and Social Research'),
    ('TUGG',        'Public / TUGG'),
    ('WALL',        'The Wallaby Club Inc.');

COMMIT;
