-- Deploy huni-harvest:add-aus-unsw-feeds to pg

BEGIN;

INSERT INTO harvest.provider
  (id, name)
VALUES
  ('AUS', 'Australian Datasets'),
  ('UNSW', 'University of New South Wales');

INSERT INTO harvest.feed
  (id, provider_id, feed_type_id, preprocess_type_id, is_enabled, params)
VALUES
  ('AUS', 'AUS', 'HuNI_CSV', 'HuNI_CSV', true,
     '{ "url":"https://raw.githubusercontent.com/HuNIVL/australian-datasets/main/", "index":"" }'),
  ('UNSW', 'UNSW', 'HuNI_CSV', 'HuNI_CSV', true,
     '{ "url":"https://raw.githubusercontent.com/HuNIVL/unsw/main/", "index":"" }');

COMMIT;
