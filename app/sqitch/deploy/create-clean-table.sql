-- Deploy huni-harvest:create-clean-table to pg
-- requires: create-original-table

BEGIN;

CREATE TABLE harvest.clean (
    -- These are the two bits of information provided by the preprocess phase
    id              TEXT        NOT NULL,
    type            TEXT        NOT NULL,

    -- This is the original file from the harvest
    original_name   TEXT        NOT NULL,
    feed_id         TEXT        NOT NULL,
    harvest_date    DATE        NOT NULL,

    file_id         TEXT        NOT NULL,

    PRIMARY KEY (id, type, feed_id, harvest_date),

    FOREIGN KEY (feed_id)
        REFERENCES harvest.feed (id),
    FOREIGN KEY (file_id)
        REFERENCES harvest.file (digest),
    FOREIGN KEY (feed_id, harvest_date)
        REFERENCES harvest.harvest (feed_id, date),
    FOREIGN KEY (feed_id, harvest_date, original_name)
        REFERENCES harvest.original (feed_id, harvest_date, name)
);

-- Without this index, deleting original files is very slow, as it needs to
-- do a seq scan on the clean table for every original file deleted.
CREATE INDEX clean_original_fk_index
    ON harvest.clean (feed_id, harvest_date, original_name);

COMMIT;
