-- Deploy huni-harvest:create-insert-file-function to pg
-- requires: create-file-table

BEGIN;

CREATE OR REPLACE FUNCTION harvest.insert_file(content BYTEA) RETURNS TEXT AS
$insert_file$
    DECLARE
        existing_file   harvest.file%ROWTYPE;
        new_digest      TEXT;
    BEGIN
        -- If we already have a matching file, just return it.
        -- If we have a digest collision, we append a newline to the content and
        -- try again. The files are xml, so trailing whitespace will be safely
        -- ignored.
        LOOP
            new_digest = encode(digest(content, 'sha1'), 'hex');

            SELECT * INTO existing_file
                FROM harvest.file WHERE digest = new_digest;

            IF FOUND THEN
                IF existing_file.content = content THEN
                    RETURN existing_file.digest;
                ELSE
                    content := content || E'\n'::BYTEA;
                END IF;
            ELSE
                INSERT INTO harvest.file (digest, content)
                       VALUES (new_digest, content);
                RETURN new_digest;
            END IF;

        END LOOP;
    END;
$insert_file$
LANGUAGE plpgsql;

COMMIT;
