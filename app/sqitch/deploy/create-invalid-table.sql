-- Deploy huni-harvest:create-invalid-table to pg
-- requires: create-original-table
-- requires: create-harvest-table

BEGIN;

CREATE TABLE harvest.invalid (
    -- This is the original file from the harvest
    original_name   TEXT        NOT NULL,
    feed_id         TEXT        NOT NULL,
    harvest_date    DATE        NOT NULL,

    -- Error causing this file to be invalid
    error           TEXT        NOT NULL,

    PRIMARY KEY (feed_id, harvest_date, original_name),

    FOREIGN KEY (feed_id)
        REFERENCES harvest.feed (id),
    FOREIGN KEY (feed_id, harvest_date)
        REFERENCES harvest.harvest (feed_id, date),
    FOREIGN KEY (feed_id, harvest_date, original_name)
        REFERENCES harvest.original (feed_id, harvest_date, name)
);

COMMIT;
