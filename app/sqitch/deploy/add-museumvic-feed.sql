-- Deploy huni-harvest:add-museumvic-feed to pg

BEGIN;

INSERT INTO harvest.provider (id, name) VALUES
    ('MVC', 'Museums Victoria Collections');

INSERT INTO harvest.feed (id, provider_id, feed_type_id, preprocess_type_id, is_enabled, params) VALUES
    ('MVC', 'MVC', 'HuNI', 'HuNI', true,
        '{ "url":"https://museumvic.huni.net.au", "index":"resources.xml" }');

COMMIT;
