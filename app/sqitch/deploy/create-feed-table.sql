-- Deploy huni-harvest:create-feed-table to pg
-- requires: create-provider-table
-- requires: create-feed-type-table

BEGIN;

CREATE TABLE harvest.feed (
    -- For most providers, there will be a single feed, and we can give the
    -- source the same id as the provider (eg. ADB).
    -- For some, there are multiple feeds, so we give them distiguished names
    -- (eg MuraA, MuraB).
    id                  TEXT PRIMARY KEY,
    provider_id         TEXT NOT NULL REFERENCES harvest.provider (id),
    feed_type_id        TEXT NOT NULL REFERENCES harvest.feed_type (id),
    preprocess_type_id  TEXT NOT NULL REFERENCES harvest.preprocess_type (id),
    is_enabled          BOOLEAN NOT NULL,
    params              JSON NOT NULL
);

INSERT INTO harvest.feed (id, provider_id, feed_type_id, preprocess_type_id, is_enabled, params) VALUES
    ('ACMI',            'ACMI',     'HuNI',     'HuNI',     true,
        '{ "url":"https://acmi.huni.net.au", "index":"resources.xml" }'),
    ('ADB',             'ADB',      'OAI',      'EAC',      true,
        '{ "url":"http://adb.anu.edu.au/oai-pmh", "query":{ "metadataPrefix":"eac-cpf" } }'),
    ('AFIRC',           'AFIRC',    'HuNI',     'HuNI',     true,
        '{ "url":"https://afirc.huni.net.au", "index":"resources.xml" }'),
    ('AMHD',            'AMHD',     'HuNI',     'HuNI',     true,
        '{ "url":"http://amhd.info/xml", "index":"resources.xml" }'),
    ('AU-APFA',         'AU-APFA',  'HuNI',     'EAC',      true,
        '{ "url":"http://www.apfa.esrc.unimelb.edu.au/eac", "index":"resources.xml" }'),
    ('AusStage',        'AusStage', 'HuNI',     'HuNI',     true,
        '{ "url":"http://ausstage.edu.au/feed", "index":"resources.xml.gz" }'),
    ('AustLit-agents',  'AustLit',  'HuNI',     'EAC',      true,
        '{ "url":"http://www.austlit.edu.au/static/huniHarvest/agents", "index":"huni.xml" }'),
    ('AustLit-works',   'AustLit',  'HuNI',     'AustLit',  true,
        '{ "url":"http://www.austlit.edu.au/static/huniHarvest/works", "index":"huni.xml" }'),
    ('AWAP',            'AWAP',     'OAI',      'EAC',      true,
        '{ "url":"http://oai.esrc.unimelb.edu.au/AWAPnew/provider", "query":{ "metadataPrefix":"eac-cpf" } }'),
    ('Bonza',           'Bonza',    'HuNI',     'HuNI',     true,
        '{ "url":"http://bonzadb.com.au/static/huni", "index":"resources.xml" }'),
    ('CAARP',           'CAARP',    'HuNI',     'HuNI',     true,
        '{ "url":"http://caarp.edu.au/feed", "index":"resources.xml" }'),
    ('CircusOz',        'CircusOz', 'HuNI',     'HuNI',     true,
        '{ "url":"http://circusozdev.eres.rmit.edu.au/huni/feed", "index":"resources.xml.gz" }'),
    ('DAAO-eac-cpf',    'DAAO',     'OAI',      'EAC',      true,
        '{ "url":"http://oai.daao.org.au/oai/provider", "query":{ "metadataPrefix":"eac-cpf" } }'),
    ('DAAO-rif',        'DAAO',     'OAI',      'DAAO',     true,
        '{ "url":"http://oai.daao.org.au/oai/provider", "query":{ "metadataPrefix":"rif" } }'),
    ('EMEL',            'EMEL',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.emelbourne.net.au/eac", "index":"resources.xml" }'),
    ('EOAS',            'EOAS',     'OAI',      'EAC',      true,
        '{ "url":"http://oai.esrc.unimelb.edu.au/EOASnew/provider", "query":{ "metadataPrefix":"eac-cpf" } }'),
    ('FCAC',            'FCAC',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.findandconnect.gov.au/ref/act/eac", "index":"resources.xml" }'),
    ('FCNA',            'FCNA',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.findandconnect.gov.au/ref/australia/eac",  "index":"resources.xml" }'),
    ('FCNS',            'FCNS',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.findandconnect.gov.au/ref/nsw/eac", "index":"resources.xml" }'),
    ('FCNT',            'FCNT',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.findandconnect.gov.au/ref/nt/eac", "index":"resources.xml" }'),
    ('FCQD',            'FCQD',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.findandconnect.gov.au/ref/qld/eac", "index":"resources.xml" }'),
    ('FCSA',            'FCSA',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.findandconnect.gov.au/ref/sa/eac", "index":"resources.xml" }'),
    ('FCTS',            'FCTS',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.findandconnect.gov.au/ref/tas/eac", "index":"resources.xml" }'),
    ('FCVC',            'FCVC',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.findandconnect.gov.au/ref/vic/eac", "index":"resources.xml" }'),
    ('FCWA',            'FCWA',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.findandconnect.gov.au/ref/wa/eac", "index":"resources.xml" }'),
    ('GOLD',            'GOLD',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.egold.net.au/eac", "index":"resources.xml" }'),
    ('MAP',             'MAP',      'HuNI',     'HuNI',     true,
        '{ "url":"https://mediaarchivesproject.mq.edu.au/xml", "index":"resources.xml" }'),
    ('MURA-abi',        'MURA',     'OAI',      'MARC21',   false,
        '{ "url":"http://203.0.89.252/uhtbin/OAI-script.pl", "query":{ "metadataPrefix":"marc21", "set":"ABI" } }'),
    ('MURA-book',       'MURA',     'OAI',      'MARC21',   false,
        '{ "url":"http://203.0.89.252/uhtbin/OAI-script.pl", "query":{ "metadataPrefix":"marc21", "set":"BOOK/SERIALANALYTICS", "force":1 } }'),
    ('MURA-huni',       'MURA',     'OAI',      'MARC21',   false,
        '{ "url":"http://203.0.89.252/uhtbin/OAI-script.pl", "query":{ "metadataPrefix":"marc21", "set":"HUNI" } }'),
    ('OA',              'OA',       'OAI',      'EAC',      true,
        '{ "url":"http://oa.anu.edu.au/oai-pmh", "query":{ "metadataPrefix":"eac-cpf" } }'),
    ('PDSC-olac',       'PDSC',     'OAI',      'PDSC',     true,
        '{ "url":"http://catalog.paradisec.org.au/oai/item", "query":{ "metadataPrefix":"olac" } }'),
    ('PDSC-rif',        'PDSC',     'OAI',      'PDSC',     true,
        '{ "url":"http://catalog.paradisec.org.au/oai/collection", "query":{ "metadataPrefix":"rif" } }'),
    ('SAUL',            'SAUL',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.saulwick.info/eac", "index":"resources.xml" }'),
    ('TUGG',            'TUGG',     'HuNI',     'Heurist',  true,
        '{ "url":"http://heurist.sydney.edu.au/HEURIST/HEURIST_FILESTORE/HuNI_TUGG/hml-output", "index":"resources.xml" }'),
    ('WALL',            'WALL',     'HuNI',     'EAC',      true,
        '{ "url":"http://www.wallabyclub.org.au/compendium/eac", "index":"resources.xml" }');

COMMIT;
