-- Revert huni-harvest:create-provider-table from pg

BEGIN;

DROP TABLE IF EXISTS harvest.provider;

COMMIT;
