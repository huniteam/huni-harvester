-- Revert huni-harvest:create-harvest-schema from pg

BEGIN;

DROP EXTENSION IF EXISTS pgcrypto;

DROP SCHEMA IF EXISTS harvest;

COMMIT;
