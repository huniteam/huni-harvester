-- Revert huni-harvest:create-transform-type-table from pg

BEGIN;

DROP TABLE IF EXISTS harvest.transform_type;

COMMIT;
