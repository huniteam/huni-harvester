-- Revert huni-harvest:create-preprocess-type-table from pg

BEGIN;

DROP TABLE IF EXISTS harvest.preprocess_type;

COMMIT;
