-- Revert huni-harvest:update-feed-urls-2020 from pg

BEGIN;

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://adb.anu.edu.au/oai-pmh"}'
WHERE id = 'ABD';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://amhd.info/xml"}'
WHERE id = 'AMHD';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://www.ausstage.edu.au/feed"}'
WHERE id = 'AusStage';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://www.austlit.edu.au/static/huniHarvest/agents"}'
WHERE id = 'AustLit-agents';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://www.austlit.edu.au/static/huniHarvest/works"}'
WHERE id = 'AustLit-works';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://oai.esrc.unimelb.edu.au/AWAPnew/provider"}'
WHERE id = 'AWAP';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://oai.esrc.unimelb.edu.au/EOASnew/provider"}'
WHERE id = 'EOAS';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://www.findandconnect.gov.au/ref/act/eac"}'
WHERE id = 'FCAC';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://www.findandconnect.gov.au/ref/australia/eac"}'
WHERE id = 'FCNA';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://www.findandconnect.gov.au/ref/nsw/eac"}'
WHERE id = 'FCNS';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://www.findandconnect.gov.au/ref/nt/eac"}'
WHERE id = 'FCNT';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://www.findandconnect.gov.au/ref/qld/eac"}'
WHERE id = 'FCQD';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://www.findandconnect.gov.au/ref/sa/eac"}'
WHERE id = 'FCSA';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://www.findandconnect.gov.au/ref/tas/eac"}'
WHERE id = 'FCTS';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://www.findandconnect.gov.au/ref/vic/eac"}'
WHERE id = 'FCVC';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://www.findandconnect.gov.au/ref/wa/eac"}'
WHERE id = 'FCWA';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://203.0.89.252/uhtbin/OAI-script.pl"}'
WHERE id = 'MURA-abi';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://203.0.89.252/uhtbin/OAI-script.pl"}'
WHERE id = 'MURA-book';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://203.0.89.252/uhtbin/OAI-script.pl"}'
WHERE id = 'MURA-huni';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://oa.anu.edu.au/oai-pmh"}'
WHERE id = 'OA';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://catalog.paradisec.org.au/oai/item"}'
WHERE id = 'PDSC-olac';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://catalog.paradisec.org.au/oai/collection"}'
WHERE id = 'PDSC-rif';

UPDATE harvest.feed
   SET params = params::jsonb - 'url' || '{"url":"http://heurist.sydney.edu.au/HEURIST/HEURIST_FILESTORE/HuNI_TUGG/hml-output"}'
WHERE id = 'TUGG';

COMMIT;
