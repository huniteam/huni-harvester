-- Revert huni-harvest:create-original-views from pg

BEGIN;

DROP VIEW IF EXISTS harvest.extended_original;
DROP VIEW IF EXISTS harvest.original_history;

COMMIT;
