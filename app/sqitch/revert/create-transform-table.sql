-- Revert huni-harvest:create-transform-table from pg

BEGIN;

DROP TABLE IF EXISTS harvest.transform;

COMMIT;
