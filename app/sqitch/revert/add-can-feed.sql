-- Revert huni-harvest:add-can-feed from pg

BEGIN;

DELETE FROM harvest.feed     WHERE id = 'CAN';
DELETE FROM harvest.provider WHERE id = 'CAN';

COMMIT;
