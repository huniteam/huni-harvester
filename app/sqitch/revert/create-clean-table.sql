-- Revert huni-harvest:create-clean-table from pg

BEGIN;

DROP TABLE IF EXISTS harvest.clean;

COMMIT;
