-- Revert huni-harvest:create-insert-file-function from pg

BEGIN;

DROP FUNCTION IF EXISTS harvest.insert_file(BYTEA);

COMMIT;
