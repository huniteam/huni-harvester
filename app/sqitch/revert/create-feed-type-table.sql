-- Revert huni-harvest:create-feed-type-table from pg

BEGIN;

DROP TABLE IF EXISTS harvest.feed_type;

COMMIT;
