-- Revert huni-harvest:update-can-feed-name from pg

BEGIN;

UPDATE harvest.provider
SET name = 'Canadiana'
WHERE id  = 'CAN';

COMMIT;
