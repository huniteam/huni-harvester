-- Revert huni-harvest:add-museumvic-feed from pg

BEGIN;

DELETE FROM harvest.feed     WHERE id = 'MVC';
DELETE FROM harvest.provider WHERE id = 'MVC';

COMMIT;
