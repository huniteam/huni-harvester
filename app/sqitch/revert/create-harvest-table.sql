-- Revert huni-harvest:create-harvest-table from pg

BEGIN;

DROP TABLE IF EXISTS harvest.harvest;

COMMIT;
