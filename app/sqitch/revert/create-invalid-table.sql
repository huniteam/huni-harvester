-- Revert huni-harvest:create-invalid-table from pg

BEGIN;

DROP TABLE IF EXISTS harvest.invalid;

COMMIT;
