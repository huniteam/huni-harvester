-- Revert huni-harvest:create-harvest-status-type-table from pg

BEGIN;

DROP TABLE IF EXISTS harvest.harvest_status_type;

COMMIT;
