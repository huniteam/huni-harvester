-- Revert huni-harvest:add-links-column-to-feed-table from pg

BEGIN;

ALTER TABLE harvest.feed
 DROP COLUMN IF EXISTS links_config;

COMMIT;
