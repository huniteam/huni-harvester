-- Revert huni-harvest:add-anmm-feed from pg

BEGIN;

DELETE FROM harvest.feed     WHERE id = 'ANMM';
DELETE FROM harvest.provider WHERE id = 'ANMM';

COMMIT;
