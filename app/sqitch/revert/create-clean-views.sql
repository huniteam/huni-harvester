-- Revert huni-harvest:create-clean-views from pg

BEGIN;

DROP VIEW IF EXISTS harvest.extended_clean;
DROP VIEW IF EXISTS harvest.clean_history;

COMMIT;
