-- Revert huni-harvest:create-guest from pg

BEGIN;

REVOKE read_access FROM guest;
DROP USER guest;

ALTER DEFAULT PRIVILEGES IN SCHEMA harvest
  REVOKE SELECT ON TABLES FROM read_access;
REVOKE SELECT ON ALL TABLES IN SCHEMA harvest FROM read_access;
REVOKE USAGE ON SCHEMA harvest FROM read_access;

DROP ROLE IF EXISTS read_access;

COMMIT;
