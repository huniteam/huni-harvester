-- Revert huni-harvest:add-aus-unsw-feeds from pg

BEGIN;

DELETE FROM harvest.feed     WHERE id = 'AUS' OR id = 'UNSW';
DELETE FROM harvest.provider WHERE id = 'AUS' OR id = 'UNSW';

COMMIT;
