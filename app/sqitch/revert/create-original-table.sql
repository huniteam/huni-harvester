-- Revert huni-harvest:create-original-table from pg

BEGIN;

DROP TABLE IF EXISTS harvest.original;

COMMIT;
