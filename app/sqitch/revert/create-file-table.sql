-- Revert huni-harvest:create-file-table from pg

BEGIN;

DROP TABLE IF EXISTS harvest.file;

COMMIT;
