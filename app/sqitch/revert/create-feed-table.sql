-- Revert huni-harvest:create-feed-table from pg

BEGIN;

DROP TABLE IF EXISTS harvest.feed;

COMMIT;
