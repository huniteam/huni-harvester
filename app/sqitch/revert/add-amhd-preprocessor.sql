-- Revert huni-harvest:add-amhd-preprocessor from pg

BEGIN;

UPDATE harvest.feed
   SET preprocess_type_id = 'HuNI'
 WHERE id = 'AMHD';

DELETE FROM harvest.preprocess_type
      WHERE id = 'AMHD';

COMMIT;
