-- Revert huni-harvest:add-huni-csv-preprocessor from pg

BEGIN;

DELETE FROM harvest.preprocess_type WHERE id = 'HuNI_CSV';
DELETE FROM harvest.feed_type       WHERE id = 'HuNI_CSV';

COMMIT;
