use 5.24.0;
use warnings;

use Plack::Builder;

use Plack::Middleware::CrossOrigin;
use Plack::Middleware::Headers;
use HuNI::Harvest::API;

# default max paged items; allows test fixtures to reduce

builder {

    # Param munging for paged GETs
    # CORS headers
    enable 'CrossOrigin', origins => '*', headers => '*';

    # Cache control headers
    enable 'Headers',
        code   => '200',
        set    => [
            'Cache-Control' => 'no-cache, no-store',
            'Pragma'        => 'no-cache',
            'Expires'       => '0'
        ];

    # The actual app :)
    HuNI::Harvest::API->to_app;
};
