package Log::Any::Plugin::Progress;

use 5.26.0;
use warnings;

use Log::Any                qw( $log );
use Log::Any::Plugin::Util  qw( set_new_method );

use Data::Printer {
    hash_separator  => ': ',
    multiline       => 0,
};

sub install {
    my ($class, $adapter_class, %args) = @_;

    my $last_progress_time = time;
    my $progress_update_sec = $ENV{LOG_ANY_PROGRESS_UPDATE_SEC}
                                // $args{update_sec}
                                // 60;

    my $method = $ENV{LOG_ANY_PROGRESS_METHOD}
                    // $args{method}
                    // 'info';

    if ($method eq 'none') {
        $log->info('Disabling Log::Any::Plugin::Progress');
        set_new_method($adapter_class, progress => sub { });
    }
    else {
        set_new_method($adapter_class, progress => sub {
            my ($self, $data, $force) = @_;

            $last_progress_time = 0 if $force;

            my $now = time;
            if ($now - $last_progress_time >= $progress_update_sec) {
                $log->$method(np(%$data));
                $last_progress_time = $now;
            }
            return $data;
        });
    }
}

1;
