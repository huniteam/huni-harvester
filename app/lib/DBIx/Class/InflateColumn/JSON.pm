package DBIx::Class::InflateColumn::JSON;

use strict;
use warnings;
use base qw( DBIx::Class );
use Cpanel::JSON::XS qw( decode_json encode_json );

use namespace::clean;

__PACKAGE__->load_components(qw( InflateColumn ));

sub register_column {
    my ($self, $column, $info, @rest) = @_;
    $self->next::method($column, $info, @rest);

    my $data_type = lc( $info->{data_type} || '' );
    return if $data_type ne 'json';

    # Use canonical encoding here. It's a bit slower, but we don't use these
    # JSON columns for anything but config data. What we get is repeatable
    # column contents, so we only see actual changes logged.
    my $encoder = Cpanel::JSON::XS->new->utf8->canonical;

    $self->inflate_column($column => {
        inflate => sub { decode_json($_[0]) },
        deflate => sub {
            # takes data structure or an encoded JSON string
            ref $_[0] ? $encoder->encode($_[0]) : $_[0];
        },
    });
}

1;
