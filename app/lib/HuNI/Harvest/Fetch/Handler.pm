package HuNI::Harvest::Fetch::Handler;

use 5.20.0;
use warnings;

use DateTime                qw( );
use Function::Parameters    qw( :strict );

use Moo::Role;

requires qw( fetch );

has harvest => (
    is          => 'ro',
    default     => 0,
);

has feed => (
    is          => 'lazy',
    builder     => method() {
        return $self->harvest->feed;
    },
);

has schema => (
    is          => 'lazy',
    builder     => method() {
        return $self->harvest->result_source->schema;
    },
);

has previous_harvest => (
    is          => 'lazy',
    builder     => method() {
        return $self->harvest->previous;
    },
);

has stats => (
    is => 'lazy',
    builder => sub {
        +{
            duplicate => 0,
            inserted  => 0,
            deleted   => 0,
            total     => 0,
        },
    },
);

1;
