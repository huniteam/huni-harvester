package HuNI::Harvest::Fetch::Handler::HuNI_CSV;

use 5.20.0;
use warnings;

use HTTP::Tiny                      qw( );
use HuNI::Harvest::Validator::CSV   qw( );
use Log::Any                        qw( $log );
use Path::Tiny                      qw( path tempdir );
use Text::CSV_XS                    qw( );
use Try::Tiny;
use XML::Hash::XS                   qw( hash2xml );

use Moo;
use Function::Parameters    qw( :strict );

with 'HuNI::Harvest::Fetch::Handler';

my @entity_types = qw( concept event organisation person place work );

has url => (
    is => 'lazy',
    builder => method() {
        $self->feed->params->{url};
    },
);

has index => (
    is => 'lazy',
    builder => method() {
        $self->feed->params->{index};
    },
);

has ua => (
    is => 'lazy',
    builder => sub { HTTP::Tiny->new },
);

# This is a temporary directory that we fetch the csv files into.
has csv_dir => (
    is => 'lazy',
    builder => sub { tempdir },
);

has csv_schema => (
    is => 'lazy',
    builder => method() {
        $self->download_file('huni-csv.json', $ENV{CSV_VALIDATOR_SCHEMA_URL})
            or die "$ENV{CSV_VALIDATOR_SCHEMA_URL} not found";
    },
);

has validator => (
    is => 'lazy',
    default => method() {
        HuNI::Harvest::Validator::CSV->new(schema_path => $self->csv_schema);
    },
);

method fetch() {
    my $existing_rs = $self->feed->extended_originals->latest;
    my %existing;
    while (my $row = $existing_rs->next) {
        $existing{$row->name} = {
            name        => $row->name,
            file_id     => $row->file_id,
            is_deleted  => $row->is_deleted,
            data        => $row->data,
        };
    }

    my %csv_path = $self->fetch_and_validate_csv;
    my $files_rs = $self->schema->resultset('File');

    # Iterate through every row of every csv file.
    for my $type (keys %csv_path) {
        my $iterator = $self->csv_iterator($csv_path{$type});

        while (my $record = $iterator->()) {
            my $name = join('_', $type, $record->{ID});

            # The values of $record are wide characters.
            # The XML in $xml is utf8-encoded.
            my $xml = hash2xml($record,
                canonical => 1,         # So we generate the same xml each time
                indent    => 2,         # Not necessary, but easier to read
                root      => 'record',  # To match the solr transforms

                encoding  => 'utf-8',   # Encode to utf8
                utf8      => 0,         # Setting this to 0 enables utf8 ... o_O
            );

            my $file_id = $files_rs->insert_content_utf8($xml);
            my $status = 'inserted';

            # Look up and remove the previous version.
            my $previous = delete $existing{$name};
            if ($previous && ($previous->{file_id} eq $file_id)) {
                if ($previous->{is_deleted}) {
                    $status = 'undeleted';
                }
                else {
                    $status = 'duplicate';
                }
            }

            if ($status ne 'duplicate') {
                $self->harvest->create_related(originals => {
                    name        => $name,
                    file_id     => $file_id,
                    is_deleted  => 0,
                    data        => { },
                });
            }

            $self->stats->{$status}++;
            $self->stats->{total}++;
            $log->progress($self->stats);
        }
    }

    # What's left in %existing are csv rows that used to exist, but no longer
    # do, so we need to delete them.
    for my $record (values %existing) {
        next if $record->{is_deleted}; # already deleted

        $record->{is_deleted} = 1;  # mark newly deleted records as deleted
        $self->harvest->create_related(originals => $record);

        $self->stats->{deleted}++;
        $self->stats->{total}++;

        $log->progress($self->stats);
    }
    $log->progress($self->stats, 1);
}

method fetch_and_validate_csv() {
    my $existing_rs = $self->feed->extended_originals->latest;
    my %csv_path;

    my $validation_errors = 0;
    for my $type (@entity_types) {
        my $filename = "$type.csv";

        my $path = $self->download_file($filename);

        if (!$path) {
            # It's okay for a CSV file to be missing, but only if there's no
            # current records of that type. We take that to mean something has
            # gone wrong, rather than "delete all this type of record".
            my $found = $existing_rs->search({
                name      => { -like => "${type}%" },
                -not_bool => [qw( is_deleted )],
            })->count;

            if ($found == 0) {
                $log->info("Skipping missing $filename");
            }
            else {
                my $msg = ($found == 1) ? 'record remains' : 'records remain';
                $log->error("Missing $filename but $type $msg");
                $validation_errors++;
            }
            next;
        }

        my $result = $self->validator->validate($path);
        if ($result->{validation}->{state} eq 'valid') {
            $csv_path{$type} = $path;
            next;
        }

        $validation_errors++;
        for my $error (@{ $result->{validation}->{errors} }) {
            $log->error("CSV validation error in $filename:", $error);
        }
    }

    if ($validation_errors) {
        die 'CSV failed validation, aborting harvest';
    }

    return %csv_path;
}

method csv_iterator($csv_path) {
    my $csv = Text::CSV_XS->new ({
        allow_loose_quotes  => 1,
        auto_diag           => 1,
        binary              => 1,
        decode_utf8         => 1,
    });


    my $fh = $csv_path->openr_raw;
    my @names = $csv->getline($fh);
    $csv->column_names(@{$names[0]});

    return sub {
        return $csv->getline_hr($fh);
    };
}

method download_file($filename, $url = $self->url . '/' . $filename) {
    $log->debug("Fetching $filename from $url");

    my $path = $self->csv_dir->child($filename);
    my $fh = $path->openw_raw;
    my $response = $self->ua->get($url, { data_callback =>
        fun($chunk_data, $chunk_response) {
            print {$fh} $chunk_data;
        },
    });
    close($fh) or die "Writing $path: $!\n";

    if ($response->{status} == 404) {
        # Just return nothing if the file was not found.
        return;
    }

    if (!$response->{success}) {
        # Any other failures are a problem though.
        die "Downloading $url: $response->{status} response->{content}\n";
    }

    return $path;
}

1;
