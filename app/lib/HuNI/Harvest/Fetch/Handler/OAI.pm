package HuNI::Harvest::Fetch::Handler::OAI;

use 5.20.0;
use warnings;

use Function::Parameters    qw( :strict );
use HTTP::OAI               qw( );
use HTTP::Tiny              qw( );
use Log::Any                qw( $log );
use URI                     qw( );

use Moo;
with 'HuNI::Harvest::Fetch::Handler';

has url => (
    is => 'lazy',
    builder => method() {
        $self->feed->params->{url};
    },
);

has query => (
    is => 'lazy',
    builder => method() {
        $self->feed->params->{query};
    },
);

has harvester => (
    is => 'lazy',
    builder => method() {
        my $repository = HTTP::OAI::Identify->new(baseURL => $self->url);
        my $harvester = HTTP::OAI::Harvester->new(
            repository => $repository,
            resume => 1,
        );
        $harvester->timeout(180);
        return $harvester;
    },
);

has ua => (
    is => 'lazy',
    builder => sub {
        HTTP::Tiny->new,
    },
);

method fetch() {
    my %args = (
        %{ $self->query },
        onRecord => fun($record, $response) {
            $self->fetch_record($record);
            $self->stats->{total}++;
            $log->progress($self->stats);
        },
    );

    my $previous = $self->harvest->previous;
    if ($previous) {
        # Allow some overlap on the dates. We'll catch any duplicates.
        my $day_before = $previous->date->subtract(days => 1);
        $args{from} = $self->harvest->format_date($day_before);
    }

    my $r = $self->harvester->ListIdentifiers(%args);
    if ($r->is_error) {
        my ($code, $message) = ($r->code, $r->message);
        chomp $message;
        die "$code $message\n";
    }

    $log->progress($self->stats, 1);
}

method fetch_record($record) {
    my $name = $record->identifier;
    $name =~ tr/\/:/_-/; # replace / and : with _ and -

    my $previous = $self->feed->extended_originals->latest->search({
        name => $name,
    })->first;

    if ($previous) {
        my $datestamp = $previous->{datestamp} // '';
        if ($record->datestamp eq $datestamp) {
            $log->debug("Skipping $name - already fetched");
            $self->stats->{duplicate}++;
            return;
        }
    }

#    if ($record->is_deleted) {
#        if (!$previous) {
#            $self->stats->{deleted_missing}++;
#            $log->info("Skipping deleted $name - deleting non-existent file");
#            return;
#        }
#
#        if ($previous->is_deleted) {
#            $self->stats->{duplicate_deleted}++;
#            $log->debug("Skipping deleted $name - already deleted");
#        }
#        return;
#    }

    my $content_utf8 = $self->fetch_full_record($record->identifier);
    my $files = $self->schema->resultset('File');
    my $file_id = $files->insert_content_utf8($content_utf8);

    if ($previous && ($previous->file_id eq $file_id)) {
        $self->stats->{duplicate}++;
        $log->debug("Skipping $name - duplicate");
        # TODO - update timestamp
        return;
    }

    $self->harvest->create_related(originals => {
        name    => $name,
        file_id => $file_id,
        data => {
            datestamp => $record->datestamp,
        },
        is_deleted => $record->is_deleted ? 1 : 0,
    });

    my $stat = $record->is_deleted ? 'deleted' : 'inserted';
    $self->stats->{$stat}++;

    return;
}

method fetch_full_record($identifier) {
    my $url = URI->new($self->url);
    $url->query_form(
        %{ $self->query },
        identifier => $identifier,
        verb => 'GetRecord',
    );

    $log->debug("Fetching $identifier from $url");

    my $response = $self->ua->get($url);
    unless ($response->{success}) {
        use Data::Dumper::Concise; print STDERR Dumper($response);
        die $response;
    }

    return $response->{content};
}

1;
