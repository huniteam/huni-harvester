package HuNI::Harvest::Fetch::Handler::HuNI;

use 5.20.0;
use warnings;

use Crypt::Digest::MD5      qw( md5_hex );
use Function::Parameters    qw( :strict );
use Gzip::Faster            qw( gunzip );
use HTTP::Tiny              qw( );
use Log::Any                qw( $log );
use XML::LibXML             qw( );

use Moo;
with 'HuNI::Harvest::Fetch::Handler';

has url => (
    is => 'lazy',
    builder => method() {
        $self->feed->params->{url};
    },
);

has index => (
    is => 'lazy',
    builder => method() {
        $self->feed->params->{index};
    },
);

has ua => (
    is => 'lazy',
    builder => sub { HTTP::Tiny->new },
);

has records => (
    is => 'lazy',
    builder => method() {
        my $xml = $self->fetch_file($self->index);
        my $dom = XML::LibXML->load_xml(string => $xml);

        my $text = fun($node, $path) {
            my ($value) = $node->findnodes("./$path/text()");
            return $value->data;
        };

        my @records;
        for my $node ($dom->findnodes('//record')) {
            my %record;
            $record{name} = $text->($node, 'name');
            if ($record{name} eq $self->index) {
                $log->debug("Skipping index file $record{name}");
                next;
            }
            $record{data}->{hash} = $text->($node, 'hash');
            $record{is_deleted} = 0;
            push(@records, \%record);
        }

        $self->stats->{total} = scalar @records;

        return \@records;
    },
);

method fetch() {
    my %keep;

    my $existing_rs = $self->feed->extended_originals->latest;
    my %existing;
    while (my $row = $existing_rs->next) {
        $existing{$row->name} = {
            name        => $row->name,
            file_id     => $row->file_id,
            is_deleted  => $row->is_deleted,
            data        => $row->data,
        };
    }

    my $files = $self->schema->resultset('File');
    for my $current (@{ $self->records }) {

        $log->progress($self->stats);
        my $previous = delete $existing{ $current->{name} };
        if ($previous && ($previous->{data}->{hash} eq $current->{data}->{hash})) {
            if (!$previous->{is_deleted}) {
                $self->stats->{duplicate}++;
                next;
            }
            $current->{file_id} = $previous->{file_id};
            $self->stats->{undeleted}++;
        }

        if (!$current->{file_id}) {
            my $content_utf8 = $self->fetch_file($current->{name});
            $current->{file_id} = $files->insert_content_utf8($content_utf8);
            $self->stats->{inserted}++;
        }

        $self->harvest->create_related(originals => $current);
    }

    for my $record (grep { !$_->{is_deleted} } values %existing) {
        $record->{is_deleted} = 1;

        $self->harvest->create_related(originals => $record);

        $self->stats->{deleted}++;
        $self->stats->{total}++;

        $log->progress($self->stats);
    }

    $log->progress($self->stats, 1);
}

method fetch_file($filename) {
    my $url = $self->url . '/' . $filename;
    $log->debug("Fetching $filename from $url");

    my $response = $self->ua->get($url);

    die $response unless $response->{success};
    my $content = $response->{content};

    if ($filename =~ /.gz$/) {
        $content = gunzip($content);
    }

    return $content;
}

1;
