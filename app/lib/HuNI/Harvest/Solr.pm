package HuNI::Harvest::Solr;

use 5.26.0;
use warnings;

use HTTP::Tiny              qw( );
use Log::Any                qw( $log );
use Moo;

use Function::Parameters    qw( :strict );

has ua => (
    is => 'lazy',
    # isa => 'HTTP::Tiny',
    builder => method() {
        return HTTP::Tiny->new;
    },
);

has solr_uri => (
    is => 'ro',
    required => 1,
);

has transformer => (
    is => 'ro',
    required => 1,
);

has request_limit => (
    is => 'ro',
    default => 1024 * 1024,
);

method wrap_iterator($prefix, $suffix, $iterator) {
    my %wrapper = (
        prefix => $prefix,
        suffix => $suffix,
    );

    return sub {
        if (my $prefix = delete $wrapper{prefix}) {
            $log->debug($prefix);
            return $prefix;
        }

        if (my $value = $iterator->()) {
            return $value;
        }

        if (my $suffix = delete $wrapper{suffix}) {
            $log->debug($suffix);
            return $suffix;
        }

        return;
    };
}

method make_transform_iterator($clean_rs, $progress) {
    return $self->wrap_iterator('<add>', '</add>', sub {
        while (my $clean = $clean_rs->next) {
            $log->progress($progress);
            my $name = $clean->name;
            my $doc = $self->transformer->transform($clean);
            if (defined $doc) {
                $progress->{added}++;
                $log->debug($name);
                return $doc;
            }
            $progress->{skipped}++;
            $log->progress($progress);
        }
        return;
    });
}

method transform_and_upload($clean_rs) {
    my $total = $clean_rs->count;
    $log->info("Transforming $total clean records for solr");
    my $chunksize = 5000;
    my $pages = int(($total + $chunksize - 1) / $chunksize);

    my %progress = (
        total => $total,
        added => 0,
        skipped => 0,
    );

    for (my $page = 0; $page < $pages; $page++) {
        my $rs = $clean_rs->search({ }, {
            order_by => [qw( me.type me.id ), { -desc => 'me.harvest_date' }],
            prefetch => [qw( file original ), { feed => 'provider' }],
            page     => $page + 1,
            rows     => $chunksize,
        });

        my $data_callback = $self->make_transform_iterator($rs, \%progress);
        $self->post($data_callback);
        $log->progress(\%progress, 1);
    }

    $self->post('<commit waitSearcher="false"/>');
}

method optimize() {
    $log->info('Optimising Solr index');
    $self->post('<optimize waitSearcher="false"/>');
}

method post($content) {
    my $response = $self->ua->post($self->solr_uri, {
        headers => {
            'Content-type' => 'application/xml',
        },
        content => $content,
    });
use Data::Dumper::Concise; print STDERR Dumper($response) unless $response->{success};
    die unless $response->{success};
}

1;
