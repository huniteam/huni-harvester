package HuNI::Harvest::Log;

use 5.20.0;
use warnings;

use Data::Printer {
    colored         => 1,
    hash_separator  => ': ',
    multiline       => 0,
    color => {
        hash => 'green',
        number => 'cyan',
    },
};

use Log::Any                qw( );
use Log::Any::Adapter       qw( );
use Log::Any::Plugin        qw( );

my $is_production = $ENV{HUNI_HARVEST_PRODUCTION} // 0;

if ($is_production) {
    $ENV{LOG_ANY_PROGRESS_METHOD} //= 'none';
}

Log::Any::Adapter->set('Stderr');
Log::Any::Plugin->add('Progress');
Log::Any::Plugin->add('ANSIColor') unless $is_production;
Log::Any::Plugin->add('Levels', level => $ENV{HUNI_DEBUG} ? 'debug'
                                       : $ENV{HUNI_QUIET} ? 'warning'
                                       :                    'info');

1;
