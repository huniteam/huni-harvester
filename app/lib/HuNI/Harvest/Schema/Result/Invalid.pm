use utf8;
package HuNI::Harvest::Schema::Result::Invalid;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Result::Invalid

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Result>

=cut

use base 'HuNI::Harvest::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<harvest.invalid>

=cut

__PACKAGE__->table("harvest.invalid");

=head1 ACCESSORS

=head2 original_name

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 feed_id

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 harvest_date

  data_type: 'date'
  is_foreign_key: 1
  is_nullable: 0

=head2 error

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "original_name",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "feed_id",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "harvest_date",
  { data_type => "date", is_foreign_key => 1, is_nullable => 0 },
  "error",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</feed_id>

=item * L</harvest_date>

=item * L</original_name>

=back

=cut

__PACKAGE__->set_primary_key("feed_id", "harvest_date", "original_name");

=head1 RELATIONS

=head2 feed

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::Feed>

=cut

__PACKAGE__->belongs_to(
  "feed",
  "HuNI::Harvest::Schema::Result::Feed",
  { id => "feed_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 harvest

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::Harvest>

=cut

__PACKAGE__->belongs_to(
  "harvest",
  "HuNI::Harvest::Schema::Result::Harvest",
  { date => "harvest_date", feed_id => "feed_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 original

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::Original>

=cut

__PACKAGE__->belongs_to(
  "original",
  "HuNI::Harvest::Schema::Result::Original",
  {
    feed_id => "feed_id",
    harvest_date => "harvest_date",
    name => "original_name",
  },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-08 01:19:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:rky9lSivaZ2pp5XN/waGkw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
