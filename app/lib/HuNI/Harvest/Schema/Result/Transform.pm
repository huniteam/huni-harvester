use utf8;
package HuNI::Harvest::Schema::Result::Transform;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Result::Transform

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Result>

=cut

use base 'HuNI::Harvest::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<harvest.transform>

=cut

__PACKAGE__->table("harvest.transform");

=head1 ACCESSORS

=head2 type

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 feed_id

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 file_type

  data_type: 'text'
  is_nullable: 0

=head2 transform

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "type",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "feed_id",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "file_type",
  { data_type => "text", is_nullable => 0 },
  "transform",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</type>

=item * L</feed_id>

=item * L</file_type>

=back

=cut

__PACKAGE__->set_primary_key("type", "feed_id", "file_type");

=head1 RELATIONS

=head2 feed

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::Feed>

=cut

__PACKAGE__->belongs_to(
  "feed",
  "HuNI::Harvest::Schema::Result::Feed",
  { id => "feed_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 type

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::TransformType>

=cut

__PACKAGE__->belongs_to(
  "type",
  "HuNI::Harvest::Schema::Result::TransformType",
  { id => "type" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-10-02 16:09:10
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:LG/8MVhHQAFcJzmJl0VAXg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
