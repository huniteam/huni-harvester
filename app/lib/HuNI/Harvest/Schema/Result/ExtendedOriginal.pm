use utf8;
package HuNI::Harvest::Schema::Result::ExtendedOriginal;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Result::ExtendedOriginal

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Result>

=cut

use base 'HuNI::Harvest::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<harvest.extended_original>

=cut

__PACKAGE__->table("harvest.extended_original");
__PACKAGE__->result_source_instance->view_definition(" SELECT o.name,\n    o.feed_id,\n    o.harvest_date,\n    o.file_id,\n    o.data,\n    o.is_deleted,\n    h.history,\n    ((o.harvest_date)::text = (h.history ->> 0)) AS is_latest\n   FROM (harvest.original o\n     JOIN harvest.original_history h USING (name, feed_id))");

=head1 ACCESSORS

=head2 name

  data_type: 'text'
  is_nullable: 1

=head2 feed_id

  data_type: 'text'
  is_nullable: 1

=head2 harvest_date

  data_type: 'date'
  is_nullable: 1

=head2 file_id

  data_type: 'text'
  is_nullable: 1

=head2 data

  data_type: 'json'
  is_nullable: 1

=head2 is_deleted

  data_type: 'boolean'
  is_nullable: 1

=head2 history

  data_type: 'json'
  is_nullable: 1

=head2 is_latest

  data_type: 'boolean'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "name",
  { data_type => "text", is_nullable => 1 },
  "feed_id",
  { data_type => "text", is_nullable => 1 },
  "harvest_date",
  { data_type => "date", is_nullable => 1 },
  "file_id",
  { data_type => "text", is_nullable => 1 },
  "data",
  { data_type => "json", is_nullable => 1 },
  "is_deleted",
  { data_type => "boolean", is_nullable => 1 },
  "history",
  { data_type => "json", is_nullable => 1 },
  "is_latest",
  { data_type => "boolean", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-20 23:12:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:dDC/OGSphUU1dTgtJzrmwA

__PACKAGE__->inherit_relationships_from('Original');

__PACKAGE__->inherit_methods_from(Original => qw( prev_version next_version ));

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
