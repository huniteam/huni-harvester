use utf8;
package HuNI::Harvest::Schema::Result::TransformType;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Result::TransformType

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Result>

=cut

use base 'HuNI::Harvest::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<harvest.transform_type>

=cut

__PACKAGE__->table("harvest.transform_type");

=head1 ACCESSORS

=head2 id

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns("id", { data_type => "text", is_nullable => 0 });

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 transforms

Type: has_many

Related object: L<HuNI::Harvest::Schema::Result::Transform>

=cut

__PACKAGE__->has_many(
  "transforms",
  "HuNI::Harvest::Schema::Result::Transform",
  { "foreign.type" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-10-02 12:32:09
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:nFt2/Gy0YI7D+99O/vCQuw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
