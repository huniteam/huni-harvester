use utf8;
package HuNI::Harvest::Schema::Result::ExtendedClean;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Result::ExtendedClean

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Result>

=cut

use base 'HuNI::Harvest::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<harvest.extended_clean>

=cut

__PACKAGE__->table("harvest.extended_clean");
__PACKAGE__->result_source_instance->view_definition(" SELECT c.id,\n    c.type,\n    c.original_name,\n    c.feed_id,\n    c.harvest_date,\n    c.file_id,\n    h.history,\n    ((c.harvest_date)::text = (h.history ->> 0)) AS is_latest\n   FROM (harvest.clean c\n     JOIN harvest.clean_history h USING (id, type, feed_id))");

=head1 ACCESSORS

=head2 id

  data_type: 'text'
  is_nullable: 1

=head2 type

  data_type: 'text'
  is_nullable: 1

=head2 original_name

  data_type: 'text'
  is_nullable: 1

=head2 feed_id

  data_type: 'text'
  is_nullable: 1

=head2 harvest_date

  data_type: 'date'
  is_nullable: 1

=head2 file_id

  data_type: 'text'
  is_nullable: 1

=head2 history

  data_type: 'json'
  is_nullable: 1

=head2 is_latest

  data_type: 'boolean'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "text", is_nullable => 1 },
  "type",
  { data_type => "text", is_nullable => 1 },
  "original_name",
  { data_type => "text", is_nullable => 1 },
  "feed_id",
  { data_type => "text", is_nullable => 1 },
  "harvest_date",
  { data_type => "date", is_nullable => 1 },
  "file_id",
  { data_type => "text", is_nullable => 1 },
  "history",
  { data_type => "json", is_nullable => 1 },
  "is_latest",
  { data_type => "boolean", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-20 05:15:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:JDaAPYZBAIOkm9Mg+nF8Mg

__PACKAGE__->inherit_relationships_from('Clean');
__PACKAGE__->inherit_methods_from(Clean => qw( name ));

__PACKAGE__->has_many(
    "versions",
    __PACKAGE__,
    {
        'foreign.id'      => 'self.id',
        'foreign.type'    => 'self.type',
        'foreign.feed_id' => 'self.feed_id',
    },
);

use Function::Parameters    qw( :strict );

method next_version() {
    my $datestr = $self->format_date($self->harvest_date);

    return $self->versions->search(
        {
            harvest_date => { '>' => $datestr },
        },
        {
            order_by => [{ -asc => 'harvest_date' }],
            rows => 1,
        })->single;
}

method prev_version() {
    my $datestr = $self->format_date($self->harvest_date);

    return $self->versions->search(
        {
            harvest_date => { '<' => $datestr },
        },
        {
            order_by => [{ -desc => 'harvest_date' }],
            rows => 1,
        })->single;
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
