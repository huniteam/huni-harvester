use utf8;
package HuNI::Harvest::Schema::Result::OriginalHistory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Result::OriginalHistory

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Result>

=cut

use base 'HuNI::Harvest::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<harvest.original_history>

=cut

__PACKAGE__->table("harvest.original_history");
__PACKAGE__->result_source_instance->view_definition(" SELECT original.name,\n    original.feed_id,\n    json_agg(original.harvest_date ORDER BY original.harvest_date DESC) AS history\n   FROM harvest.original\n  GROUP BY original.name, original.feed_id");

=head1 ACCESSORS

=head2 name

  data_type: 'text'
  is_nullable: 1

=head2 feed_id

  data_type: 'text'
  is_nullable: 1

=head2 history

  data_type: 'json'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "name",
  { data_type => "text", is_nullable => 1 },
  "feed_id",
  { data_type => "text", is_nullable => 1 },
  "history",
  { data_type => "json", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-20 23:12:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:EpUm/SE33LLrHADGsH8l+Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
