use utf8;
package HuNI::Harvest::Schema::Result::File;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Result::File

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Result>

=cut

use base 'HuNI::Harvest::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<harvest.file>

=cut

__PACKAGE__->table("harvest.file");

=head1 ACCESSORS

=head2 digest

  data_type: 'text'
  is_nullable: 0

=head2 content

  data_type: 'bytea'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "digest",
  { data_type => "text", is_nullable => 0 },
  "content",
  { data_type => "bytea", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</digest>

=back

=cut

__PACKAGE__->set_primary_key("digest");

=head1 RELATIONS

=head2 cleans

Type: has_many

Related object: L<HuNI::Harvest::Schema::Result::Clean>

=cut

__PACKAGE__->has_many(
  "cleans",
  "HuNI::Harvest::Schema::Result::Clean",
  { "foreign.file_id" => "self.digest" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 originals

Type: has_many

Related object: L<HuNI::Harvest::Schema::Result::Original>

=cut

__PACKAGE__->has_many(
  "originals",
  "HuNI::Harvest::Schema::Result::Original",
  { "foreign.file_id" => "self.digest" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-05 03:51:18
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:QVllrXFvojDbByRi6xE8qQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
