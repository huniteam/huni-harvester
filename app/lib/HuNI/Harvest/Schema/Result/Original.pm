use utf8;
package HuNI::Harvest::Schema::Result::Original;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Result::Original

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Result>

=cut

use base 'HuNI::Harvest::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<harvest.original>

=cut

__PACKAGE__->table("harvest.original");

=head1 ACCESSORS

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 feed_id

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 harvest_date

  data_type: 'date'
  is_foreign_key: 1
  is_nullable: 0

=head2 file_id

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 data

  data_type: 'json'
  is_nullable: 0

=head2 is_deleted

  data_type: 'boolean'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "name",
  { data_type => "text", is_nullable => 0 },
  "feed_id",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "harvest_date",
  { data_type => "date", is_foreign_key => 1, is_nullable => 0 },
  "file_id",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "data",
  { data_type => "json", is_nullable => 0 },
  "is_deleted",
  { data_type => "boolean", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</feed_id>

=item * L</harvest_date>

=item * L</name>

=back

=cut

__PACKAGE__->set_primary_key("feed_id", "harvest_date", "name");

=head1 RELATIONS

=head2 cleans

Type: has_many

Related object: L<HuNI::Harvest::Schema::Result::Clean>

=cut

__PACKAGE__->has_many(
  "cleans",
  "HuNI::Harvest::Schema::Result::Clean",
  {
    "foreign.feed_id"       => "self.feed_id",
    "foreign.harvest_date"  => "self.harvest_date",
    "foreign.original_name" => "self.name",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 feed

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::Feed>

=cut

__PACKAGE__->belongs_to(
  "feed",
  "HuNI::Harvest::Schema::Result::Feed",
  { id => "feed_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 file

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::File>

=cut

__PACKAGE__->belongs_to(
  "file",
  "HuNI::Harvest::Schema::Result::File",
  { digest => "file_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 harvest

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::Harvest>

=cut

__PACKAGE__->belongs_to(
  "harvest",
  "HuNI::Harvest::Schema::Result::Harvest",
  { date => "harvest_date", feed_id => "feed_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 invalid

Type: might_have

Related object: L<HuNI::Harvest::Schema::Result::Invalid>

=cut

__PACKAGE__->might_have(
  "invalid",
  "HuNI::Harvest::Schema::Result::Invalid",
  {
    "foreign.feed_id"       => "self.feed_id",
    "foreign.harvest_date"  => "self.harvest_date",
    "foreign.original_name" => "self.name",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-08 01:21:21
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:i2paylGzzvf97D8+WZSclA

__PACKAGE__->has_many(
    "versions",
    __PACKAGE__,
    {
        'foreign.feed_id' => 'self.feed_id',
        'foreign.name'    => 'self.name',
    },
);

use Function::Parameters    qw( :strict );

method next_version() {
    my $datestr = $self->format_date($self->harvest_date);

    return $self->versions->search(
        {
            harvest_date => { '>' => $datestr },
        },
        {
            order_by => [{ -asc => 'harvest_date' }],
            rows => 1,
        })->single;
}

method prev_version() {
    my $datestr = $self->format_date($self->harvest_date);

    return $self->versions->search(
        {
            harvest_date => { '<' => $datestr },
        },
        {
            order_by => [{ -desc => 'harvest_date' }],
            rows => 1,
        })->single;
}


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
