use utf8;
package HuNI::Harvest::Schema::Result::HarvestStatusReport;

=head1 NAME

HuNI::Harvest::Schema::Result::HarvestStatusReport;

=cut

use strict;
use warnings;

=head1 BASE CLASS: L<DBIx::Class::Core>

=cut

use base qw/DBIx::Class::Core/;
use Function::Parameters qw(:strict);
use Cpanel::JSON::XS;

__PACKAGE__->table_class('DBIx::Class::ResultSource::View');

# __PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSONB");
__PACKAGE__->load_components("InflateColumn::DateTime");

__PACKAGE__->table('harvest_status_report');
__PACKAGE__->result_source_instance->is_virtual(1);

my $sql = <<'SQL';
  SELECT harvest.provider.id             AS provider_id,
         harvest.provider.name           AS provider_name,
         harvest.feed.id                 AS feed_id,
         harvest.feed.feed_type_id       AS feed_type,
         harvest.feed.preprocess_type_id AS preprocess_type,
         harvest.feed.is_enabled         AS feed_is_enabled,
         /* harvest.feed.params             AS feed_params, */
         COALESCE(target_harvest.date, timestamp '-infinity'::date)
                                         AS harvest_date,
         COALESCE(target_harvest.status_id, 'no harvest')
                                         AS harvest_status,
         COALESCE(original_count.original_count,0)
                                         AS original_count,
         COALESCE(clean_count.clean_count,0)
                                         AS clean_count,
         COALESCE(invalid_count.invalid_count,0)
                                         AS invalid_count,
         COALESCE(solr_count.solr_count,0)
                                         AS solr_count,
         COALESCE(no_solr_count.no_solr_count,0)
                                         AS no_solr_count,
         ?::date = target_harvest.date   AS is_latest
  FROM harvest.provider
  JOIN harvest.feed
  ON  harvest.feed.provider_id = harvest.provider.id
  AND harvest.feed.is_enabled
  LEFT JOIN (
    SELECT * FROM harvest.harvest
  ) AS target_harvest
    ON  harvest.feed.id = target_harvest.feed_id
    /* AND target_harvest.status_id = 'succeeded' */
    AND target_harvest.date = (
      /* get the most recent successful harvest for each feed */
      SELECT MAX(most_recent_harvest.date)
      FROM harvest.harvest AS most_recent_harvest
      WHERE target_harvest.feed_id = most_recent_harvest.feed_id
        /* AND most_recent_harvest.status_id = 'succeeded' */
        AND most_recent_harvest.date <= ?
    )
  LEFT JOIN (
    SELECT feed_id, harvest_date, COUNT(*) AS original_count
    FROM harvest.original
    GROUP BY feed_id, harvest_date
  ) AS original_count
    ON  original_count.feed_id = harvest.feed.id
    AND original_count.harvest_date = target_harvest.date
  LEFT JOIN (
    SELECT feed_id, harvest_date, COUNT(*) AS clean_count
    FROM harvest.clean
    GROUP BY feed_id, harvest_date
  ) AS clean_count
    ON  clean_count.feed_id = harvest.feed.id
    AND clean_count.harvest_date = target_harvest.date
  LEFT JOIN (
    SELECT hc.feed_id, harvest_date, COUNT(*) AS solr_count
    FROM harvest.clean hc
    JOIN harvest.transform ht
      ON ht.feed_id = hc.feed_id
     AND ht.file_type = hc.type
     AND ht.type = 'solr'
    GROUP BY hc.feed_id, harvest_date
  ) AS solr_count
    ON  solr_count.feed_id = harvest.feed.id
    AND solr_count.harvest_date = target_harvest.date
  LEFT JOIN (
    SELECT hc.feed_id, harvest_date, COUNT(*) AS no_solr_count
    FROM harvest.clean hc
    LEFT JOIN harvest.transform ht
      ON ht.feed_id = hc.feed_id
     AND ht.file_type = hc.type
     AND ht.type = 'solr'
    WHERE ht.feed_id IS NULL
    GROUP BY hc.feed_id, harvest_date
  ) AS no_solr_count
    ON  no_solr_count.feed_id = harvest.feed.id
    AND no_solr_count.harvest_date = target_harvest.date
  LEFT JOIN (
    SELECT feed_id, harvest_date, COUNT(*) AS invalid_count
    FROM harvest.invalid
    GROUP BY feed_id, harvest_date
  ) AS invalid_count
    ON  invalid_count.feed_id = harvest.feed.id
    AND invalid_count.harvest_date = target_harvest.date
  ORDER BY harvest.provider.id,
           harvest.feed.id
SQL

__PACKAGE__->result_source_instance->view_definition($sql);

__PACKAGE__->add_columns(
  'provider_id' => {
    data_type => 'text',
  },
  'provider_name' => {
    data_type => 'text',
  },
  'feed_id' => {
    data_type => 'text',
  },
  'feed_type' => {
    data_type => 'text',
  },
  'preprocess_type' => {
    data_type => 'text',
  },
  'feed_is_enabled' => {
    data_type => 'boolean',
  },
  #'feed_params' => {
  #  data_type => 'json',
  #},
  'harvest_date' => {
    data_type => 'date',
  },
  'harvest_status' => {
    data_type => 'text',
  },
  'original_count' => {
    data_type => 'integer',
  },
  'clean_count' => {
    data_type => 'integer',
  },
  'invalid_count' => {
    data_type => 'integer',
  },
  'solr_count' => {
    data_type => 'integer',
  },
  'no_solr_count' => {
    data_type => 'integer',
  },
  'is_latest' => {
    data_type => 'boolean',
  },
);

method non_json_columns() {
    return qw( );
}

my $column_handler = {
    boolean   => fun ($value) { $value ? Cpanel::JSON::XS::true : Cpanel::JSON::XS::false },
    date      => fun ($value) { $value->strftime('%F') },
    numeric   => fun ($value) { 0 + $value },
    timestamp => fun ($value) { $value->strftime('%FT%T.%3NZ') },
    'timestamp with time zone' => fun ($value) { $value->strftime('%FT%T.%3NZ') if $value },
};

# Use this instead of as_hashes.
# If you need to exclude columns, create a non_json_columns method on that
# result class. See Result/Household.pm for an example.
method for_json() {

    # Get the basic hash of inflated values
    my $data = { $self->get_inflated_columns };
    delete @{ $data }{ $self->non_json_columns };

    # This contains the column metadata
    my $columns_info = $self->result_source->columns_info;

    for my $column (keys %$data) {
        my $info = $columns_info->{$column};
        next unless $info;  # virtual columns like record_count have no metadata

        # Fixup column values depending on type
        my $data_type = $info->{data_type};
        if ( my $handler = $column_handler->{$data_type} ) {
            $data->{$column} = $handler->( $data->{$column} );
        }
    }

    return $data;
}

1;
