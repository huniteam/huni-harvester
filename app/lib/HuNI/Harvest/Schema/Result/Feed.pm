use utf8;
package HuNI::Harvest::Schema::Result::Feed;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Result::Feed

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Result>

=cut

use base 'HuNI::Harvest::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<harvest.feed>

=cut

__PACKAGE__->table("harvest.feed");

=head1 ACCESSORS

=head2 id

  data_type: 'text'
  is_nullable: 0

=head2 provider_id

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 feed_type_id

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 preprocess_type_id

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 is_enabled

  data_type: 'boolean'
  is_nullable: 0

=head2 params

  data_type: 'json'
  is_nullable: 0

=head2 links_config

  data_type: 'json'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "text", is_nullable => 0 },
  "provider_id",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "feed_type_id",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "preprocess_type_id",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "is_enabled",
  { data_type => "boolean", is_nullable => 0 },
  "params",
  { data_type => "json", is_nullable => 0 },
  "links_config",
  { data_type => "json", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 cleans

Type: has_many

Related object: L<HuNI::Harvest::Schema::Result::Clean>

=cut

__PACKAGE__->has_many(
  "cleans",
  "HuNI::Harvest::Schema::Result::Clean",
  { "foreign.feed_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 feed_type

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::FeedType>

=cut

__PACKAGE__->belongs_to(
  "feed_type",
  "HuNI::Harvest::Schema::Result::FeedType",
  { id => "feed_type_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 harvests

Type: has_many

Related object: L<HuNI::Harvest::Schema::Result::Harvest>

=cut

__PACKAGE__->has_many(
  "harvests",
  "HuNI::Harvest::Schema::Result::Harvest",
  { "foreign.feed_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 invalids

Type: has_many

Related object: L<HuNI::Harvest::Schema::Result::Invalid>

=cut

__PACKAGE__->has_many(
  "invalids",
  "HuNI::Harvest::Schema::Result::Invalid",
  { "foreign.feed_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 originals

Type: has_many

Related object: L<HuNI::Harvest::Schema::Result::Original>

=cut

__PACKAGE__->has_many(
  "originals",
  "HuNI::Harvest::Schema::Result::Original",
  { "foreign.feed_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 preprocess_type

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::PreprocessType>

=cut

__PACKAGE__->belongs_to(
  "preprocess_type",
  "HuNI::Harvest::Schema::Result::PreprocessType",
  { id => "preprocess_type_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 provider

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::Provider>

=cut

__PACKAGE__->belongs_to(
  "provider",
  "HuNI::Harvest::Schema::Result::Provider",
  { id => "provider_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 transforms

Type: has_many

Related object: L<HuNI::Harvest::Schema::Result::Transform>

=cut

__PACKAGE__->has_many(
  "transforms",
  "HuNI::Harvest::Schema::Result::Transform",
  { "foreign.feed_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-11-22 10:38:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:D9uNewDr+P5QqhBTJ2n69A

__PACKAGE__->has_many(
  "extended_cleans",
  "HuNI::Harvest::Schema::Result::ExtendedClean",
  { "foreign.feed_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

__PACKAGE__->has_many(
  "extended_originals",
  "HuNI::Harvest::Schema::Result::ExtendedOriginal",
  { "foreign.feed_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

use Function::Parameters    qw( :strict );
use Log::Any                qw( $log );

method create_harvest($datestr, $delete_existing) {
    my $harvest = $self->harvests->find({ date => $datestr });
    if ($harvest) {
        my $feed_id = $self->id;
        if (!$delete_existing) {
            $log->error("Harvest already exists for $feed_id on $datestr");
            return;
        }
        $log->info("Removing existing $datestr harvest for $feed_id");

        $harvest->invalids->delete;
        $harvest->cleans->delete;
        $harvest->originals->delete;
        $harvest->delete;
    }

    return $self->add_to_harvests({
        date      => $datestr,
        status_id => 'in-progress',
    });
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
