use utf8;
package HuNI::Harvest::Schema::Result::Harvest;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Result::Harvest

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Result>

=cut

use base 'HuNI::Harvest::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<harvest.harvest>

=cut

__PACKAGE__->table("harvest.harvest");

=head1 ACCESSORS

=head2 feed_id

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 date

  data_type: 'date'
  is_nullable: 0

=head2 status_id

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "feed_id",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "date",
  { data_type => "date", is_nullable => 0 },
  "status_id",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</feed_id>

=item * L</date>

=back

=cut

__PACKAGE__->set_primary_key("feed_id", "date");

=head1 RELATIONS

=head2 cleans

Type: has_many

Related object: L<HuNI::Harvest::Schema::Result::Clean>

=cut

__PACKAGE__->has_many(
  "cleans",
  "HuNI::Harvest::Schema::Result::Clean",
  {
    "foreign.feed_id"      => "self.feed_id",
    "foreign.harvest_date" => "self.date",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 feed

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::Feed>

=cut

__PACKAGE__->belongs_to(
  "feed",
  "HuNI::Harvest::Schema::Result::Feed",
  { id => "feed_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 invalids

Type: has_many

Related object: L<HuNI::Harvest::Schema::Result::Invalid>

=cut

__PACKAGE__->has_many(
  "invalids",
  "HuNI::Harvest::Schema::Result::Invalid",
  {
    "foreign.feed_id"      => "self.feed_id",
    "foreign.harvest_date" => "self.date",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 originals

Type: has_many

Related object: L<HuNI::Harvest::Schema::Result::Original>

=cut

__PACKAGE__->has_many(
  "originals",
  "HuNI::Harvest::Schema::Result::Original",
  {
    "foreign.feed_id"      => "self.feed_id",
    "foreign.harvest_date" => "self.date",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 status

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Result::HarvestStatusType>

=cut

__PACKAGE__->belongs_to(
  "status",
  "HuNI::Harvest::Schema::Result::HarvestStatusType",
  { id => "status_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-13 04:53:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:lNUmTUtOgPtd07gKu3TR/A

__PACKAGE__->has_many(
  "extended_cleans",
  "HuNI::Harvest::Schema::Result::ExtendedClean",
  {
    "foreign.feed_id"      => "self.feed_id",
    "foreign.harvest_date" => "self.date",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);

use Function::Parameters qw( :strict );

method previous() {
    my $datestr = $self->format_date($self->date);
    $self->feed->harvests->search(
        {
            date => { '<' => $datestr },
        },
        {
            order_by => { -desc => 'date' },
            limit => 1,
        })->first;
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
