use utf8;
package HuNI::Harvest::Schema::Result::CleanHistory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Result::CleanHistory

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Result>

=cut

use base 'HuNI::Harvest::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<harvest.clean_history>

=cut

__PACKAGE__->table("harvest.clean_history");
__PACKAGE__->result_source_instance->view_definition(" SELECT clean.id,\n    clean.type,\n    clean.feed_id,\n    json_agg(clean.harvest_date ORDER BY clean.harvest_date DESC) AS history\n   FROM harvest.clean\n  GROUP BY clean.id, clean.type, clean.feed_id");

=head1 ACCESSORS

=head2 id

  data_type: 'text'
  is_nullable: 1

=head2 type

  data_type: 'text'
  is_nullable: 1

=head2 feed_id

  data_type: 'text'
  is_nullable: 1

=head2 history

  data_type: 'json'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "text", is_nullable => 1 },
  "type",
  { data_type => "text", is_nullable => 1 },
  "feed_id",
  { data_type => "text", is_nullable => 1 },
  "history",
  { data_type => "json", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-20 01:33:46
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:d6r1KUYqNnAtL30uhx8LEA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
