package HuNI::Harvest::Schema::ResultSet::File;

use 5.24.0;
use warnings;
use parent 'HuNI::Harvest::Schema::ResultSet';

use DBD::Pg                     qw( PG_BYTEA );
use Encode                      qw( encode_utf8 );
use Function::Parameters        qw( :strict );
use Log::Any                    qw( $log );
use Try::Tiny;

method insert_content_utf8($utf8) {
    my $statement = 'SELECT harvest.insert_file(?)';
    my $result = $self->result_source->storage->dbh_do(method($dbh) {
        my $sth = $dbh->prepare($statement);
        $sth->bind_param(1, $utf8, { pg_type => PG_BYTEA });
        $sth->execute;
        $sth->fetchrow_arrayref;
    });
    return $result->[0];
}

method insert_content_wide($wide) {
    return $self->insert_content_utf8(encode_utf8($wide));
}

method deleted_file_content() {
    return "<deleted></deleted>\n";
}

1;
