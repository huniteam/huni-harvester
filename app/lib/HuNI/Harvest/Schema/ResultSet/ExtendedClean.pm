package HuNI::Harvest::Schema::ResultSet::ExtendedClean;

use 5.26.0;
use warnings;
use parent 'HuNI::Harvest::Schema::ResultSet';

use Function::Parameters    qw( :strict );

method latest() {
    return $self->search({ is_latest => 1 });
}

# $huni_name is like: 'PDSC***item***EP2-TEXTS'
# - can use *** or ::: as the separator
# - optional .xml suffix is ignored
# $date is YYYY-MM-DD
method find_filename(:$huni_name, :$date = 'latest') {
    my $sep = qr/:::|\*\*\*/;
    $huni_name =~ s/\.xml$//;
    my ($provider_id, $record_type, $record_id) = split($sep, $huni_name)
        or return;

    return $self->find_file(
        provider_id => $provider_id,
        record_type => $record_type,
        record_id   => $record_id,
        date        => $date,
    );
}

# For a filename like:  'PDSC***item***EP2-TEXTS'
#   $provider_is 'PDSC'
#   $record_type is 'item'
#   $record_id is 'EP2-TEXTS'
method find_file(:$provider_id, :$record_type, :$record_id, :$date = 'latest') {
    my $query = {
        type                => $record_type,
        'me.id'             => $record_id,
        'feed.provider_id'  => $provider_id,
    };

    if ($date eq 'latest') {
        $query->{is_latest} = 1;
    }
    else {
        $query->{harvest_date} = $date;
    };

    my $attr = { prefetch => [qw( file feed )] };
    return $self->search($query, $attr)->single;
}

1;
