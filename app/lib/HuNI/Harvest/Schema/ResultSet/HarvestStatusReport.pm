package HuNI::Harvest::Schema::ResultSet::HarvestStatusReport;

use 5.26.0;
use warnings;
use parent 'HuNI::Harvest::Schema::ResultSet';

use Function::Parameters qw( :strict );

method for_date($date) {
    return $self->for_datestr($self->result_source->schema->format_date($date));
}

method for_datestr($datestr) {
    return $self->search({ }, { bind => [ $datestr, $datestr ] });
}

1;
