package HuNI::Harvest::Schema::ResultSet::ExtendedOriginal;

use 5.26.0;
use warnings;
use parent 'HuNI::Harvest::Schema::ResultSet';

use Function::Parameters    qw( :strict );

method latest() {
    return $self->search({ is_latest => 1 });
}

# $provider_id is the external name. eg DAAO (not DAAO-rif)
# $original_filename is the non-hunified filename (as seen in original/)
# $date is YYYY-MM-DD
# Returns an ExtendedOriginal row, or null.
method find_file(:$provider_id, :$original_filename, :$date = 'latest') {
    my $query = {
        name                => $original_filename,
        'feed.provider_id'  => $provider_id,
    };

    if ($date eq 'latest') {
        $query->{is_latest} = 1;
    }
    else {
        $query->{harvest_date} = $date;
    };

    my $attr = { prefetch => [qw( file )], join => [qw( feed )] };

    return $self->search($query, $attr)->single;
}

1;
