package HuNI::Harvest::Schema::ResultSet;

use 5.24.0;
use warnings;

use parent 'DBIx::Class::ResultSet';

use Function::Parameters qw( :strict );

# Use this instead of as_hashes.
# If you need to exclude columns, create a non_json_columns method on that
# result class. See Result/Household.pm for an example.
method for_json() {
    return [ map { $_->for_json } $self->all ];
}

method format_datetime($datetime) {
    return $self->result_source->schema->format_datetime($datetime);
}

method format_date($date) {
    return $self->result_source->schema->format_date($date);
}

1;
