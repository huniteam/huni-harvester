package HuNI::Harvest::Schema::Result;

use 5.24.0;
use warnings;

use parent 'DBIx::Class::Core';

use Function::Parameters qw( :strict );
use Cpanel::JSON::XS;

# This code is hacked from DBIx::Class::ResultSource::MultipleTableInheritance
method inherit_relationships_from($class: $source) {
    # Fully-qualified class names are prefixed with a '+'.
    # Relative class names are not.
    if (! ($source =~ s/^\+//)) {
        $source = __PACKAGE__ . '::' . $source;
    }
    Class::C3::Componentised->ensure_class_loaded($source);

    for my $rel ($source->relationships) {
        my $rel_info = $source->relationship_info($rel);

        $class->add_relationship(
            $rel, $rel_info->{source}, $rel_info->{cond},
            # extra key first to default it
            { originally_defined_in => 'Link', %{$rel_info->{attrs}} },
        );
    }
}

method inherit_methods_from($class: $source, @symbols) {
    # Fully-qualified class names are prefixed with a '+'.
    # Relative class names are not.
    if (! ($source =~ s/^\+//)) {
        $source = __PACKAGE__ . '::' . $source;
    }
    Class::C3::Componentised->ensure_class_loaded($source);

    for my $symbol (@symbols) {
        no strict 'refs';
        *{ $class . '::' . $symbol } = *{ $source . '::' . $symbol };
    }
}

method non_json_columns() {
    return qw( );
}

my $column_handler = {
    boolean   => fun ($value) { $value ? Cpanel::JSON::XS::true : Cpanel::JSON::XS::false },
    date      => fun ($value) { $value->strftime('%F') },
    numeric   => fun ($value) { 0 + $value },
    timestamp => fun ($value) { $value->strftime('%FT%T.%3NZ') },
    'timestamp with time zone' => fun ($value) { $value->strftime('%FT%T.%3NZ') if $value },
};

# Use this instead of as_hashes.
# If you need to exclude columns, create a non_json_columns method on that
# result class. See Result/Household.pm for an example.
method for_json() {

    # Get the basic hash of inflated values
    my $data = { $self->get_inflated_columns };
    delete @{ $data }{ $self->non_json_columns };

    # This contains the column metadata
    my $columns_info = $self->result_source->columns_info;

    for my $column (keys %$data) {
        my $info = $columns_info->{$column};
        next unless $info;  # virtual columns like record_count have no metadata

        # Fixup column values depending on type
        my $data_type = $info->{data_type};
        if ( my $handler = $column_handler->{$data_type} ) {
            $data->{$column} = $handler->( $data->{$column} );
        }
    }

    return $data;
}

method format_datetime($datetime) {
    return $self->result_source->schema->format_datetime($datetime);
}

method format_date($date) {
    return $self->result_source->schema->format_date($date);
}

1;
