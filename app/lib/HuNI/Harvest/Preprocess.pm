package HuNI::Harvest::Preprocess;

use 5.20.0;
use warnings;

use Class::Load             qw( load_class );
use Cpanel::JSON::XS        qw( encode_json );
use Encode                  qw( decode_utf8 );
use Function::Parameters    qw( :strict );
use HuNI::Harvest::Inserter qw( );
use Log::Any                qw( $log );
use Try::Tiny;
use XML::LibXML             qw( );

use Moo;

has harvest => (
    is => 'ro',
    required => 1,
);

has preprocessor => (
    is => 'lazy',
);

has schema => (
    is => 'lazy',
    builder => method() {
        $self->harvest->result_source->schema;
    },
);

# replace => 0 means only add new files
# replace => 1 means blow away this feed and preprocess from scratch
has replace => (
    is => 'ro',
    default => 0,
);

has seen => (
    is => 'lazy',
    builder => method() {
        return { };
    },
);

method _build_preprocessor() {
    my $preprocess_type = $self->harvest->feed->preprocess_type_id;
    my $class_name = join('::', __PACKAGE__, 'Handler', $preprocess_type);
    return load_class($class_name)->new;
}

method run_preprocess_stage() {
    my $harvest = $self->harvest;

    my $name = join(' ',
        $harvest->feed_id,
        $harvest->format_date($harvest->date),
    );

    if ($self->replace) {
        $self->remove_preprocessed_files;
    }
    elsif ($harvest->cleans->count + $harvest->invalids->count) {
        $log->info("Skipping $name - already preprocessed");
        return;
    }
    if ($harvest->originals->count == 0) {
        $log->info("Skipping $name - no original files");
        return;
    }
    $self->preprocess_files;
}

method remove_preprocessed_files() {
    for my $type (qw( clean invalid )) {
        my $relation = $type . 's';
        my $count = $self->harvest->$relation->count;
        next unless $count;

        $log->info("Removing $count $type files");
        $self->harvest->$relation->delete;
    }
}

method remove_invalid_files() {
    my $count = $self->harvest->invalids->count;
    return unless $count;
    $log->info("Removing $count invalid files");
    $self->harvest->invalids->delete;
}

method preprocess_files() {
    my $feed_id = $self->harvest->feed_id;
    my $datestr = $self->harvest->format_date($self->harvest->date);

    $log->info("Preprocessing $feed_id $datestr");

    my $clean_inserter = HuNI::Harvest::Inserter->new(
        resultset => $self->schema->resultset('Clean'),
        columns => [qw(
            feed_id harvest_date
            original_name id type file_id
        )],
        constant_values => [ $feed_id, $datestr ],
    );
    my $invalid_inserter = HuNI::Harvest::Inserter->new(
        resultset => $self->schema->resultset('Invalid'),
        columns => [qw(
            feed_id harvest_date
            original_name error
        )],
        constant_values => [ $feed_id, $datestr ],
    );

    my $files_rs = $self->schema->resultset('File');
    my $deleted_file_id =
        $files_rs->insert_content_wide($files_rs->deleted_file_content);
    my $originals = $self->harvest->search_related(originals => { }, {
        prefetch => [qw( file )],
        order_by => [{ -asc => 'is_deleted' }],  # false, then true
    });
    # Note that we order the files so that the deletes come last.
    # This takes care of the rename case, where both old name and new name will
    # generate the same clean file.
    my $preprocessor = $self->preprocessor;
    my %progress = (
        originals => $originals->count,
    );
    my $provider_id = $self->harvest->feed->provider_id;
    while (my $original = $originals->next) {
        if ($original->is_deleted) {
            my $previous = $self->find_previous_valid_original($original);
            if (!$previous) {
                $progress{invalid}++;
                $invalid_inserter->insert($original->name,
                    'Deleted with no previous valid original');
            }
            else {
                for my $clean ($previous->cleans) {
                    if ($self->seen->{$clean->type}->{$clean->id}++) {
                        my $name = $original->name;
                        $log->debug("Not deleting $name - already seen");
                        $progress{renamed}++;
                        next;
                    }
                    $log->debug($original->name . ' -> ' .
                                $clean->name . ' DELETED');
                    $progress{deleted}++;
                    $clean_inserter->insert(
                        $original->name,
                        $clean->id,
                        $clean->type,
                        $deleted_file_id,
                    );
                }
            }
            next;
        }
        my @clean;
        try {
            @clean = $self->preprocessor->preprocess($original);
        }
        catch {
            chomp;
            my $error = $_;
            my $name = $original->name;
            my $short_error = ($error =~ /\A(.*?)$/ms)[0];
            $log->error("Preprocess $name failed: $short_error");
            $invalid_inserter->insert($name, $error);
            $progress{invalid}++;
        };
        for my $clean (@clean) {
            my $name = join(':::', $provider_id, $clean->{type}, $clean->{id});
            if ($self->seen->{$clean->{type}}->{$clean->{id}}++) {
                $log->warning("Skipping $name - already seen");
                next;
            }

            if (exists $clean->{content}) {
                $clean->{file_id} =
                    $files_rs->insert_content_wide($clean->{content});
            }

            $clean_inserter->insert(
                $original->name,
                $clean->{id},
                $clean->{type},
                $clean->{file_id} // $original->file_id,
            );

            $log->debug($original->name . " -> $name");
            $progress{clean}++;
        }
        $log->progress(\%progress);
    }

    $clean_inserter->flush;
    $invalid_inserter->flush;

    $log->progress(\%progress, 1);
}

method find_previous_valid_original($original) {
    my $name = $original->name . ' at '
             . $original->format_date($original->harvest_date);
    $log->debug("Finding replacement for deleted original $name");

    while (1) {
        $original = $original->prev_version;
        if (!$original) {
            $log->warning("No valid original found for $name - marking as invalid");
            return;
        }

        my $prev_date = $original->format_date($original->harvest_date);
        if (!$original->invalid) {
            $log->debug("Using previous original from $prev_date for deleted $name");
            return $original;
        }

        $log->debug("Previous original for $name is from $prev_date, but is not valid");
    }
}

1;
