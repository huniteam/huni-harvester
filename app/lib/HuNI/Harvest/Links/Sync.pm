package HuNI::Harvest::Links::Sync;

use 5.26.0;
use warnings;

use Cpanel::JSON::XS        qw( encode_json );
use Data::Dumper::Concise   qw( Dumper );
use Function::Parameters    qw( :strict );
use HuNI::Harvest::Inserter qw( );
use Log::Any                qw( $log );
use Path::Tiny              qw( path );
use YAML::XS                qw( Load );

use Try::Tiny;

use Moo;

has schema => (
    is => 'ro',
    required => 1,
    handles => [qw( resultset )],
);

has yaml_file => (
    is => 'lazy',
    default => sub { die "yaml_file not specified\n"; }
);

has config => (
    is => 'lazy',
    builder => \&_build_config,
);

method _build_config() {
    my $yaml = path($self->yaml_file)->slurp_utf8;
    my $rules = Load($yaml);

    my $errors = 0;
    my %config;
    for my $rule (@$rules) {
        if (my $feed_id = $self->check_rule($rule)) {
            push(@{ $config{$feed_id} }, $rule);
        }
        else {
            $log->error('Errors in rule:');
            $log->error(Dumper($rule));
            $errors++;
            next;
        }
    }

    if ($errors) {
        die "Errors: $errors -- aborting\n";
    }

    return \%config;
}

method sync() {
    return $self->schema->txn_do(sub {
        my $feed_rs = $self->resultset('Feed');
        while (my $row = $feed_rs->next) {
            my $config = $self->config->{$row->id};
            $row->update({ links_config => $config });
        }
    });
}

method check_rule($rule) {
    my $feed_id = $rule->{match}->{from}->{feed_id};
    return $feed_id;
}

#method _do_sync($configs) {
#    my $errors = 0;
#    for my $config (@$configs) {
#        $errors += $self->_check_config($config);
#    }
#    return if $errors > 0;
#
#    $self->resultset('LinksConfig')->delete;
#    my $inserter = HuNI::Harvest::Inserter->new(
#        resultset => $self->resultset('LinksConfig'),
#        columns => [qw( feed_id priority config )],
#    );
#
#    my %next_priority;
#    for my $config (@$configs) {
#        my $feed_id = $config->{match}->{from}->{feed_id};
#        if (!$feed_id) {
#            $log->error(Dumper($config));
#            return;
#        }
#        my $priority = ++ $next_priority{$feed_id};
#        $inserter->insert(
#            $feed_id,
#            $priority,
#            encode_json($config),
#        );
#    }
#    $inserter->flush;
#}
#
#method _check_config($config) {
#    return 0;
#}
#
#fun _flatten_hash($in) {
#    my $out = { };
#
#    for my $key (keys %$in) {
#        my $value = $in->{$key};
#        my $rv = ref $value;
#        if ($rv eq 'HASH') {
#            my $sub = _flatten_hash($value);
#            for my $sk (keys %$sub) {
#                $out->{"$key.$sk"} = $sub->{$sk};
#            }
#        }
#        elsif ($rv eq '') {
#            $out->{$key} = $value;
#        }
#        else {
#            die "Unexpected $rv for $key - only support scalars and hashes";
#        }
#    }
#
#    return $out;
#}

1;
