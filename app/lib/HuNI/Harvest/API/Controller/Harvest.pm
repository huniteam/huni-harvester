package HuNI::Harvest::API::Controller::Harvest;
use 5.24.0;
use warnings;
use Function::Parameters {
    fun => 'function_strict', method => 'method_strict',
    route => { defaults => 'method', 'shift' => '$app', strict => 1 }
};

# Created by SD::OpenAPI::Loader::Dancer2 v0.0.5 @ 2017-02-24 02:18:53

use HuNI::Harvest::Util    qw( resultset schema );
use Try::Tiny;
use Time::Piece;

=head1 NAME

HuNI::Harvest::API::Controller::Status - a controller

=head1 ROUTES

=head2 getDates

GET /harvest/dates

Get array of harvest dates

=cut

route getDates($params, $metadata) {

    return resultset('Harvest')->search(
        {},
        {
          columns  => [ qw/date/ ],
          distinct => 1,
          order_by => 'date DESC',
        }
    )->for_json();
}

=head2 getStatus

GET   /harvest/status?date=YYYY-MM-DD

Latest harvest status or status as at a specific date

=cut

route getStatus($params, $metadata) {

    my $harvest_date;

    if (! $params->{date} ) {
        $harvest_date = schema->today;
    }
    else {
        $harvest_date = $params->{date};
    }

    return resultset('HarvestStatusReport')
               ->for_date($harvest_date)
               ->for_json();

}

1;
