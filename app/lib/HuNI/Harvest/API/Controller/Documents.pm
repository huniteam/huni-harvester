package HuNI::Harvest::API::Controller::Documents;
use 5.24.0;
use warnings;
use Function::Parameters {
    fun => 'function_strict', method => 'method_strict',
    route => { defaults => 'method', 'shift' => '$app', strict => 1 }
};

use HuNI::Harvest::Util qw(
    resultset schema links_transformer solr_transformer
);
use Try::Tiny;

=head1 NAME

HuNI::Harvest::API::Controller::Documents - a controller

=head1 ROUTES

=head2 getOriginalDocument

GET   /documents/original/{provider_id}/{filename}?date=YYYY-MM-DD:

Find a specific document from the aggregate optionally from a specific
harvest identified by date

=cut

route getOriginal($params, $metadata) {

    if ( ! exists $params->{date} ) {
        $params->{date} = 'latest';
    }
    else {
        $params->{date} = schema->format_date($params->{date});
    }

    $params->{original_filename} = delete $params->{filename};

    my $file = resultset('ExtendedOriginal')->find_file(%$params);

    my $content_type = 'application/xml';
    if ( $file->count_related('invalid') ) {
        $content_type = 'text/plain';
    }

    if ( ! $file ) {
        $app->response->status(404);
        $app->send_as('html', "$params->{provider_id}/$params->{original_filename} at $params->{date} not found");
    };

    # The file content is already utf8 encoded. We need to use send_file here -
    # it sends the content as-is. If we use send_as, it utf8 encodes it again.
    $app->send_file(\$file->file->content, content_type => $content_type);
}

# /documents/{document_type}/{provider_id}/{record_type}/{record_id}:

route getCleanOrSolr($params, $metadata) {

    my $document_type = delete $params->{document_type};
    if (exists $params->{date}) {
        $params->{date} = schema->format_date($params->{date});
    }
    else {
        $params->{date} = 'latest';
    }

    my $file = resultset('ExtendedClean')->find_file(%$params);
    if (my $content = transform_file($file, $document_type)) {
        $app->send_file(\$content, content_type => 'application/xml');
    }
    $app->response->status(404);
    $app->send_as('html', "$params->{huni_name} at $params->{date} not found");
}

# /documents/{document_type}/{huni_name}:

route getCleanOrSolrByName($params, $metadata) {

    my $document_type = delete $params->{document_type};
    if (exists $params->{date}) {
        $params->{date} = schema->format_date($params->{date});
    }
    else {
        $params->{date} = 'latest';
    }

    my $file = resultset('ExtendedClean')->find_filename(%$params);

    if (my $content = transform_file($file, $document_type)) {
        $app->send_file(\$content, content_type => 'application/xml');
    }
    $app->response->status(404);
    $app->send_as('html', "$params->{huni_name} at $params->{date} not found");
}

my %clean_transformer = (
    clean => fun($clean) { $clean->file->content                     },
    solr  => fun($clean) { solr_transformer->transform($clean)       },
    links => fun($clean) { links_transformer->transform_only($clean) },
);

fun transform_file($clean, $type) {
    return unless $clean;

    my $transformer = $clean_transformer{$type}
        or return;

    return $transformer->($clean);
}

=head2 getByFeedDateType

GET /documents/{feed_id}/{date}/{document_type}

List feed documents harvested on a specific date for a specific type

=cut


route getByFeedDateType($params, $metadata) {

    my $type_map = {
        'original' => 'Original',
        'clean'    => 'Clean',
        'invalid'  => 'Invalid',
        'solr'     => 'Clean',
        'no-solr'  => 'Clean',
    };

    fun prepare_doc_obj($params, $document) {

        my $doc_obj;

        if ( $params->{document_type} eq 'original'
          || $params->{document_type} eq 'invalid' )
        {
            $doc_obj->{href} = '/api/documents' .
                               '/original' .
                               '/' . $document->feed->provider_id;

            if ( $params->{document_type} eq 'original' ) {
                $doc_obj->{name} = $document->name;
                $doc_obj->{href} = $doc_obj->{href} .
                                   '/' . $document->name;
            }
            else {
                $doc_obj->{name} = $document->original_name;
                $doc_obj->{href} = $doc_obj->{href} .
                                   '/' . $document->original_name;
                $doc_obj->{error} = $document->error;
            }
        }

        if ( $params->{document_type} eq 'clean'
          || $params->{document_type} =~ /solr$/ )
        {

            my $document_type = $params->{document_type} eq 'no-solr'
                                ? 'clean'
                                : $params->{document_type};

            $doc_obj = {
                name => $document->name,
                href => '/api/documents' .
                        '/' . $document_type .
                        '/' . $document->name,
            };
        }

        $doc_obj->{document_type} = $params->{document_type};
        $doc_obj->{href}          = $doc_obj->{href} . '?date='
                                                     . $params->{datestr};

        return $doc_obj;
    }

    $params->{datestr} = schema->format_date($params->{date});

    my $query = {
        harvest_date    => $params->{datestr},
        'me.feed_id'    => $params->{feed_id},
    };
    my $attr = {
        prefetch => 'feed',
    };

    # Solr query needs to join against the transforms table so we only show
    # docs with matching transforms.

    if ($params->{document_type} =~ /solr$/) {

        # Match 'solr' transforms for solr and IS NULL (undef) for 'no-solr'
        my $transforms_type = $params->{document_type} eq 'solr'
                              ? 'solr'
                              : undef;

        $query->{'transforms.type'} = $transforms_type;
        $attr->{join} = 'transforms';
    }

    my $table = $type_map->{$params->{document_type}};
    my $rs = resultset($table)->search($query, $attr);

    return [ map { prepare_doc_obj($params, $_) } $rs->all ];
}

1;
