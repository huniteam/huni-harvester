package HuNI::Harvest::API::Controller::Feed;

use 5.26.0;
use warnings;

use HuNI::Harvest::Util qw( resultset );
use XML::RSS            qw( );

use Function::Parameters {
    fun => 'function_strict', method => 'method_strict',
    route => { defaults => 'method', 'shift' => '$app', strict => 1 }
};

route rss($params, $metadata) {
    my $xml = create_rss();
    return $app->send_file(\$xml, content_type => $metadata->{produces}->[0]);
}

fun create_rss() {
    my $rss = XML::RSS->new(version => '2.0');

    $rss->channel(
        title => 'HuNI Harvest Status',
        link  => 'https://harvest.huni.net.au',
        description => 'Reporting feed for the HuNI nightly harvest',
    );

    my $date_str = resultset('Harvest')->get_column('date')->max;
    my $harvest = get_harvest($date_str);
    my $title = "HuNI harvest for $date_str";
    my $description = make_description($date_str, $harvest);

    $rss->add_item(title => $title, description => $description);
    return $rss->as_string;
}

fun get_harvest($date_str) {
    my $rs = resultset('HarvestStatusReport')->for_datestr($date_str)->search({
        harvest_date => $date_str,
    });
    return $rs;
}

fun make_description($date_str, $harvest) {
    fun h($tag, $text) {
        return "<$tag>$text</$tag>";
    }

    my $html = h(h1 => "HuNI Harvest for $date_str");

    $html .= qq(<a href="https://harvest.huni.net.au/#$date_str">);
    $html .= "https://harvest.huni.net.au/#$date_str</a><p/>";

    my @th_columns = qw( Provider Feed Original Clean Invalid Solr );
    my @td_columns = qw( provider_name feed_id original_count clean_count invalid_count solr_count );

    my $table = '';
    while (my $row = $harvest->next) {
        my $td = join("\n", map { h(td => $row->$_) } @td_columns);
        $table .= h(tr => $td);
    }

    if ($table) {
        my $th = join("\n", map { h(th => $_) } @th_columns);
        $html .= h(table => "$th$table");
    }
    else {
        $html .= "No new data harvested";
    }

    return $html;
}

1;
