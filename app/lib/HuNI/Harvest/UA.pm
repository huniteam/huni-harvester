package HuNI::Harvest::UA;

use 5.20.0;
use warnings;

use Gzip::Faster                qw( gunzip );
use Function::Parameters        qw( :strict );
use Log::Any                    qw( $log );

use parent 'HTTP::Tiny';
use Class::Method::Modifiers    qw( around );

our $VERSION = $HTTP::Tiny::VERSION;

method new($class: %args) {
    my $verbose = delete $args{verbose};
    if (delete $args{gzip} // 1) {
        $args{default_headers}->{'Accept-Encoding'} = 'gzip';
    }
    my $self = $class->SUPER::new(%args);
    if ($verbose) {
        $self->{_verbose} = 1;
    }
    return bless($self, $class);
}

around request => fun($orig, $self, $method, $url, $options = { }) {
    my $sleeptime = 1;
    my $verbose = $self->{_verbose};
    my $response;
    for (1 .. 5) {
        if ($verbose) {
            $log->info("REQUEST $method $url", $options);
        }
        $response = $self->$orig($method, $url, $options);
        if ($verbose) {
            $log->info("RESPONSE", $response);
        }

        if ($response->{success}) {
            _decode_content($response);
            last;
        }

        last unless _should_retry($response);

        sleep $sleeptime;
        $sleeptime *= 2;
    }
    return $response;
};

fun _should_retry($response) {
    return 0;
}

my %decode_content = (
    gzip => \&gunzip,
);

fun _decode_content($response) {
    my $encoding = $response->{headers}->{'content-encoding'}
        or return;

    if (my $decode = $decode_content{$encoding}) {
        my $old = length $response->{content};
        delete $response->{headers}->{'content-encoding'};
        $response->{content} = $decode->($response->{content});
#        my $new = length $response->{content};
#        my $percent = 100 * ($new - $old) / $new;
#        say STDERR "$old -> $new $percent%";
        return;
    }

    warn "Unexpected content-encoding: $encoding";
}

1;

__END__

=head1 NAME

HuNI::Harvest::UA - HTTP::Tiny with some handy extensions

=head1 DESCRIPTION

Adds automatic gzip handling to HTTP::Tiny - specify gzip => 0 to disable.

=cut
