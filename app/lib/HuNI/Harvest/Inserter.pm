package HuNI::Harvest::Inserter;

use 5.26.0;
use warnings;

use Moo;

use Function::Parameters    qw( :strict );

has resultset => (
    is => 'ro',
    # isa => 'DBIx::Class::Resultset'
    required => 1,
);

has columns => (
    is => 'ro',
    # isa => 'ArrayRef[String]',
    required => 1,
);

has constant_values => (
    is => 'ro',
    default => sub { [ ] },
);

has cache => (
    is => 'lazy',
    builder => method() { [ $self->columns ] },
);

has batch_size => (
    is => 'ro',
    default => 5000,
);

method insert(@values) {
    push(@{ $self->cache }, [ @{ $self->constant_values }, @values ]);
    if (@{ $self->cache } > $self->batch_size) {
        $self->flush;
    }
}

method flush() {
    return unless @{ $self->cache } > 1;

    $self->resultset->populate($self->cache);

    @{ $self->cache } = ( $self->columns );
}

1;
