package HuNI::Harvest::Preprocess::Handler;

use 5.20.0;
use warnings;

use Function::Parameters    qw( :strict );
use XML::LibXML             qw( );

use Moo::Role;

requires qw( preprocess namespaces );

# Creates an XPathContext object using the namespaces provided by the
# namespaces method which is implemented by the consuming class.
has xpath_context => (
    is => 'lazy',
    builder => method() {
        my $xpath_context = XML::LibXML::XPathContext->new;
        for my $prefix (sort keys %{ $self->namespaces }) {
            $xpath_context->registerNs($prefix => $self->namespaces->{$prefix});
        }
        return $xpath_context;
    },
    handles => [qw( findnodes )],
);

method find_text($path, $node) {
    my @nodes = $self->findnodes($path . '/text()', $node);

    if (@nodes == 0) {
        my @parts = split('/', $path);
        my $target = pop(@parts);
        $target =~ s/^.*://;

        die "Can't find $target\n";
    }

    # If there's more than one, just take the first.
    return $nodes[0]->data;
}

1;
