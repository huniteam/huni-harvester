package HuNI::Harvest::Preprocess::Handler::DAAO;

use 5.20.0;
use warnings;

use Function::Parameters    qw( :strict );
use Moo;

with 'HuNI::Harvest::Preprocess::Format::XML';

my $namespaces = {
    oai => 'http://www.openarchives.org/OAI/2.0/',
};

sub namespaces {
    return $namespaces;
}

method preprocess_xml($filename, $dom) {
    my $id = $self->find_text('//oai:record/oai:header/oai:identifier', $dom);
    return ({
        id   => $id,
        type => 'event',
    });
}

1;
