package HuNI::Harvest::Preprocess::Handler::MARC21;

use 5.20.0;
use warnings;

use Function::Parameters    qw( :strict );
use Moo;

with 'HuNI::Harvest::Preprocess::Format::XML';

my $namespaces = {
    ns => 'http://www.openarchives.org/OAI/2.0/',
};

sub namespaces {
    return $namespaces;
}

# 1 means the value is lc(key)
my %type_map = (
    AAIN            => 1,
    ABI             => 1,
    ANALYTIC        => 1,                           # MURA-book
    BOOK            => 1,
    'COMP/FILE'     => 'computer-file',
    'FILM/VIDEO'    => 'film',
    KIT             => 1,
    LANGUAGE        => 1,
    MANUSCRIPT      => 1,
    MAP             => 1,
    MICROFORM       => 1,
    MOVING_IMG      => 'film',
    PAMPHLET        => 1,
    'PICT-COLN'     => 'picture-collection',
    PICTURE         => 1,
    POSTER          => 1,
    RAREBOOK        => 'book',
    RAREPAMPH       => 'pamphlet',
    RARESERIAL      => 'serial',
    REFERENCE       => 'reference',
    SERIAL          => 1,
    'SOUND-COLN'    => 'sound-collection',
    SOUND_ITEM      => 'sound-item',
    WEBSITE         => 1,
);

method preprocess_xml ($filename, $dom) {
    my $id = $self->find_text('//ns:controlfield[@tag="001"]', $dom);
    my $set_spec = $self->find_text('//ns:record/ns:header/ns:setSpec', $dom);

    my $type = $type_map{$set_spec}
        or die "Unexpected set spec \"$set_spec\"\n";

    if ($type eq '1') {
        $type = lc($set_spec);
    }

    return ({
        id   => $id,
        type => $type,
    });
}

1;
