package HuNI::Harvest::Preprocess::Handler::HuNI;

use 5.20.0;
use warnings;

use Function::Parameters    qw( :strict );
use Moo;

with 'HuNI::Harvest::Preprocess::Format::XML';

sub namespaces {
    return [ ];
}

method preprocess_xml($filename, $dom) {
    my $original_filename = $filename;
    $filename =~ s/^oai-//;
    $filename =~ s/\.xml$//i;
    my @components = split(/-/, $filename);
    my $site = shift @components;
    my $id = pop @components;
    my $type = join('-', @components);

    die "Cannot determine id and type of $original_filename\n"
        unless defined $id && defined $type;

    return ({
        id   => $id,
        type => $type,
    });
}

1;
