package HuNI::Harvest::Preprocess::Handler::AMHD;

use 5.20.0;
use warnings;

use Function::Parameters    qw( :strict );
use Moo;

with 'HuNI::Harvest::Preprocess::Format::XML';

sub namespaces {
    return { };
}

method preprocess_xml($filename, $dom) {
    my @types = qw( institution person project );
    for my $type (@types) {
        # eg. /person/PERSON_ID
        my $xpath = "/$type/\U${type}_id";
        if (my $id = eval { $self->find_text($xpath, $dom) }) {
            return {
                id   => $id,
                type => $type,
            };
        }
    }
    die "Can't find type\n";
}

1;
