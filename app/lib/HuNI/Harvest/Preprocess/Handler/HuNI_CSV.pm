package HuNI::Harvest::Preprocess::Handler::HuNI_CSV;

use 5.20.0;
use Moo;
use Path::Tiny;
use Log::Any                qw( $log );
use Function::Parameters    qw( :strict );

with 'HuNI::Harvest::Preprocess::Format::XML';

method namespaces () {
    return [ ];
}

method preprocess_xml($filename, $dom) {
    my $basename = path($filename)->basename('.csv');

    # The id can have underscores, but the type is only going to be concept,
    # organisation etc.
    my ($type, $id) = split('_', $basename, 2);

    return ({
        id   => $id,
        type => $type,
    });
}

1;
