package HuNI::Harvest::Preprocess::Handler::Heurist;

use 5.20.0;
use warnings;

use Function::Parameters    qw( :strict );
use Moo;

with 'HuNI::Harvest::Preprocess::Format::XML';

my $namespaces = {
    ns => 'http://heuristnetwork.org',
};

sub namespaces {
    return $namespaces;
}

method preprocess_xml($filename, $dom) {
    my $id = $self->find_text('//ns:record/ns:id', $dom);
    my $type = $self->find_text('//ns:record/ns:type', $dom);
    $type =~ tr/a-zA-Z0-9_ //cd; # remove all non alphnum chars
    $type =~ tr/ /_/s;           # replace \s+ with _
    return ({
        id   => $id,
        type => $type,
    });
}

1;
