package HuNI::Harvest::Preprocess::Handler::AustLit;

use 5.20.0;
use warnings;

use Function::Parameters    qw( :strict );
use Moo;

with 'HuNI::Harvest::Preprocess::Format::XML';
sub namespaces {
    return { };
}

method preprocess_xml($filename, $dom) {
    my $type = $self->find_text('/austlitWork/cpfDescription/identity/entityType', $dom);
    my $id = $self->find_text('/austlitWork/control/recordId', $dom);

    return ({
        id   => $id,
        type => $type,
    });
}

1;
