package HuNI::Harvest::Preprocess::Handler::PDSC;

use 5.20.0;
use warnings;

use Encode                  qw( encode_utf8 );
use Function::Parameters    qw( :strict );
use Moo;

with 'HuNI::Harvest::Preprocess::Format::XML';

my $namespaces = {
    oai => 'http://www.openarchives.org/OAI/2.0/',
    rif => 'http://ands.org.au/standards/rif-cs/registryObjects',
};

sub namespaces {
    return $namespaces;
}

fun make_filename($type, $id) {
    return join(':::', 'PDSC', $type, $id);
}

method preprocess_xml($filename, $dom) {
    my ($type, $id) = $self->identify_file($dom);

    if ($type eq 'collection') {
        return $self->extract_collection($dom);
    }

    return ({ id => $id, type => $type });
}

method identify_file($dom) {
    my ($request) = $self->findnodes('/oai:OAI-PMH/oai:request', $dom);

    # <request identifier="oai:paradisec.org.au:OE1">
    #                                           ^^^ $id
    my $id = $request->getAttribute('identifier');
    $id =~ s/^.*://;

    #  http://catalog.paradisec.org.au/oai/collection
    #                                      ^^^^^^^^^^ $type
    my $type = $request->textContent;
    $type =~ s/^.*\///;

    return ($type, $id);
}

my %collection_record_type = (
    collection  => 'collection',
    user        => 'person',
);

method extract_collection($dom) {
    my @ret;
    for my $item ($self->findnodes('//rif:registryObject', $dom)) {
        my ($key) = $self->findnodes('rif:key', $item);

        my ($site, $type, $id) = split('/', $key->textContent);
        $type = $collection_record_type{$type} or next;

        my $content = $item->serialize(0, 1);
        push(@ret, {
            id      => $id,
            type    => $type,
            content => $content,
        });
    }
    return @ret;
}

1;
