package HuNI::Harvest::Preprocess::Handler::EAC;

use 5.20.0;
use warnings;

use Function::Parameters    qw( :strict );
use Moo;

with 'HuNI::Harvest::Preprocess::Format::XML';

my $namespaces = {
    n => 'urn:isbn:1-931666-33-4',
};

sub namespaces {
    return $namespaces;
}

method preprocess_xml($filename, $dom) {
    my $type = $self->find_text('//n:eac-cpf/n:cpfDescription/n:identity/n:entityType', $dom);
    my $id = $self->find_text('//n:eac-cpf/n:control/n:recordId', $dom);

    return ({
        id   => $id,
        type => $type,
    });
}

1;
