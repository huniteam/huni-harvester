package HuNI::Harvest::API;

use 5.26.0;
use warnings;

use Dancer2;

set serializer => 'JSON';

use Function::Parameters  qw( :strict );
use HuNI::Harvest::Log    qw( );
use HuNI::Harvest::Util   qw(
    openapi
    validate_spec
    get_range_header
    set_range_header
);

use Try::Tiny;

validate_spec(openapi->spec);
openapi->make_app(app);
openapi->set_hook(after_handler => \&auto_set_range_header);

get '/version.json' => sub {
	return {
        git_sha => $ENV{GIT_SHA},
    };
};

get '/ping' => sub {
    send_file(\"Ok", content_type => 'text/plain');
};

get '/huni-csv.json' => sub {
    send_file($ENV{CSV_VALIDATOR_SCHEMA_PATH},
        charset => 'utf-8',
        content_type => 'application/json',
        system_path => 1,
    );
};

if ($ENV{EXPOSE_OPENAPI_SPEC}) {
    get '/swagger.json' => sub {
        return openapi->swagger;
    };
}

fun auto_set_range_header($app, $params, $result, $metadata) {
    # If we have a result that is an array, and no range header has been set,
    # assume it's a full range and set the header accordingly.
    if ((ref $result eq 'ARRAY') && !get_range_header($app)) {
        my $items = scalar @$result;
        set_range_header($app, 0, $items - 1, $items);
    }
}

1;
