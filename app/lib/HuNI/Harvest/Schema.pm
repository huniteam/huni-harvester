use utf8;
package HuNI::Harvest::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces(
    default_resultset_class => "+HuNI::Harvest::Schema::ResultSet",
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-07-18 06:24:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:yUMER6hXaLjZyiv9F/6kkw

use DateTime                qw( );
use Function::Parameters    qw( :strict );
use Log::Any                qw( $log );

method format_datetime($datetime) {
    return $self->storage->datetime_parser->format_datetime($datetime);
}

method format_date($date) {
    return $self->storage->datetime_parser->format_date($date);
}

method today() {
    # If you're testing a feed and want to run it on different harvest dates,
    # set HUNI_HARVEST_DATE to yyyy-mm-dd.
    if ($ENV{HUNI_HARVEST_DATE}) {
        $log->info("Using HUNI_HARVEST_DATE=\"$ENV{HUNI_HARVEST_DATE}\"");
        my ($year, $month, $day) = split('-', $ENV{HUNI_HARVEST_DATE});
        return DateTime->new(
            year      => $year,
            month     => $month,
            day       => $day,
            time_zone => 'Australia/Melbourne');
    }
    return DateTime->now(time_zone => 'Australia/Melbourne');
}

method vacuum_analyze() {
    # Previously we just used "VACUUM ANALYZE", but that now generates a ton of
    # warnings for all the pg_* and other tables we don't own. Instead specify
    # the exact table names each time.
    my @tables = map { $_->from }
                 grep { $_->isa('DBIx::Class::ResultSource::Table') }
                 map { $self->source($_) }
                 $self->sources;

    $self->storage->dbh_do(fun($storage, $dbh) {
        $dbh->do("VACUUM ANALYZE $_") for @tables;
    });
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
