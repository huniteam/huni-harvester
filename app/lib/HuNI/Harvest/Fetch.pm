package HuNI::Harvest::Fetch;

use 5.26.0;
use warnings;

use Class::Load     qw( load_class );
use Log::Any        qw( $log );
use Moo;

use Function::Parameters qw( :strict );

has harvest => (
    is       => 'ro',
    required => 1,
);

has fetcher => (
    is          => 'lazy',
    builder     => method() {
        my $feed_type = $self->harvest->feed->feed_type_id;
        my $class_name = join('::', __PACKAGE__, 'Handler', $feed_type);
        return load_class($class_name)->new(
            harvest => $self->harvest,
        );
    },
);

method run_fetch_stage() {
    $self->fetcher->fetch;
}

1;
