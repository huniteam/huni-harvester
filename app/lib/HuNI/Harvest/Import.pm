package HuNI::Harvest::Import;

# This module has a bunch of hardcoded stuff that would normally go in the
# database. We hardcode it because this will only be used one time. Once the
# database is deployed, this module is obsolete.

use 5.20.0;
use warnings;

use Cpanel::JSON::XS        qw( encode_json );
use Crypt::Digest::MD5      qw( md5_hex );
use Encode                  qw( encode_utf8 );
use Function::Parameters    qw( :strict );
use HuNI::Harvest::Inserter qw( );
use Log::Any                qw( $log );
use Try::Tiny;
use XML::LibXML             qw( );

use Moo;

has feed => (
    is => 'ro',
    #isa => 'DBIx::Class::Row',
    required => 1,
);

has aggregate => (
    is => 'ro',
    #isa => 'Path::Tiny',
    required => 1,
);

has resources => (
    is => 'ro',
    #isa => 'Path::Tiny',
    required => 1,
);

# replace => 0 means only add new files
# replace => 1 means blow away this feed and start from scratch
has replace => (
    is => 'ro',
    default => 0,
);

has schema => (
    is => 'lazy',
    builder => method() {
        $self->feed->result_source->schema;
    },
);

has files_rs => (
    is => 'lazy',
    builder => method() {
        $self->schema->resultset('File');
    },
);

my %feed_dir_name = (
    'AU-APFA'           => 'APFA',
);

my %resources_name = (
    'AU-APFA'           => 'APFA',
    'AustLit-agents'    => 'AustLitA',
    'AustLit-works'     => 'AustLitB',
    'DAAO-eac-cpf'      => 'DAAO1',
    'DAAO-rif'          => 'DAAO2',
);

method run() {
    if ($self->replace) {
        $self->reset_feed;
    }

    my $feed_id = $self->feed->id;

    my @exclude_filename_regex;
    if ($self->feed->feed_type_id eq 'HuNI') {
        # Some HuNI feed dirs have resources.xml files
        push(@exclude_filename_regex, qr/^resources\.xml$/);
    }
    if ($self->feed->provider_id eq 'AWAP') {
        push(@exclude_filename_regex, qr/-resources$/);
    }
    if ($self->feed->provider_id eq 'AFIRC') {
        push(@exclude_filename_regex, qr/^afirc.xml$/);
    }
    push(@exclude_filename_regex,
        qr/^deleted_records.xml$/,
        qr/^oai-.*-resources$/,
    );

    my $dir_name = $feed_dir_name{$self->feed->id} // $self->feed->id;

    my $deleted_contents_regex;
    if ($self->feed->feed_type_id eq 'OAI') {
        $deleted_contents_regex = qr/status="deleted"/;
    }

    my %latest_file;
    my $latest_rs = $self->schema->resultset('ExtendedOriginal')->search({
        feed_id => $feed_id,
        is_latest => 1,
    });
    while (my $row = $latest_rs->next) {
        $latest_file{$row->name} = {
            file_id    => $row->file_id,
            is_deleted => $row->is_deleted,
            data       => encode_json($row->data),
        };
    }

    my @date_dirs = sort $self->aggregate->children;
    for my $date_dir (@date_dirs) {
        my $feed_dir = $date_dir->child($dir_name, 'original');
        next unless $feed_dir->is_dir;

        my @files = $feed_dir->children;
        next unless @files;

        my $date = $date_dir->basename;

        # If we're not in replace mode, check for a previous harvest.
        if (!$self->replace && $self->feed->harvests->find({ date => $date })) {
            $log->info("Skipping existing $date");
            next;
        }

        $log->info("Importing $date from $feed_dir");
        my $harvest = $self->feed->create_related(harvests => {
            date => $date,
            status_id => 'in-progress',
        });

        my $status;
        my %count = (
            added => 0,
            updated => 0,
            deleted => 0,
            undeleleted => 0,
            total => scalar @files,
        );
        my $inserter = HuNI::Harvest::Inserter->new(
            resultset => $self->schema->resultset('Original'),
            columns => [qw(
                feed_id harvest_date
                name file_id data is_deleted
            )],
            constant_values => [ $harvest->feed_id, $harvest->date ],
        );
        try {
            for my $file (@files) {
                my $name = $file->basename;

                $log->progress(\%count);

                if (grep { $name =~ $_ } @exclude_filename_regex) {
                    $log->debug("Skipping excluded $name");
                    $count{excluded_filename}++;
                    next;
                }

                my $is_deleted = 0;

                my $content = $file->slurp_utf8;
                if ($deleted_contents_regex
                        && ($content =~ $deleted_contents_regex)) {
                    $is_deleted = 1;
                    #$content = $self->files_rs->deleted_file_content;
                    #
                    # Don't act on deleted files yet, there's a chance we've
                    # already deleted this file.
                }

                my $utf8 = encode_utf8($content);

                my $file_id = $self->files_rs->insert_content_utf8($utf8);
                my $previous = $latest_file{$name};
                if ($previous) {
                    # Not sure if this case happens or not.
                    if ($is_deleted) {
                        if ($previous->{is_deleted}) {
                            $count{redeleted}++;
                            next;
                        }
                        else {
                            $count{deleted}++;
                        }
                    }
                    else {
                        if ($previous->{is_deleted}) {
                            $count{undeleted}++;
                        }
                        elsif ($previous->{file_id} eq $file_id) {
                            $count{duplicate}++;
                            next;
                        }
                        else {
                            $count{updated}++;
                        }
                    }
                }
                else {
                    if ($is_deleted) {
                        $count{ghost_deleted}++;
                        next;
                    }

                    $count{added}++;
                }

                my $data = encode_json({ hash => md5_hex($utf8) });
                $latest_file{$name} = {
                    file_id    => $file_id,
                    is_deleted => $is_deleted,
                    data       => $data,
                };

                $inserter->insert(
                    $name,
                    $file_id,
                    $data,
                    $is_deleted,
                );
            }

            $inserter->flush;

            if ($self->feed->feed_type_id eq 'HuNI') {
                my $deleted = $self->process_deletions($harvest, \%latest_file);
                $count{deleted} += $deleted;
                $count{total} += $deleted;
            }

            $log->progress(\%count, 1);

            $status = 'succeeded';
        }
        catch {
            chomp;
            $log->error($_);
            $status = 'failed';
        };

        if ($harvest->originals->count == 0) {
            $log->info("Removing empty harvest");
            $harvest->delete;
        }
        else {
            $harvest->update({ status_id => $status });
        }
    }
}

method reset_feed() {
    $log->info("Deleting previous harvests");
    $self->feed->invalids->delete;
    $self->feed->cleans->delete;
    $self->feed->originals->delete;
    $self->feed->harvests->delete;
}

method process_deletions($harvest, $latest_file) {
    my $resources_filename
        = $resources_name{$self->feed->id} // $self->feed->id;
    $resources_filename .= '-resources.xml';

    my $path = $self->resources->child(
        $harvest->format_date($harvest->date), $resources_filename);

    if (!$path->is_file) {
        $log->warning("$path not found, skipping deletions");
        return 0;
    }

    $log->info("Processing deletes from $path");

    my $keep = $self->load_resources($path);
    my $deleted = 0;
    my $inserter = HuNI::Harvest::Inserter->new(
        resultset => $self->schema->resultset('Original'),
        columns => [qw(
            feed_id harvest_date is_deleted
            name file_id data
        )],
        constant_values => [ $self->feed->id, $harvest->date, 1 ],
    );

    for my $name (keys %$latest_file) {
        next if $keep->{$name};

        my $file = $latest_file->{$name};
        next if $file->{is_deleted};

        $inserter->insert(
            $name,
            $file->{file_id},
            $file->{data},
        );

        $file->{is_deleted} = 1;
        $deleted++;
    }

    $inserter->flush;
    return $deleted;
}

method load_resources($path) {
    my $dom = XML::LibXML->load_xml(string => $path->slurp_utf8);
    my %files = map { $_->data => 1 } $dom->findnodes("//name/text()");
    return \%files;
}

1;
