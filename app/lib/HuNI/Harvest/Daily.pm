package HuNI::Harvest::Daily;

use 5.26.0;
use warnings;

use HuNI::Harvest::Fetch                        qw( );
use HuNI::Harvest::Links                        qw( );
use HuNI::Harvest::Preprocess                   qw( );
use HuNI::Harvest::Solr                         qw( );
use HuNI::Harvest::Transform::Handler::Solr     qw( );
use Log::Any                                    qw( $log );
use Try::Tiny;

use Function::Parameters        qw( :strict );

method do_daily_harvest($harvest) {
    my $feed_id = $harvest->feed_id;
    my $datestr = $harvest->format_date($harvest->date);

    $log->info("Harvesting $feed_id for $datestr");

    my $fetch = HuNI::Harvest::Fetch->new(harvest => $harvest);
    my $pp = HuNI::Harvest::Preprocess->new(harvest => $harvest);
    my $transformer = HuNI::Harvest::Transform::Handler::Solr->new(
        schema => $harvest->result_source->schema,
    );
    my $solr = HuNI::Harvest::Solr->new(
        solr_uri => $ENV{SOLR_UPDATE},
        transformer => $transformer,
    );
    my $linker = HuNI::Harvest::Links->new(harvest => $harvest);

    my $ok = 1;
    try {
        $fetch->run_fetch_stage;
        if ($harvest->originals->count > 0) {
            $pp->run_preprocess_stage;
            if ($harvest->cleans->count > 0) {
                if ($ENV{NO_SOLR_UPDATE}) {
                    $log->info("Skipping solr upload for now");
                }
                else {
                    my $cleans = $harvest->extended_cleans;
                    $solr->transform_and_upload($cleans);
                }
                $linker->run_links_stage;
            }
        }
    }
    catch {
        chomp;
        $log->error($_);
        $ok = 0;
    };

    if ($ok && $harvest->originals->count == 0) {
        $log->info("Harvesting $feed_id for $datestr succeeded, but empty.");
        $harvest->delete;
    }
    else {
        my $status = $ok ? 'succeeded' : 'failed';
        $log->info("Harvesting $feed_id for $datestr $status");
        $harvest->update({ status_id => $status });
    }

    return $ok;
}

1;
