package HuNI::Harvest::Util;
use 5.24.0;
use warnings;
use parent "Exporter::Tiny";

our @EXPORT_OK = qw(
    find_in find_in_rs openapi resultset schema
    do_ranged_query process_match_queries get_range_header set_range_header
    validate_spec solr_transformer links_transformer
);

use Function::Parameters qw( :strict );

use SD::OpenAPI::Live::Dancer2  qw( );
use HuNI::Harvest::Schema;
use HuNI::Harvest::Transform::Handler::Links;
use HuNI::Harvest::Transform::Handler::Solr;

use Log::Any                    qw( $log );
use Path::Tiny                  qw( path );

fun openapi() {
    state $openapi = SD::OpenAPI::Live::Dancer2->new(
        swagger_path => '/app/swagger.yaml',
        namespace    => 'HuNI::Harvest::API',
    );
    return $openapi;
}

fun schema() {
    state $schema = _schema();
    return $schema;
}

fun _schema() {
    return HuNI::Harvest::Schema->connect($ENV{PGDSN}, undef, undef,
        {
            pg_errorlevel => 2, # verbose errors
            on_connect_do =>
                [
                  "SET TIME ZONE 'UTC'",
                  #'SET search_path TO ...',
                ],
        }
    );
}

fun solr_transformer() {
    state $transformer = _solr_transformer();
    return $transformer;
}

fun _solr_transformer() {
    my $schema = schema();
    return HuNI::Harvest::Transform::Handler::Solr->new(schema => $schema);
}

fun links_transformer() {
    state $transformer = _links_transformer();
    return $transformer;
}

fun _links_transformer() {
    my $schema = schema();
    return HuNI::Harvest::Transform::Handler::Links->new(schema => $schema);
}

fun resultset($class) {
    return schema()->resultset($class);
}

# Find a single item in a table or return an error response to he user.
# Note that $id should specify the primary key for the given table.
# Any further filters such as "valid => 1" or "is_fieldworker => 1" should
# appear in the $filter parameter. DBIC's find quietly ignores any query params
# which do not relate to unique constraints.
#
# eg. $households->find({ household_id => 123, is_valid => 1 }) does not
# use the 'is_valid => 1' part in the query.
fun find_in($app, $table, $id, $filter = { }) {
    return find_in_rs($app, resultset($table)->search_rs($filter), $id, {
        "No $table found with" => { id => $id, filter => $filter }
    });
}

# Find a single item in a resultset. If not found, an error response is sent,
# and the function never returns.
fun find_in_rs($app, $resultset, $id, $error) {
    my $result = $resultset->find($id);
    unless ($result) {
        $app->response->status(404);
        $app->send_as(JSON => { errors => $error });
    }
    return $result;
}

my $range_header_key = 'Content-Range';

# Set the Content-Range header on the response
fun set_range_header($app, $start, $end, $total) {
    my $range = ($total == 0) ? '*/0' : "$start-$end/$total";
    $app->response->header($range_header_key => "items $range");
}

# Return the range header if there is one
fun get_range_header($app) {
    return $app->response->header($range_header_key);
}

# Provide a resultset with everything except the ->with_range($range).
# Returns the range as json, and sets the range header.
fun do_ranged_query($app, $rs, $range) {
    my $items = $rs->with_range($range)->for_json;

    # Note that first and last will be wrong if total is zero, but that gets
    # taken care of in set_range_header.
    my $first = $range->[0];
    my $last  = $first + (scalar @$items) - 1;
    my $total = $rs->count;

    set_range_header($app, $first, $last, $total);

    return $items;
}

# Remove any x-match queries from $params and convert them to escaped ilike
# queries in $query. $params and $query can be the same hash.
# $column mapping is an optional hash of param-name => column-name mappings.
fun process_match_queries($params, $metadata, $query, $column_mapping = { }) {
    my $key = 'x-match';
    for my $param (grep { $_->{$key} } @{ $metadata->{parameters} }) {
        my $name = $param->{name};
        next unless exists $params->{$name};

        my $search_string = delete $params->{$name};
        next unless length $search_string > 0;

        $search_string =~ s/[%_]/\\&/g;

        if ($param->{'x-match'} eq 'start') {
            $search_string = "$search_string%";
        }
        elsif ($param->{'x-match'} eq 'substring') {
            $search_string = "%$search_string%";
        }

        my $column = $column_mapping->{$name} // $name;
        $query->{$column} = { ilike => $search_string };
    }
}

# Run further validations on the swagger spec
fun validate_spec($spec) {
    my $errors = 0;

    $log->info('Performing app-side swagger validation');
    while (my ($path, $handlers) = each %{ $spec->{paths} }) {
        while (my ($method, $handler) = each %$handlers) {
            for my $param (@{ $handler->{parameters} // [] }) {
                $errors += validate_param($method, $path, $param);
            }
        }
    }

    die "Swagger validation errors: $errors\n" if $errors;
    $log->info('App-side swagger validated ok');
}

fun validate_param($method, $path, $param) {
    my $prefix = "$method $path: param $param->{name}";
    my $match_key = 'x-match';
    my @match_values = qw( start substring );
    my $match_regex = join('|', @match_values);
    my $errors = 0;

    if (exists $param->{$match_key}) {
        if ($method ne 'get') {
            $log->error("$prefix has $match_key in non-get request");
            $errors++;
        }
        elsif ($param->{$match_key} !~ /^(?:$match_regex)$/) {
            local $, = ', ';
            $log->error("$prefix has $match_key=$param->{$match_key}, must be one of: @match_values");
            $errors++;
        }
    }

    return $errors;
}

1;
