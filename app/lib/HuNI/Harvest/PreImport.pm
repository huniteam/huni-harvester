package HuNI::Harvest::PreImport;

# This module has a bunch of hardcoded stuff that would normally go in the
# database. We hardcode it because this will only be used one time. Once the
# database is deployed, this module is obsolete.

use 5.20.0;
use warnings;

use Function::Parameters    qw( :strict );
use Log::Any                qw( $log );
use Path::Tiny              qw( );

use Moo;

has aggregate => (
    is => 'ro',
    #isa => 'Path::Tiny',
    required => 1,
);

my %prefilter_dirs = (
    AustLit => [
        [ 'AustLit-agents'    => qr/<eac-cpf/                   ],
        [ 'AustLit-works'     => qr/^<austlitWork/              ],
    ],
    DAAO => [
        [ 'DAAO-eac-cpf'      => qr/metadataPrefix="eac-cpf"/   ],
        [ 'DAAO-rif'          => qr/metadataPrefix="rif"/       ],
    ],
    PDSC => [
        [ 'PDSC-olac'         => qr/metadataPrefix="olac"/      ],
        [ 'PDSC-rif'          => qr/metadataPrefix="rif"/       ],
    ],
    MURA => [
        [ 'MURA-abi'          => qr/<setSpec>ABI</              ],
        [ 'MURA-book',        => qr/<setSpec>ANALYTIC</         ],
        [ 'MURA-huni',        => qr/<setSpec>.*</               ],
    ],
);

my %excluded_filename_regex = (
    DAAO => qr/^oai-/,
);

method run() {
    for my $date_dir (sort $self->aggregate->children) {
        for my $provider_dir (sort $date_dir->children) {
            my $provider = $provider_dir->basename;
            my $prefilter = $prefilter_dirs{$provider};
            next unless $prefilter;

            $self->process_feed_dir($date_dir, $provider, $prefilter);
        }
    }
}

method process_feed_dir($date_dir, $provider, $prefilter) {
    my $provider_dir = $date_dir->child($provider);
    my @feeds = map { $_->[0] } @$prefilter;
    if (grep { $date_dir->child($_, 'original')->is_dir } @feeds) {
        $log->info("$provider_dir already processed");
        return;
    }

    my @original_files = $provider_dir->child('original')->children;
    my %stats = (
        total => scalar @original_files,
    );
    my %progress = (
        $date_dir->basename . "/$provider" => \%stats,
    );

    my $exclude = $excluded_filename_regex{$provider};

    for my $original_file (@original_files) {
        if ($exclude && ($original_file->basename =~ $exclude)) {
            $stats{skipped}++;
            next;
        }
        my $content = $original_file->slurp;
        my $linked;
        for my $filter (@$prefilter) {
            my ($feed, $regex) = @$filter;
            if ($content =~ $regex) {
                my $dest_dir = $date_dir->child($feed, 'original');
                $dest_dir->mkpath;

                $log->debug("$original_file -> $dest_dir");
                link($original_file, $dest_dir->child($original_file->basename))
                    or $log->error("Linking $original_file to $dest_dir: $!");
                $linked = 1;
                $stats{$feed}++;
                last;
            }
        }
        if (!$linked) {
            $log->warning("$original_file not matched");
            $stats{other}++;
        }
        $log->progress(\%progress);
    }
    $log->progress(\%progress, 1);
}

1;
