package HuNI::Harvest::Validator::CSV;

use 5.20.0;
use Moo;
use warnings;
use Function::Parameters    qw( :strict );
use Path::Tiny              qw( path );
use Log::Any                qw( $log );
use HTTP::Tiny;
use IPC::Run                qw( run timeout );
use Cwd;
use Cpanel::JSON::XS;
use Try::Tiny;
use URI;

# This is the path to the csv schema. Files to be validated will be in the
# same directory (this is a csvlint limitation).
has schema_path => (
    is => 'ro',
    required => 1,
);

has csvlint_cmd => (
    is => 'ro',
    default => $ENV{CSV_VALIDATOR_CSVLINT_CMD} // 'csvlint',
);

# The csv file will be in the same directory as schema_path
method validate ($csv_path) {
    my $cwd = getcwd;
    my $schema_dir = $self->schema_path->parent;

    chdir $schema_dir->stringify;

    my @cmd = (
        $self->csvlint_cmd,
        '--schema=' . $self->schema_path->basename,
        '--json',
        '--werror',
        $csv_path->basename);

    $log->debug(@cmd);

    my ($stdin, $stdout, $stderr);
    my $ok = do {
        # Need to set LANG here so that csvlint knows the file is UTF8.
        # Thanks to @russellj for his blood sweat and tears finding that out
        # four years ago.
        local $ENV{LANG} = 'C.UTF-8';
        run ( [ @cmd ], \$stdin, \$stdout, \$stderr, timeout( 60 ) );
    };

    chdir $cwd;

    my $results;
    if ( $stdout ) {
        $results = try {
            decode_json( $stdout );
        }
        catch {
            $log->error($stdout);
            die ( $_ );
        };
    }

    if ( $stderr ) {
        $log->warning ( $stderr );
    }

    return $results;
}

1;
