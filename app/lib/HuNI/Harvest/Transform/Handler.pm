package HuNI::Harvest::Transform::Handler;

use 5.26.0;
use warnings;

use Log::Any                    qw( $log );
use XML::LibXML                 qw( );
use XML::LibXSLT                qw( );

use Function::Parameters        qw( :strict );

use Moo;

has schema => (
    is => 'ro',
    required => 1,
);

has xslt => (
    is => 'lazy',
    builder => method() {
        XML::LibXSLT->new;
    },
);

has xpc => (
    is => 'lazy',
    builder => method() {
        my $xpc = XML::LibXML::XPathContext->new;
        return $xpc;
    },
);

has stylesheet => (
    is => 'lazy',
    builder => sub { +{ } },
);

method do_transform($clean) {
    my $stylesheet =
        $self->load_stylesheet($clean->feed_id, $clean->type);
    if (!$stylesheet) {
        my $type = join('/', $clean->feed_id, $clean->type);
        $self->warning("No transform found for $type");
        return;
    }

    my $xml = XML::LibXML->load_xml(string => $clean->file->content);
    return $stylesheet->transform($xml);
}

method load_stylesheet($feed_id, $type) {
    # _load_stylesheet_from_file returns undef if it can't find a stylesheet
    # We store that undef so we don't keep checking the same stylesheet.
    if (!exists $self->stylesheet->{$feed_id}->{$type}) {
        my $transform = $self->schema->resultset('Transform')->find({
            type        => $self->transform_type,
            feed_id     => $feed_id,
            file_type   => $type,
        });

        $self->stylesheet->{$feed_id}->{$type} =
            $transform && $self->_create_stylesheet($transform->transform);
    }

    return $self->stylesheet->{$feed_id}->{$type};
}

method _create_stylesheet($string) {
    my $xml = XML::LibXML->load_xml(string => $string);
    return $self->xslt->parse_stylesheet($xml);
}

# We warn about datetime formats we don't recognise. Store the ones we've
# warned about so we only warn once for each one.
has warned_about => (
    is => 'lazy',
    builder => method() {
        return { };
    },
);

method warning($message) {
    if (!$self->warned_about->{$message}) {
        $log->warning($message);
    }
    $self->warned_about->{$message}++;
}

1;
