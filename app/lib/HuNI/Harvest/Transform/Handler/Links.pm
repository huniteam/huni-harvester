package HuNI::Harvest::Transform::Handler::Links;

use 5.26.0;
use warnings;

use Function::Parameters        qw( :strict );

use Moo;
extends 'HuNI::Harvest::Transform::Handler';

sub transform_type { 'link' }

method transform($clean) {
    my $record = $self->do_transform($clean);
    return [ ] unless $record;

    my @links;
    for my $link ($self->xpc->findnodes('//link', $record)) {
        push(@links, _xml_to_hash([ $link->childNodes ]));
    }

    return \@links;
}

method transform_only($clean) {
    my $record = $self->do_transform($clean);
    return $record->serialize(1, 1);
}

# Flatten the resulting link xml to a hash. There are no attributes or arrays.
# Each hash value is either a string, or another hash.
fun _xml_to_hash($nodes) {
    my %json;
    for my $node (@$nodes) {
        next unless $node->hasChildNodes;

        my @children = $node->childNodes;
        $json{ $node->nodeName } = _get_node_text(\@children)
                                // _xml_to_hash(\@children);
    }
    return \%json;
}

fun _get_node_text($nodes) {
    return unless @$nodes == 1;
    return unless $nodes->[0]->nodeName eq '#text';

    my $text = $nodes->[0]->data;
    $text =~ s/^\s*|\s*$//g;
    return $text;
}

1;
