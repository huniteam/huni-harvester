package HuNI::Harvest::Transform::Handler::Solr;

use 5.26.0;
use warnings;

use DateTime                    qw( );
use DateTime::Format::Strptime  qw( );
use HTML::Entities              qw( decode_entities );
use HTML::Strip                 qw( );
use Log::Any                    qw( $log );

use Function::Parameters        qw( :strict );

use Moo;
extends 'HuNI::Harvest::Transform::Handler';

sub transform_type { 'solr' }

has timestamp => (
    is => 'lazy',
    builder => method() {
        my $now = DateTime->now;
        return $now->strftime('%FT%TZ');
    },
);

has documents_api_url => (
    is => 'ro',
    builder => sub {
        my $url = $ENV{DOCUMENTS_API_URL}
            or die "\$DOCUMENTS_API_URL not set \n";
        return $url;
    },
);

has html_sanitiser => (
    is => 'ro',
    builder => sub {
        HTML::Strip->new
    },
);

has datetime_parsers => (
    is => 'lazy',
    builder => method() {
        my @formats = (
            '%FT%TZ',   # 1976-12-31T23:59:01Z
            '%Y-%m-%d', # 1976-01-01
            '%Y %m %d', # 1976 01 01
            '%d %B %Y', # 12 January 1997
            '%B %Y',    # February 1998
            '%Y',       # 2004
            'c. %Y',    # c. 2004
            '%Y?',      # 2004?
        );

        my @parsers = map {
            DateTime::Format::Strptime->new(pattern => $_, time_zone => 'UTC')
        } @formats;

        return \@parsers;
    },
);

method transform($clean) {
    my $record = $self->do_transform($clean);
    return unless $record;

    # All the transforms wrap an <add> tag around each document. We don't
    # want this, we just want the <doc> inside.
    $record = $record->documentElement->firstChild;

    for my $field ($self->xpc->findnodes('//field', $record)) {
        if ($field->textContent !~ /\S/) {
            # Remove blank fields
            $record->removeChild($field);
        }
        elsif ($field->getAttribute('name') =~ /Date$/) {
            # Normalise date fields for solr
            if (my $date = $self->normalise_datetime($field->textContent)) {
                $field->firstChild->setData($date);
            }
            else {
                $record->removeChild($field);
            }
        }
        else {
            # Strip any html
            my $stripped = $self->strip_html($field->textContent);
            $field->firstChild->setData($stripped);
        }
    }

    my $docid = $clean->name =~ tr/:/*/r;
    $record->addChild(field(docid => $docid));
    $record->addChild(field(sourceLastUpdate => $self->timestamp));

    # TODO: These are the per-site fields which normally live in the transforms.
    #       For HuNI_CSV feeds, these should get added to the feed.params json,
    #       inserted when we create the feed. Not sure what those should be just
    #       yet.
    my $feed = $clean->feed;
    if ($feed->feed_type_id eq 'HuNI_CSV') {
        my $params = $feed->params;
        $record->addChild(field(sourceAgencyCode => $feed->provider->id));
        $record->addChild(field(sourceAgencyName => $feed->provider->name));
        #$record->addChild(field(sourceSiteAddress => 'TODO: sourceSiteAddress'));
        #$record->addChild(field(sourceSiteTag => 'TODO: sourceSiteTag'));
    }

    my $harvest_date = $clean->format_date($clean->harvest_date);
    for my $date (grep { $_ le $harvest_date } @{ $clean->history }) {
        $record->addChild(field(document_history =>
            $self->format_link(solr => $clean, $date)));
    }
    $record->addChild(field(provider_source =>
        $self->format_link(clean => $clean, $harvest_date)));

    if ($clean->original->is_deleted) {
        $record->addChild(field(isDeleted => 'true'));
    }

    return $record->serialize(1, 1);
}

method strip_html($input) {
    # Convert &lt; to < etc.
    my $decoded = decode_entities($input);

    # Strip tags
    my $stripped = $self->html_sanitiser->parse($decoded);
    $self->html_sanitiser->eof;

    # Originally I was re-encoding these entities, but this isn't what we want,
    # because the xml encoder will encode these as necessary.
    return $stripped;
}

method format_link($subdir, $clean, $date) {
    sprintf('%s/%s/%s:::%s:::%s?date=%s',
    #http://ralph-p500.fz.sdlocal.net:8080/api/documents/clean/FCAC:::corporateBody:::AE00029?date=2017-06-23
        $self->documents_api_url,
        $subdir,
        $clean->feed->provider_id,
        $clean->type,
        $clean->id,
        $date);
}

fun field($key, $value) {
    my $node = XML::LibXML::Element->new('field');
    $node->setAttribute(name => $key);
    $node->appendText($value);
    return $node;
}

method normalise_datetime($datestr) {
    for my $parser (@{ $self->datetime_parsers }) {
        my $datetime = $parser->parse_datetime($datestr);
        if ($datetime) {
            return $datetime->strftime('%FT%TZ');
        }
    }
    $self->warning("Unexpected date format \"$datestr\"");
    return;
}

1;
