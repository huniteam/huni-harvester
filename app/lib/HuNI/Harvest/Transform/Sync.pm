package HuNI::Harvest::Transform::Sync;

use 5.26.0;
use warnings;

use Function::Parameters    qw( :strict );
use Log::Any                qw( $log );
use Path::Tiny              qw( path );
use Try::Tiny;

use Moo;

has schema => (
    is => 'ro',
    required => 1,
);

method sync($root) {
    $root = path($root);    # path ctor is idempotent

    my $transform_types = $self->schema->resultset('TransformType')
                               ->search({ }, { order_by => 'id' });

    my $providers = $self->schema->resultset('Provider')->search({ }, {
            prefetch => 'feeds',
        });

    # If a provider has multiple feeds, and they're all the same, we create a
    # single directory with one set of transforms. The code below will copy
    # them to each feed.
    # If a provider has multiple feeds and they differ, we can instead create
    # a directory for each feed.
    my %provider_feeds;
    while (my $provider = $providers->next) {
        $provider_feeds{ $provider->id } = [ $provider->feeds ];

        for my $feed ($provider->feeds->all) {
            $provider_feeds{ $feed->id } = [ $feed ] for $provider->feeds;

            # This is a little bit of a hack. If the feed is a HuNI_CSV feed,
            # load its transforms from HuNI_CSV.
            my $huni_csv = 'HuNI_CSV';
            if ($feed->feed_type_id eq $huni_csv) {
                push(@{ $provider_feeds{$huni_csv} }, $feed);
            }
        }
    }

    while (my $transform_type = $transform_types->next) {
        $transform_type->transforms->delete;
        $self->load_type($root->child($transform_type->id), $transform_type, \%provider_feeds);
    }
}

method load_type($path, $type, $provider_feeds) {
    my $ext = qr/\.xsl$/;
    for my $dir (sort $path->children) {
        next unless $dir->is_dir;

        my $name = $dir->basename;
        my $feeds = $provider_feeds->{ $dir->basename }
            or next;

        for my $xsl (sort $dir->children($ext)) {
            my %transform = (
                file_type   => $xsl->basename($ext),
                transform   => try {
                    $xsl->slurp_utf8
                }
                catch {
                    chomp;
                    die "Loading $xsl: $_\n";
                },
            );

            for my $feed (@$feeds) {
                my $feed_id = $feed->id;
                my $type_id = $type->id;
                $log->info("Adding $type_id transform: $feed_id/$transform{file_type}");
                $type->add_to_transforms({
                    %transform,
                    feed_id   => $feed->id,
                });
            }
        }
    }
}

1;
