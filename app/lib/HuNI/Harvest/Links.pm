package HuNI::Harvest::Links;

use 5.26.0;
use warnings;

use Function::Parameters                        qw( :strict );
use HuNI::Harvest::Links::Insert                qw( );
use HuNI::Harvest::Transform::Handler::Links    qw( );
use Log::Any                                    qw( $log );

use Moo;

has harvest => (
    is => 'ro',
    required => 1,
);

has schema => (
    is => 'lazy',
    builder => method() {
        $self->harvest->result_source->schema;
    },
);

has extended_cleans => (
    is => 'lazy',
    builder => method() {
        $self->schema->resultset('ExtendedClean');
    },
);

has config => (
    is => 'lazy',
    builder => method() {
        $self->harvest->feed->links_config,
    },
);

has transformer => (
    is => 'lazy',
    builder => method() {
        HuNI::Harvest::Transform::Handler::Links->new(schema => $self->schema);
    },
);

has inserter => (
    is => 'lazy',
    builder => method() {
        HuNI::Harvest::Links::Insert->new(
            auth => $ENV{WEBAPP_API_AUTH},
            base => $ENV{WEBAPP_API_URI},
        ),
    },
);

method run_links_stage() {
    my $feed_id = $self->harvest->feed_id;
    my $date = $self->harvest->date->ymd;
    my $desc = "$feed_id $date";

    if (!$self->config) {
        $log->warn("No links config for $feed_id");
        return;
    }

    $log->info("Finding links for $desc");
    my $links = $self->extract_links;
    if (!@$links) {
        $log->warn("No links found.");
        return;
    }

    $log->info("Assigning linktypes for $desc");
    $self->assign_linktypes($links);

    $log->info("Uploading links for $desc");
    $self->upload_links($links);
}

method extract_links() {
    my %progress = (
        A_cleans => 0,
        B_linked => 0,
        B_unlinked => 0,
        C_created => 0,
        C_candidates => 0,
        C_unmatched => 0,
        C_ignored => 0,
        C_invalid => 0,
    );

    my %links;

    # Stage one is to process all the clean documents, extract any potential
    # links, and resolve them into a single set of valid links.
    for my $from ($self->harvest->cleans) {
        $progress{A_cleans}++;

        my $was_linked = 0;
        my $candidates = $self->transformer->transform($from);
        for my $link (@$candidates) {
            my $key = $self->resolve($from, $link, \%progress);
            next unless $key;

            $was_linked = 1;

            if (exists $links{$key}) {
                # Merge the synopses
                $links{$key}->{synopsis} =
                    strcat($links{$key}->{synopsis}, $link->{synopsis});
            }
            else {
                $links{$key} = $link;
            }
        }

        $progress{C_candidates} += @$candidates;
        $progress{$was_linked ? 'B_linked' : 'B_unlinked'}++;
        $log->progress(\%progress);
    }
    $log->progress(\%progress, 1);
    return [ values %links ];
}

method assign_linktypes($links) {
    my %progress = (
        assigned => 0,
        total    => scalar @$links,
    );

    for my $link (@$links) {
        $self->inserter->get_linktype($link);
        $progress{assigned}++;
        $log->progress(\%progress);
    }
    $log->progress(\%progress, 1);
}

method upload_links($links) {
    my %progress = (
        added => 0,
        duplicate => 0,
        total => scalar @$links,
    );

    # Stage three uploads the links in batches
    my @links_to_insert = @$links;
    while (@links_to_insert) {
        my $batch_size = 500;
        my @batch = splice(@links_to_insert, 0, $batch_size);
        my $result = $self->inserter->insert_link_batch(\@batch);

        $progress{added}      += $result->{added}     // 0;
        $progress{duplicate}  += $result->{duplicate} // 0;
        $log->progress(\%progress);
    }
    $log->progress(\%progress, 1);
}

my %id_map = (
    id          => 'me.id',
    feed        => 'me.feed_id',
    provider    => 'feed.provider_id',
);

method resolve($from, $link, $progress) {
    my $to = $self->find_target(delete $link->{to});
    if (!$to) {
        $progress->{C_invalid}++;
        return;
    }

    $link->{from_docid}          = $from->name('***');
    $link->{from}->{feed_id}     = $from->feed_id;
    $link->{from}->{provider_id} = $from->feed->provider_id;
    $link->{from}->{type}        = $from->type;
    $link->{from}->{id}          = $from->id;

    $link->{to_docid}            = $to->name('***');
    $link->{to}->{feed_id}       = $to->feed_id;
    $link->{to}->{provider_id}   = $to->feed->provider_id;
    $link->{to}->{type}          = $to->type;
    $link->{to}->{id}            = $to->id;

    for my $template (@{ $self->config }) {
        if (is_match($link, $template->{match})) {
            if (!$template->{link}) {
                # We have a match, but the template has no link, which means we
                # should ignore this link. Typically this is when there is
                # A->B and B->A links available, and the A->B version has better
                # data than the B->A.
                $progress->{C_ignored}++;
                return;
            }

            # We got a match with a linktype. Create the link.
            for my $key (keys %{ $template->{link} }) {
                $link->{$key} = $template->{link}->{$key};
            }
            $progress->{C_created}++;
            return $self->normalise($link);
        }
    }
    $progress->{C_unmatched}++;
    return;
}

method find_target($doc) {
    my $query = {
        'me.is_latest'        => 1,
        'original.is_deleted' => 0,
    };
    for my $key (keys %$doc) {
        my $query_key = $id_map{$key} // $key;
        $query->{$query_key} = $doc->{$key};
    }

    my $attr = {
        join => [qw( feed original )],
    };
    my @targets = $self->extended_cleans->search($query, $attr);

    return $targets[0] if @targets == 1;

    if (@targets > 1) {
        $log->warn("Multiple link matches for ", $doc);
    }
    return;
}

fun is_match($candidate, $template) {

    for my $key (keys %$template) {
        my $candidate_value = $candidate->{$key};
        #$log->info("Template key $key not in candidate") unless defined $candidate_value;
        return unless defined $candidate_value;

        my $template_value = $template->{$key};

        #$log->info("Template value for $key different type to candidate") unless ref($template_value) eq ref($candidate_value);
        return unless ref($template_value) eq ref($candidate_value);
        if (ref $template_value eq 'HASH') {
            return unless is_match($candidate_value, $template_value);
        }
        else {
            #$log->info("Candidate $key=$candidate_value != template $key=$template_value") if $candidate_value ne $template_value && $key ne 'relationship';;
            return unless $candidate_value eq $template_value;
        }
    }

    return 1;
}

method normalise($link) {
    if ($link->{from_docid} gt $link->{to_docid}) {
        # Switch the sense of the link around.
        swap($link, qw( from_docid  to_docid    ));
        swap($link, qw( from_type   to_type     ));
        swap($link, qw( link_type   pair_type   ));
    }

    return join('-', map { $link->{$_} } qw( from_docid to_docid link_type ));
}

fun swap($hash, $k1, $k2) {
    ( $hash->{$k1}, $hash->{$k2} ) = ( $hash->{$k2}, $hash->{$k1} );
}

fun strcat($s1, $s2) {
    $s1 //= '';
    $s2 //= '';
    return $s1 if length $s2 == 0;
    return $s2 if length $s1 == 0;
    return $s2 if $s1 eq $s2;
    return "$s1 / $s2";
}

1;
