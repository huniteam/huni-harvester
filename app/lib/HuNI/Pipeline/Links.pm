package HuNI::Pipeline::Links;
use 5.014_000;
use warnings;

our $VERSION = "1.02";

use Data::Dumper::Concise           qw( Dumper );
use Exporter                        qw( import );
use Function::Parameters            qw( :strict );
use Log::Any                        qw( $log );
use Time::Elapsed                   qw( elapsed );

use HuNI::Pipeline::Links::Config   qw( );
use HuNI::Pipeline::Links::Insert   qw( );
use HuNI::Pipeline::Links::Loader   qw( );
use HuNI::Pipeline::Links::Resolve  qw( );

our @EXPORT_OK = qw( run );

fun run(:$solr, :$api, :$config, :$auth, :$paths, :$dry = 0, :$verbose = 0) {
    $config = HuNI::Pipeline::Links::Config->new(filename => $config);
    my $insert =  HuNI::Pipeline::Links::Insert->new(auth => $auth, base => $api, dry => $dry);
    my $loader = HuNI::Pipeline::Links::Loader->new(@$paths);
    my $resolver = HuNI::Pipeline::Links::Resolve->new(solr_url => $solr);

    my $start = time;
    my $report_time = 60;
    my $next_report = time + $report_time;
    my $total = '00 candidate';
    my %stats;

    my %links;

    # First pass throws out invalid and ignore links, and merges the rest.
    while (my $link = $loader->next) {
        $stats{$total}++;

        if (! (resolve($resolver, $link, 'from') &&
               resolve($resolver, $link, 'to'))) {
            $stats{'01 invalid'}++;
        }
        elsif (! $config->find_linktype($link)) {
            $stats{'02 ignored'}++;
        }
        else {
            my $key = normalise($link);
            if (exists $links{$key}) {
                # Merge the synopses
                $links{$key}->{synopsis} =
                    strcat($links{$key}->{synopsis}, $link->{synopsis});
                $stats{'03 merged'}++;
            }
            else {
                $links{$key} = $link;
                $stats{'04 unique'}++;
            }
        }

        if (time >= $next_report) {
            $stats{time} = elapsed(time - $start);
            $next_report += $report_time;
            $log->info(Dumper(\%stats));
        }
    }

    # Second pass assigns linktypes
    $log->info("Assigning linktypes");
    for my $link (values %links) {
        $insert->get_linktype($link);
    }

    # Third pass works on unique merged links.
    $log->info("Uploading links");
    my @links_to_insert = values %links;
    while (@links_to_insert) {
        my $batch_size = 500;
        my @batch = splice(@links_to_insert, 0, $batch_size);
        my $bs = $insert->insert_link_batch(\@batch);

        $stats{'06 added'}     += $bs->{added};
        $stats{'05 duplicate'} += $bs->{duplicate};

        if (time >= $next_report) {
            $stats{time} = elapsed(time - $start);
            $next_report += $report_time;
            $log->info(Dumper(\%stats));
        }
    }

    $stats{time} = elapsed(time - $start);
    $next_report += $report_time;
    $log->info(Dumper(\%stats));
}

fun resolve($resolver, $link, $end) {
    my $sep = '***';
    my ($feed, $type, $id) = split(/\Q$sep\E/, $link->{$end . '_docid'});

    my $info = $resolver->lookup_id($feed, $id);
    return unless $info;

    $link->{$end . '_docid'} = $info->{docid};
    $link->{$end . '_type'}  = $info->{type};
    $link->{$end . '_feed'}  = $feed;

    return $link;
}

fun normalise($link) {
    if ($link->{from_docid} gt $link->{to_docid}) {
        # Switch the sense of the link around.
        swap($link, qw( from_docid to_docid ));
        swap($link, qw( link_type pair_type ));
    }

    return join('-', map { $link->{$_} } qw( from_docid to_docid link_type ));
}

fun swap($hash, $k1, $k2) {
    ( $hash->{$k1}, $hash->{$k2} ) = ( $hash->{$k2}, $hash->{$k1} );
}

fun strcat($s1, $s2) {
    return $s1 if length $s2 == 0;
    return $s2 if length $s1 == 0;
    return $s2 if $s1 eq $s2;
    return "$s1 / $s2";
}
1;
__END__

=encoding utf-8

=for stopwords HuNI /usr/local/huni-perl

=head1 NAME

HuNI::Pipeline::Links - Insert harvested links into HuNI

=head1 INSTALLATION

cpanm -L /usr/local/huni-perl HuNI-Pipeline-Links-1.02.tar.gz

=head1  LICENSE

Copyright (C) Strategic Data.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 AUTHOR

Stephen Thirlwall E<lt>stephent@stategicdata.com.auE<gt>

=cut

