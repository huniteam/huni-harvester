package HuNI::Pipeline::Links::Insert;

use 5.014_000; # This needs to run on agg01 right now, which has perl 5.14.2
use warnings;

use Data::Dumper::Concise   qw( Dumper );
use Function::Parameters    qw( :strict );
use Log::Any                qw( $log );

use Moo;

has auth => ( is => 'ro', required => 1 );
has base => ( is => 'ro', required => 1 );
has dry  => ( is => 'ro', default => 0 );
has json => ( is => 'lazy', builder => sub { Cpanel::JSON::XS->new->utf8 } );
has ua   => ( is => 'lazy', builder => sub { HTTP::Tiny->new } );
has linktype => ( is => 'lazy', builder => sub { { } } );

method insert_link_batch($links) {
    return $self->request(POST => 'link/batch', $links);
}

method get_linktype($link) {
    my $key = join('-',
        map { $link->{$_} } qw( from_type to_type link_type pair_type ));

    $self->linktype->{$key} //= $self->create_linktype($link);

    $link->{linktype_id} = $self->linktype->{$key}->{id};
    $link->{pairtype_id} = $self->linktype->{$key}->{pair_id};
}

method create_linktype($link) {
    my $data = {
        from => $link->{from_type},
        to   => $link->{to_type},
        name => $link->{link_type},
        pair => $link->{pair_type},
    };

    return $self->request(POST => 'linktype', $data, [ 400 ]);
}

method request($method, $endpoint, $data = undef, $allowed = [ ]) {
    if ($self->dry && ($method ne 'GET')) {
        return { id => 666 };
    }

    my $url = $self->make_url($endpoint);
    my $options = { };
    if (defined $data) {
        $options->{headers} = {
            'Content-Type' => 'application/json',
        };
        $options->{content} = $self->json->encode($data);
    }
    my $response = $self->ua->request($method, $url, $options);

    if (!$response->{success}) {
        # If this is an allowed response type, just return.
        return if grep { $_ eq $response->{status} } @$allowed;

        local $, = ' ';
        print STDERR $method, $endpoint, Dumper($data), Dumper($response);
        die;
    }

    return $self->json->decode($response->{content});
}

method make_url($endpoint) {
    my $url = $self->base;
    $url =~ s{://}{'://' . $self->auth . '@'}e;
    return $url . $endpoint;
}

1;
