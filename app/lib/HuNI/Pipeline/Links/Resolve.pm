package HuNI::Pipeline::Links::Resolve;

use 5.014_000; # This needs to run on agg01 right now, which has perl 5.14.2
use warnings;

use Cpanel::JSON::XS        qw( );
use Function::Parameters    qw( :strict );
use HTTP::Tiny              qw( );
use Log::Any                qw( $log );

use Moo;

has solr_url => (
    is => 'ro',
    required => 1
);

has json => (
    is => 'lazy',
    builder => sub { Cpanel::JSON::XS->new->utf8 },
);

has ua => (
    is => 'lazy',
    builder => sub { HTTP::Tiny->new },
);

has id_map => (
    is => 'lazy',
    builder => sub { +{ } },
);

method lookup_id($feed, $id) {
    $self->id_map->{$feed} //= $self->load_feed_ids($feed);
    my $doc = $self->id_map->{$feed}->{$id};
    return unless $doc;
    return { docid => $doc->[0], type => $doc->[1] };
}

method load_feed_ids($feed) {
    my $count = $self->get($self->solr_query(
        rows => 0,
        q => "sourceAgencyCode:$feed",
    ))->{response}->{numFound};

    $log->debug("Loading $count $feed records from solr");

    my $data = $self->get($self->solr_query(
        rows => $count,
        q => "sourceAgencyCode:$feed",
        fl => 'docid,entityType,sourceID',
    ))->{response}->{docs};

    return { map { $_->{sourceID} => [ $_->{docid}, $_->{entityType} ] } @$data };
}

method get($url) {
    my $response = $self->ua->get($url);

    die "$response->{status} $response->{reason}: $response->{content}"
        unless $response->{success};
    return $self->json->decode($response->{content});
}

method solr_query(%params) {
    return $self->solr_url
         . '/select?wt=json&'
         .  join('&', map { "$_=$params{$_}" } keys %params);
}

1;
