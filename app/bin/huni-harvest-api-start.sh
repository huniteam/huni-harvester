#!/bin/bash

exec plackup \
    -s Gazelle \
    --listen :80 \
    --no-default-middleware \
    --backlog 16 \
    /app/api.psgi
