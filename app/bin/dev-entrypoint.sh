#!/bin/bash -e

# Wait for the postgres fixture to become available
eval $( /fixture/postgres/wait.sh )

# Create the final sqitch file
cp /app/sqitch.conf $SQITCH_CONF
echo "uri=db:pg://$PGUSER:$PGPASSWORD@$PGHOST:$PGHOST/$PGDATABASE" \
        >> $SQITCH_CONF

export PGDSN="dbi:Pg:host=$PGHOST;port=$PGPORT;dbname=$PGDATABASE;user=$PGUSER;password=$PGPASSWORD";

interrupt() {
    echo Exiting ...
    exit 2
}

trap interrupt INT QUIT TERM

if [[ $# > 0 ]]; then
    # Don't use eval so we can use our own signal handling above.
    "$@"
else
    sqitch deploy
    ./tools/bin/create-csv-feed \
        --provider-name "Example CSV Provider" \
        --provider-id "example-provider" \
        --feed-id "example-feed" \
        --url "http://example-csv-feed/csv"
fi
