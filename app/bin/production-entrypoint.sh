#!/bin/bash
set -e

secrets=(
    pgpassword
    webapp_api_auth
)

for secret_name in ${secrets[@]}; do
    env_name="${secret_name^^}" # ^^ is toupper

    if [ -f "${!env_name}" ]
    then
        export $env_name="$(cat ${!env_name})"
    fi
done

export PGDSN="dbi:Pg:host=${PGHOST};port=${PGPORT};dbname=${PGDATABASE};user=${PGUSER};password=${PGPASSWORD}"

exec "$@"
