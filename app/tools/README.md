All scripts in this directory should be runnable from the host shell.

Anything that should be run from within docker should live in the bin/
subdirectory.
