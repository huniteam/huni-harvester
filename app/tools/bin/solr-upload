#!/usr/bin/env perl

use 5.26.0;
use warnings;

use Getopt::Args;
use HuNI::Harvest::Log                          qw( );
use HuNI::Harvest::Schema                       qw( );
use HuNI::Harvest::Solr                         qw( );
use HuNI::Harvest::Transform::Handler::Solr     qw( );
use Log::Any                                    qw( $log );
use Path::Tiny                                  qw( path );
use Try::Tiny;

use Function::Parameters    qw( :strict );

opt date => (
    isa         => 'Str',
    comment     => 'Date to transform (defaults to all latest files)',
);

arg feeds => (
    isa         => 'ArrayRef',
    comment     => 'Feeds to transform (or "all")',
    greedy      => 1,
    required    => 1,
);

exit(main(optargs) ? 0 : 1);

fun main($arg) {
    my $schema = HuNI::Harvest::Schema->connect($ENV{PGDSN});
    my $transform = HuNI::Harvest::Transform::Handler::Solr->new(
        schema => $schema,
    );
    my $solr = HuNI::Harvest::Solr->new(
        solr_uri => $ENV{SOLR_UPDATE},
        transformer => $transform,
    );

    my $feeds = feeds_resultset($schema, $arg);
    my $query = { };
    if ($arg->{date}) {
        $query->{'me.harvest_date'} = $arg->{date};
    }
    else {
        $query->{is_latest} = 1;
    }

    while (my $feed = $feeds->next) {
        my $cleans = $feed->extended_cleans->search_rs($query);
        $log->info(sprintf('Transforming %d %s records', $cleans->count, $feed->id));
        try {
            $solr->transform_and_upload($cleans);
        }
        catch {
            chomp;
            $log->error($_);
        };
    }
    return 1;
}

fun feeds_resultset($schema, $arg) {
    my @feeds = @{ $arg->{feeds} };
    my $query = { };
    if ((@feeds == 1) && lc($feeds[0] eq 'all')) {
        # If we ask for all feeds, just give the enabled feeds.
        $query->{is_enabled} = 1;
    }
    else {
        # If we specify feeds, allow the user to override disabled feeds.
        $query->{id} = \@feeds;
    }
    my $attr = { order_by => [qw( id )] };

    return $schema->resultset('Feed')->search_rs($query, $attr);
}
